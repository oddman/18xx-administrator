# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 21:07:20 2015

TODO:
* create company dialog
* Popup-locations!
* Scrolling / collapsing company list / scaling

@author: Oddman
"""

import tkinter as tki
import tkinter.ttk as ttk
import pickle as pkl
import tkinter.font as tkFont
import tkinter.filedialog as tkFileDialog
import tkinter.colorchooser as tkColorChooser
#from os import path
import os
from tkinter.messagebox import showinfo
import datetime
#import matplotlib
#import numpy as np

PADVAL = 0
HEADERFONT = 12
BIGFONT = 12
SMALLFONT = 8
SHOW_COMPSTOCK = True

UNDOSTACK = []
REDOSTACK = []

DEFAULT_PLAYERS = []
class Memento:
    def __init__(self,logMessage,doFuncList,doVarList,undoFuncList,undoVarList):
        global REDOSTACK
        self.logMessage = logMessage
        self.doFuncList = doFuncList
        self.doVarList = doVarList
        self.undoFuncList = undoFuncList
        self.undoVarList = undoVarList
        ## Done creatin'? Fire!
        self.do()
        ## Also clear the redo stack, because a new thing has been done.
        REDOSTACK = []

    def do(self):
        UNDOSTACK.append(self)
#        print self.doFuncList
        for i,doFunc in enumerate(self.doFuncList):
#            print doFunc
            doFunc(*self.doVarList[i])

    def undo(self):
        REDOSTACK.append(self)
        UNDOSTACK.pop()
        for i,undoFunc in enumerate(self.undoFuncList):
            undoFunc(*self.undoVarList[i])

class UID:
    def __init__(self):
        self.curId = -1
    def get(self):
        self.curId += 1
        return self.curId

class Bank:
    def __init__(self):
        pass

class Entity:
    '''Company, player, ipo, or pool'''
    def __init__(self,key,title,possessions=None):
        self.key = key
        self.possessions = {}
        self.title = title

    def acquirePossession(self,possessionId,num):
        try:
            self.possessions[possessionId] += num
#            if self.possessions[possessionId] == 0:
#                del(self.possessions[possessionId])
        except KeyError:
            self.possessions[possessionId] = num
            
    def dropPossession(self,pKey):
        del(self.possessions[pKey])
        

class NonBankEntity(Entity):
    '''Company or player. Something with private cash.'''
    def __init__(self,key,title,initCash,possessions=None):
        Entity.__init__(self,key,title,possessions)
        self.cash = initCash
        
    def changeCash(self,delta):
        self.cash += delta        
        
class Company(NonBankEntity):
    def __init__(self,key,title,initCash,shareNum,companyType,
                        canFiftyFifty = False, canOperate = True,
                        canWithhold = True, initialStockLocation = 'ipo',
                        canOwnOwnStock = False, canOwnTypes = [],
                        poolGoesToCompany = False, IPOGoesToCompany = False,
                        internalCompanyRevenue = True, 
                        revenueMode = 'normal',
                        shortingAllowed = False,
                        possessions = None,
                        stockDeltaLocation = None,
                        active = True
    
    ):
        NonBankEntity.__init__(self,key,title,initCash,possessions)
        self.colors = {'fg':'#000000','bg':'#ffffff'}
        self.shareNum = shareNum
        self.shareValue = 0
        self.lastRecordedRevenue = 0
        self.companyType = companyType
        self.canFiftyFifty = canFiftyFifty
        self.canOperate = canOperate
        self.canWithhold = canWithhold
        self.initialStockLocation = initialStockLocation ## ipo, pool, self
        self.canOwnOwnStock = canOwnOwnStock
        self.canOwnTypes = canOwnTypes
        self.poolGoesToCompany = poolGoesToCompany
        self.IPOGoesToCompany = IPOGoesToCompany
        self.internalCompanyRevenue = internalCompanyRevenue
        self.revenueMode = revenueMode ## normal, ur, brazil
        self.shortingAllowed = shortingAllowed
        self.stockDeltaLocation = stockDeltaLocation
        self.active = active
        
    def setColors(self,main,bg):
        self.colors['fg'] = main
        self.colors['bg'] = bg


class CompanyProfile:
    def __init__(self, companyType, canFiftyFifty = False, 
         canOperate = True, canWithhold = True, 
         initialStockLocation = 'pool', canOwnOwnStock = False, 
         canOwnTypes = [], poolGoesToCompany = False, 
         IPOGoesToCompany = False, internalCompanyRevenue = True, 
         revenueMode = 'normal',
         shortingAllowed = False,
         active = True,
         stockDeltaLocation = None
     ):
        self.companyType = companyType
        self.canFiftyFifty = canFiftyFifty
        self.canOperate = canOperate
        self.canWithhold = canWithhold
        self.initialStockLocation = initialStockLocation ## ipo, pool, self
        self.canOwnOwnStock = canOwnOwnStock
        self.canOwnTypes = canOwnTypes
        self.poolGoesToCompany = poolGoesToCompany
        self.IPOGoesToCompany = IPOGoesToCompany
        self.internalCompanyRevenue = internalCompanyRevenue
        self.revenueMode = revenueMode ## normal, ur, brazil
        self.shortingAllowed = shortingAllowed
        self.stockDeltaLocation = stockDeltaLocation
        self.active = True
        
    def createNewCompany(self,key,title,initCash,shareNum,active=True):
        return Company(key,title,initCash,shareNum,self.companyType,
            canFiftyFifty = self.canFiftyFifty,
            canOperate = self.canOperate, canWithhold = self.canWithhold, 
            initialStockLocation = self.initialStockLocation, 
            canOwnOwnStock = self.canOwnOwnStock,
            canOwnTypes = self.canOwnTypes,
            poolGoesToCompany = self.poolGoesToCompany,
            IPOGoesToCompany = self.IPOGoesToCompany,
            internalCompanyRevenue = self.internalCompanyRevenue,
            revenueMode = self.revenueMode, ## normal, ur, brazil
            shortingAllowed = self.shortingAllowed, 
            stockDeltaLocation = self.stockDeltaLocation, ## None (== default to initialStockLocation), ipo, pool, self
            active = active
        )
        

class Player(NonBankEntity):
    '''Simple reskin of NonBankEntity (a player is nothing special
    beyond that)
    '''
    pass

class Accounter:
    def setup(self,scenario,companyProfiles,separateIPO=True,currencySign = '$'):
        self.logbook = []
        self.keygen = UID()
        self.startTime = datetime.datetime.today()
        ## Options (18xx-specific stuff)
        ## delegated to Company (and companyProfile)
#        self.poolGoesToCompany = False
#        self.IPOGoesToCompany = False
#        self.internalCompanyRevenue = True
#        self.ownOwnStockByDefault = True
#        self.urMode = False
#        self.shortingAllowed = False
        self.companyProfiles = dict((c.companyType,c) for c in companyProfiles)
        ## </options>
        self.bankSize = 0
        self.players = {}
        self.companies = {}
        self.bankEntities = {}
        self.allEntities = {}
        if separateIPO:
            self.ipo = Entity(self.keygen.get(),'IPO')
            self.bankEntities[self.ipo.key] = self.ipo
            self.allEntities[self.ipo.key] = self.ipo
        else:
            self.ipo = None
        self.pool = Entity(self.keygen.get(),'Pool')
        self.bankEntities[self.pool.key] = self.pool
        self.allEntities[self.pool.key] = self.pool
        self.currencySign = currencySign
        self.scenario = scenario

    def __getitem__(self,key):
        return self.__dict__[key]

    def __setitem__(self,key,value):
        self.__dict__.__setitem__(key,value)

    def serialize(self,filename):
        with open(filename,'wb') as outFile:
            pkl.dump(self.__dict__,outFile)

    def load(self,filename):
        with open(filename,'rb') as inFile:
            self.__dict__ = pkl.load(inFile,fix_imports=True)
#            print self.__dict__

    def doAccounts(self):
        moneyUsed = 0
        for player in self.players.values():
            moneyUsed += player.cash
        for company in self.companies.values():
            if company.active:
                moneyUsed += company.cash
        return self.bankSize-moneyUsed

    def addPlayer(self,pName,startingFunds,rezData=None):
        if rezData == None:
            pKey = self.keygen.get()
            newPlayer = Player(pKey,pName,startingFunds)
            for company in self.companies.values():
                newPlayer.possessions[company.key] = 0
        else:
            pKey = rezData['key']
            newPlayer = Player(pKey,'',-1)
            newPlayer.__dict__ = rezData
        self.players[pKey] = newPlayer
        self.allEntities[pKey] = newPlayer

    def addCompany(self,
               companyName,companyProfileName,totalStock,fg='#000000',bg='#ffffff',active=True
       ):
        companyProfile = self.companyProfiles[companyProfileName]
        newCompany = companyProfile.createNewCompany(self.keygen.get(),companyName,0,totalStock)
        self.companies[newCompany.key] = newCompany
        self.allEntities[newCompany.key] = newCompany
        for entity in self.allEntities.values():
            entity.possessions[newCompany.key] = 0
        for company in self.companies.values():
            newCompany.possessions[company.key] = 0
        tgt = None
        if newCompany.initialStockLocation == 'pool':
            tgt = self.pool
        elif newCompany.initialStockLocation == 'self':
            tgt = newCompany
        elif newCompany.initialStockLocation == 'ipo':
            tgt = self.ipo
        else:
            raise ValueError
        tgt.acquirePossession(newCompany.key,newCompany.shareNum)
        self.setCompanyColors(newCompany.key,fgColor=fg,bgColor=bg)
        newCompany.active = active

#    def editCompany(self,cKey,cName,shareNum,companyType,
#                        canFiftyFifty, canOperate,
#                        canWithhold, initialStockLocation,
#                        canOwnOwnStock, canOwnTypes,
#                        poolGoesToCompany, IPOGoesToCompany,
#                        internalCompanyRevenue, 
#                        revenueMode,
#                        shortingAllowed):
#        print locals() 
#        company = self.companies[cKey]
#        OGparams = company.__dict__.copy()
##        stockDiff = stockNum - originalStockNum
#        cInd = self.companyNames.index(cName)
#        self.companyTotalStock[cName] += stockDiff
#        if ownIPO:
#            self.companies[cName][cInd+1] += stockDiff
#        else:
#            self.stockPool[cInd] += stockDiff

    def editCompany(self,cKey,shareNum):
        company = self.companies[cKey]
        originalshareNum = company.shareNum
        shareNumDiff = shareNum - originalshareNum
        company.shareNum += shareNumDiff
        tgt = None
        if company.stockDeltaLocation != None:
            location = company.stockDeltaLocation
        else:
            location = company.initialStockLocation
        if location == 'pool':
            tgt = self.pool
        elif location == 'self':
            tgt = company
        elif location == 'ipo':
            tgt = self.ipo
        else:
            raise ValueError
        tgt.acquirePossession(company.key,shareNumDiff)

    def changeStockHoldings(self,entityKey,stockCompanyKey,stockDelta):
        self.allEntities[entityKey].acquirePossession(stockCompanyKey,stockDelta)

    def changeMoney(self,entityKey,moneyDelta):
        if entityKey not in self.bankEntities.keys():
#            print entityKey
#            print self.allEntities.items()
            self.allEntities[entityKey].cash += int(moneyDelta)

    def findStocks(self,companyKey):
        if self.ipo == None:
            inIPO = None
        else:
            inIPO = self.ipo.possessions[companyKey]
        inPool = self.pool.possessions[companyKey]
        playerHeld = []
        companyHeld = []
        for playerKey,player in self.players.items():
            playerHeld.append((player.key,player.possessions[companyKey]))
        for holdingCompanyKey,company in self.companies.items():
            companyHeld.append((company.key,company.possessions[companyKey]))
        return (inIPO,inPool,playerHeld,companyHeld)

    def removeCompany(self,companyKey):
        ## remove references to company in player holdings.
        for entity in self.allEntities.values():
            entity.dropPossession(companyKey)
            ## here, add a list of functions and arguments to return later
            ## to make it reversible
        self.companies.__delitem__(companyKey)
        self.allEntities.__delitem__(companyKey)  
        return None

    def removePlayer(self,playerKey,assetSink):
        self.players.__delitem__(playerKey)
        self.allEntities.__delitem__(playerKey)
        ## free up all the stuff owned by the player to assetSink
        ## (probably ought to default to pool)
        return []

    def calcNetWorth(self,playerKey):
        player = self.players[playerKey]
        totalWorth = player.cash
        for company in self.companies.values():
            totalWorth += player.possessions[company.key] * company.shareValue
        return (player.title,totalWorth)

    def getTotalHeldStock(self,companyKey):
        company = self.companies[companyKey]
        if company.revenueMode == 'normal':
            totalHeld = company.shareNum
        else:
            totalHeld = self.pool.possessions[companyKey] + sum(
                    [
                            entity.possessions[companyKey]
                            for entity
                            in self.allEntities.values()
                    ]
            )
        return totalHeld
    
    def getCompanyKeys(self):
        return sorted(self.companies.keys())
    
    def getPlayerKeys(self):
        return sorted(self.players.keys())
    
    def rezPlayer(self,pData):
        rezzedPlayer = Player(-1,'',-1)
        self.addPlayer('',0,rezData = pData)
        rezzedPlayer.__dict__ = pData
        
    def rezCompany(self,cData):
        rezzedCompany = Company(-1,'',-1,1,'')
        rezzedCompany.__dict__ = cData
       
    def getPlayerData(self,pKey):
        player = self.players[pKey]
        return player.__dict__
    
    def getCompanyData(self,cKey):
        company = self.companies[cKey]
        return company.__dict__
    
    def getCompanyColor(self,companyKey,fgbg):
        company = self.companies[companyKey]
        return company.colors[fgbg]    
    
    def eName(self,eKey):
        return self.allEntities[eKey].title
    
    def cKey(self,companyName):
        return [
            c.key for c in self.companies.values()
            if c.title == companyName
        ][-1]
    
    def cName(self,cKey):
        return self.companies[cKey].title
    
    def getStockValue(self,companyKey):
        return self.companies[companyKey].shareValue
    
    def getSaleableCompanies(self,entityKey):
        entity = self.allEntities[entityKey]
        relevantCompanies = [
            relevantEntityKey
            for relevantEntityKey,holding
            in entity.possessions.items()
            if holding > 0 or self.allEntities[relevantEntityKey].shortingAllowed
        ]
        return relevantCompanies
    
    def getBuyableCompanies(self,entityKey,entityType):
        if entityType in ['player','bank']:
            return self.getCompanyKeys()
        else:
            entity = self.allEntities[entityKey]
            ownableTypes = entity.canOwnTypes
            relevantCompanies = [
                companyKey
                for companyKey,company
                in self.companies.items()
                if (company.companyType in ownableTypes) or (
                        entity.canOwnOwnStock and companyKey == entityKey) and
                (entity.possessions[companyKey] != company.shareNum)
            ]
        return relevantCompanies
    
    def isTradeValid(self,shareCompanyKey,sourceEntityKey,targetEntityKey,
                     shareNum,totalAmount):
        sourceEntityShares = self.allEntities[sourceEntityKey].possessions[shareCompanyKey]
        if targetEntityKey not in self.bankEntities.keys():
            targetEntityMoney = self.allEntities[targetEntityKey].cash
            enoughMoney = targetEntityMoney >= totalAmount
        else:
            enoughMoney = True
        enoughShares = (sourceEntityShares >= shareNum) or self.companies[shareCompanyKey].shortingAllowed
        if enoughShares and enoughMoney:
            return True
        else:
            return False
        
    def payDividend(self,amount,companyKey):
        totalHeld = self.getTotalHeldStock(companyKey)
        company = self.companies[companyKey]
        incomePerStock = amount/float(totalHeld)
        landedMoney = 0
        if company.poolGoesToCompany:
            income = int(self.pool.possessions[companyKey]*incomePerStock)
            self.changeMoney(
                companyKey,income
            )
        if company.IPOGoesToCompany:
            income = int(self.ipo.possessions[companyKey]*incomePerStock)
            self.changeMoney(
                companyKey,income
            )
        for player in self.players.values():
            income = int(player.possessions[companyKey]*incomePerStock)
            self.changeMoney(
                player.key,income
            )
            landedMoney+=income
        
#            print 'company revenuing'
        for receivingCompany in self.companies.values():
            if receivingCompany.internalCompanyRevenue:
                income = int(receivingCompany.possessions[companyKey]*incomePerStock)
                self.changeMoney(
                    receivingCompany.key,income
                )
                landedMoney+=income
#        remainder = amount - self.companyTotalStock[companyName] * incomePerStock
        landedMoney += int(self.pool.possessions[companyKey]*incomePerStock)
        if company.revenueMode == 'normal' and self.ipo != None:
            landedMoney += int(self.ipo.possessions[companyKey]*incomePerStock)
        remainder = int(amount - landedMoney)
#        print remainder
        self.changeMoney(companyKey,remainder)

    def companyDividend(self,companyKey,totalIncome,kind='100%'):
#        self.log('Operating {0} for ${1}; withhold = {2}',companyName,totalIncome,withhold)
#        companyIndex = self.companyNames.index(companyName)
#        self.lastRecordedRevenues[companyName] = totalIncome
        company = self.companies[companyKey]
        if kind == '100%':
            assert company.canOperate
            self.payDividend(totalIncome,companyKey)
        elif kind == 'withhold':
            assert company.canWithhold
            self.changeMoney(companyKey,totalIncome)
        elif kind == '50/50':
            assert company.canFiftyFifty
            half = int(totalIncome / 2.)
            if (    company.shareNum == 10 
                    and (totalIncome % 10 == 0) 
                    and company.revenueMode == 'normal'
                ):
                remainder = ((totalIncome / 10) % 2) * 5
                remainder = remainder * -1 if totalIncome < 0 else remainder
                self.payDividend(half + remainder,companyKey)
                self.changeMoney(companyKey,half - remainder)
            else:
                remainder = totalIncome - (2*half)
                self.payDividend(half,companyKey)
                self.changeMoney(companyKey,half + remainder)

    def setStockValue(self,companyKey,value):
        self.companies[companyKey].shareValue = value

    def setOption(self,optionVar,optionVal):
        raise NYIError
        if optionVar == 'poolGoesToCompany':
#            self.log('Setting dividend from pool going to company (1830-style ) to {0}.',optionVal)
            self.poolGoesToCompany = optionVal
        elif optionVar == 'IPOGoesToCompany':
#            self.log('Setting dividend from IPO going to company (1830-style ) to {0}.',optionVal)
            self.IPOGoesToCompany = optionVal
        elif optionVar == 'ownOwnStockByDefault':
            self.ownOwnStockByDefault = optionVal
        elif optionVar == 'internalCompanyRevenue':
            self.internalCompanyRevenue = optionVal
        elif optionVar == 'urMode':
            self.urMode = optionVal
        else:
            raise ValueError('Unknown option: {0}'.format(optionVar))

    def setCompanyColors(self,companyKey,fgColor,bgColor):
        company = self.companies[companyKey]
        company.colors['fg'] = fgColor
        company.colors['bg'] = bgColor

class IntegerEntry(tki.Entry):
    def __init__(self,*args,**keywords):
        self.targetDict = {}
        tki.Entry.__init__(self,*args,**keywords)
        self.entryVar = keywords['textvariable']
        self.bind('<Button-3>',self.popDialog)
    def popDialog(self,event):
#        print self.__dict__
        integerDialog = IntegerDialog(self,self.targetDict,self.entryVar)
#        print 'derp!'
#        print self.master

class Mbox(object):
    root = None
    def __init__(self, msg, targetDict, targetKeys, targetFunc = None, typeCast = [None]):
        assert len(msg) == len(targetKeys)
        self.targetDict = targetDict
        self.typeCast = typeCast
        self.targetKeys = targetKeys
        self.targetFunc = targetFunc
        self.top = tki.Toplevel(Mbox.root)
#        tki.Toplevel()
        self.entries = []
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        for i,dictKey in enumerate(self.targetKeys):
            label = tki.Label(self.frm, text=msg[i])
            label.grid(row=i,column=0,padx=PADVAL,pady=PADVAL)
            thisEntry = tki.Entry(self.frm)
            self.entries.append(thisEntry)
            thisEntry.grid(row=i,column=1,padx=PADVAL,pady=PADVAL)
        self.b_submit = tki.Button(self.frm, text='Submit',command = self.entry_to_dict)
        self.b_submit.grid(row=i+1,column=0,padx=PADVAL,pady=PADVAL)
        self.top.bind('<Key-Return>',self.entry_to_dict)
        self.top.bind('<Key-KP_Enter>',self.entry_to_dict)
        b_cancel = tki.Button(self.frm, text='Cancel')
        b_cancel['command'] = self.top.destroy
        b_cancel.grid(row=i+1,column=1,padx=PADVAL,pady=PADVAL)
        self.entries[0].focus_set()

    def entry_to_dict(self,event=None):
        goToExit = [False for i in self.targetKeys]
        for i,targetKey in enumerate(self.targetKeys):
            data = self.entries[i].get()
            if data:
                if self.typeCast[i] == int:
                    try:
                        data = int(data)
                        self.targetDict[targetKey] = data
                        goToExit[i] = True
                    except ValueError:
                        showinfo(message = 'Must enter an integer in box {0}.\n(You smegmerchant).'.format(i+1))
                else:
                    self.targetDict[targetKey] = data
                    goToExit[i] = True
        if all(goToExit):
            if self.targetFunc is not None:
                self.targetFunc()
            self.top.destroy()

def incrementer(self,multiple,i):
    try:
        v = self.textvariable.get()
        v = int(v)
        v += multiple * (10**(6-i))
        self.textvariable.set(str(v))
    except ValueError:
        self.textvariable.set(self.origVal)

class IntegerDialog:
    def __init__(self,superEntry,targetDict,textvariable):
        self.mainFont = tkFont.Font(size=27)
        self.tinyFont = tkFont.Font(size=14)
        self.orders = 3
        self.superEntry = superEntry
        self.textvariable = textvariable
        self.textvariable.set(['0',self.textvariable.get()][self.textvariable.get()!=''])
        self.origVal = self.textvariable.get()
        self.top = tki.Toplevel(self.superEntry.master)
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.grid(row = 0,column=0)
        self.entry = tki.Entry(self.frm,textvariable=self.textvariable,font=self.mainFont,justify='right',width=7)
        self.entry.grid(row = 3,column=0,columnspan=self.orders)
        self.entry.focus_set()
        for i in range(self.orders):
            def thisButton(i=i):
                self.incrementer(1,i)
            plusButton = tki.Button(self.frm,text='+1' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=2)
            def thisButton(i=i):
                self.incrementer(2,i)
            plusButton = tki.Button(self.frm,text='+2' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=1)
            def thisButton(i=i):
                self.incrementer(5,i)
            plusButton = tki.Button(self.frm,text='+5' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=0)
            def thisButton(i=i):
                self.incrementer(-1,i)
            plusButton = tki.Button(self.frm,text='-1' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=4)
            def thisButton(i=i):
                self.incrementer(-2,i)
            plusButton = tki.Button(self.frm,text='-2' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=5)
            def thisButton(i=i):
                self.incrementer(-5,i)
            plusButton = tki.Button(self.frm,text='-5' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=6)
        confirmButton = tki.Button(self.frm,text='Confirm',command=self.top.destroy)
        confirmButton.grid(column=0,row=7,padx=PADVAL,pady=PADVAL,columnspan=3)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.cancel)
        cancelButton.grid(column=2,row=7,padx=PADVAL,pady=PADVAL,columnspan=3)
        resetButton = tki.Button(self.frm,text='Set at 0',command=self.reset,height=7)
        resetButton.grid(column=3,row=0,padx=PADVAL,pady=PADVAL,rowspan=7)
    def cancel(self,event=None):
        self.textvariable.set(self.origVal)
        self.top.destroy()
    def confirm(self,event=None):
        self.top.destroy()
    def reset(self,event=None):
        self.textvariable.set('0')
    def incrementer(self,multiple,i):
        try:
            v = self.textvariable.get()
            v = int(v)
            v += multiple * (10**(self.orders-1-i))
            self.textvariable.set(str(v))
        except ValueError:
            self.textvariable.set(self.origVal)


class AddCompanyDialog:
    root = None
    def __init__(self,accounter,addCompanyFunc):
        self.top = tki.Toplevel(AddCompanyDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.mainColor = '#000000'
        self.bgColor = '#c0c0c0'
        self.stockVar = tki.StringVar(value='10')
        self.cNameVar = tki.StringVar()
        self.ownIPOVar = tki.BooleanVar(value=accounter['ownOwnStockByDefault'])
        self.addCompanyFunc = addCompanyFunc
        nameLabel = tki.Label(self.frm,text='Name:')
        nameLabel.grid(column = 0, row = 0,padx=PADVAL,pady=PADVAL,sticky='W')
        nameEntry = tki.Entry(self.frm,textvariable = self.cNameVar)
        nameEntry.grid(column = 1, row = 0,padx=PADVAL,pady=PADVAL,sticky='W')
        stockNumLabel = tki.Label(self.frm,text='Number of stocks:')
        stockNumLabel.grid(column = 0, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        stockNumEntry = IntegerEntry(self.frm,textvariable=self.stockVar,width=3)
        stockNumEntry.grid(column = 1, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        ownIPOLabel = tki.Label(self.frm,text='Owns own stock')
        ownIPOLabel.grid(column = 0, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        ownIPOCheck = tki.Checkbutton(self.frm,variable=self.ownIPOVar)
        ownIPOCheck.grid(column = 1, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        colorChooserButton1 = tki.Button(self.frm,text='Main Color',command = self.pickMainColor)
        colorChooserButton1.grid(column = 0, row = 3,padx=PADVAL,pady=PADVAL,sticky='E')
        colorChooserButton2 = tki.Button(self.frm,text='Background Color',command = self.pickBackgroundColor)
        colorChooserButton2.grid(column = 1, row = 3,padx=PADVAL,pady=PADVAL,sticky='W')
        confirmButton = tki.Button(self.frm,text='Confirm',command=self.confirm)
        confirmButton.grid(column=0,row=4,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=4,padx=PADVAL,pady=PADVAL)
        nameEntry.focus_set()
    def pickMainColor(self):
        void,self.mainColor = tkColorChooser.askcolor(color=self.mainColor,parent=self.top)
    def pickBackgroundColor(self):
        void,self.bgColor = tkColorChooser.askcolor(color=self.bgColor,parent=self.top)
    def confirm(self,event=None):
        cName = self.cNameVar.get()
        try:
            stockNum = int(self.stockVar.get())
        except ValueError:
            showinfo(message = u'Stock number must be an integer.\n(You smelly viking.)'.format(cName))
        self.addCompanyFunc(cName,stockNum,self.ownIPOVar.get(),self.mainColor,self.bgColor)
#        self.accounter.addCompany()
#        self.accounter.setCompanyColors()
        self.top.destroy()

class EditCompanyDialog:
    root = None
    def __init__(self,accounter,editCompanyFunc,cKey):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.mainColor = self.accounter.getCompanyColor(cKey,'fg')
        self.bgColor = self.accounter.getCompanyColor(cKey,'bg')
        OGShareNum = accounter.companies[cKey].shareNum
        self.stockVar = tki.StringVar(value=str(OGShareNum))
        self.cNameVar = tki.StringVar()
        self.cKey = cKey
        self.editCompanyFunc = editCompanyFunc
        stockNumLabel = tki.Label(self.frm,text='Set number of stocks to:')
        stockNumLabel.grid(column = 0, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        stockNumEntry = IntegerEntry(self.frm,textvariable=self.stockVar,width=3)
        stockNumEntry.grid(column = 1, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
#        ownIPOLabel = tki.Label(self.frm,text='Stock source is company (pool otherwise)')
#        ownIPOLabel.grid(column = 0, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
#        ownIPOCheck = tki.Checkbutton(self.frm,variable=self.ownIPOVar)
#        ownIPOCheck.grid(column = 1, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        colorChooserButton1 = tki.Button(self.frm,text='Main Color',command = self.pickMainColor)
        colorChooserButton1.grid(column = 0, row = 3,padx=PADVAL,pady=PADVAL,sticky='E')
        colorChooserButton2 = tki.Button(self.frm,text='Background Color',command = self.pickBackgroundColor)
        colorChooserButton2.grid(column = 1, row = 3,padx=PADVAL,pady=PADVAL,sticky='W')
        confirmButton = tki.Button(self.frm,text='Confirm',command=self.confirm)
        confirmButton.grid(column=0,row=4,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=4,padx=PADVAL,pady=PADVAL)
        stockNumEntry.focus_set()
    def pickMainColor(self):
        void,self.mainColor = tkColorChooser.askcolor(color=self.mainColor,parent=self.top)
    def pickBackgroundColor(self):
        void,self.bgColor = tkColorChooser.askcolor(color=self.bgColor,parent=self.top)
    def confirm(self,event=None):
        try:
            stockNum = int(self.stockVar.get())
        except ValueError:
            showinfo(message = 'Stock number must be an integer.')
        self.editCompanyFunc(self.cKey,stockNum,self.mainColor,self.bgColor)
        self.top.destroy()

class TransferMoneyDialog:
    root = None
    def __init__(self,entityType,entityKey,accounter):
        self.top = tki.Toplevel(TransferMoneyDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.accounter = accounter
        self.textvariable = tki.StringVar(value='')
        self.entityType = entityType
        self.entityKey = entityKey
        self.bankFrame = tki.LabelFrame(self.frm,text='Bank')
        self.bankFrame.grid(row=1,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        self.playerFrame = tki.LabelFrame(self.frm,text='Players')
        self.playerFrame.grid(row=2,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        self.companyFrame = tki.LabelFrame(self.frm,text='Companies')
        self.companyFrame.grid(row=3,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        self.reasonFrame = tki.LabelFrame(self.frm,text='Reason')
        self.reasonFrame.grid(row=4,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        label1 = tki.Label(self.frm,text='Transfer {0}'.format(self.accounter['currencySign']))
        label1.grid(row = 0,column=0)
        self.entry = IntegerEntry(self.frm,justify='right',textvariable=self.textvariable)
        self.entry.grid(row = 0,column=1)
        self.entry.focus_set()
        label2 = tki.Label(self.frm,text='to')
        label2.grid(row = 0,column=2)
        self.target = tki.StringVar()
        self.target.set('bank.{0}'.format(self.accounter.pool.key))
        self.reason = tki.StringVar()
        self.reason.set('another reason')
        thisRB = ttk.Radiobutton(
                self.bankFrame,
                value='bank.{0}'.format(self.accounter.pool.key),
                text='Bank',
                variable=self.target,
                ## Once a radioButton is clicked, set up the sources.
#                style = '{0}.TRadiobutton'.format(companyName),
#                command = self.onRB2
        )
        thisRB.grid(column=0,row=0)
        for i,cKey in enumerate(self.accounter.getCompanyKeys()):
            company = self.accounter.companies[cKey]
#            cInd = self.accounter['companyNames'].index(companyName)
            if company.active:
                rbStyle = ttk.Style()
                rbStyle.configure(
                    '{0}.TRadiobutton'.format(company.title),
                    foreground = company.colors['fg'],
                    background = company.colors['bg']
                )
                thisRB = ttk.Radiobutton(
                    self.companyFrame,
                    value='company.{0}'.format(cKey),text='{0}'.format(company.title),
                    variable=self.target,
                    ## Once a radioButton is clicked, set up the sources.
                    style = '{0}.TRadiobutton'.format(company.title),
    #                command = self.onRB2
                )
                thisRB.grid(column=i,row=0)

        for i,(playerKey,player) in enumerate(self.accounter.players.items()):
            thisRB = ttk.Radiobutton(
                self.playerFrame,
                value='player.{0}'.format(playerKey),
                text='{0}'.format(player.title),
                variable=self.target,
                ## Once a radioButton is clicked, set up the sources.
#                style = '{0}.TRadiobutton'.format(companyName),
#                command = self.onRB2
            )
            thisRB.grid(column=i,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='train(s)',
            text='train(s)',
            variable=self.reason,
        )
        thisRB.grid(column=0,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='track laying/tokening',
            text='track laying/tokening',
            variable=self.reason,
        )
        thisRB.grid(column=1,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='loan/bond/interest',
            text='loan/bond/interest',
            variable=self.reason,
        )
        thisRB.grid(column=2,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='another reason',
            text='another reason',
            variable=self.reason,
        )
        thisRB.grid(column=3,row=0)
        confirmButton = tki.Button(self.frm,text='Transfer',command=self.confirm)
        confirmButton.grid(column=0,row=5,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=2,row=5,padx=PADVAL,pady=PADVAL)

    def confirm(self,event=None):
#        try:
        amount = int(self.entry.get())
        targetType,targetKey = self.target.get().split('.')
        targetKey = int(targetKey)
#            if self.entityType == 'player':
        funcList = [
                self.accounter.changeMoney,
                self.accounter.changeMoney
        ]
        doVarList = [
                [self.entityKey,-amount],
                [targetKey,amount]
        ]
        undoVarList = [
                [self.entityKey,amount],
                [targetKey,-amount]
        ]
        Memento(
            "{0} transferred {1}{2} to {3} for {4}.".format(
                    self.accounter.eName(self.entityKey),
                    self.accounter['currencySign'],
                    amount,
                    self.accounter.eName(targetKey),
                    self.reason.get()
                ),
            funcList,
            doVarList,
            funcList,
            undoVarList
        )
        self.root.updateLabels()
        self.top.destroy()
#        except ValueError:
#            showinfo(message = 'Must enter an amount in integers.')

class LoadScenarioDialog:
    root = None
    def __init__(self,accounter,entityType='bank',entityName='bank'):
        self.top = tki.Toplevel(LoadScenarioDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
#        self.top.bind('<Key-Return>',self.confirm)
#        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.accounter = accounter
        defaults = ['' for i in range(10)]
        defaults[:len(DEFAULT_PLAYERS)] = DEFAULT_PLAYERS
        self.startingcashvariable = tki.StringVar(value='0')
        self.textvariables = [tki.StringVar(value=v) for v in defaults]
        self.playerEntries = [tki.Entry(self.frm,textvariable=v) for v in self.textvariables]
        label1 = tki.Label(self.frm,text='Enter player names:')
        label1.grid(row = 0,column=0)
        for i,pE in enumerate(self.playerEntries):
            pE.grid(column = 0, row = i+1,padx=PADVAL,pady=PADVAL)
        label2 = tki.Label(self.frm,text='Enter starting cash per player\nfor the number of players entered:')
        label2.grid(column = 0,row=len(self.playerEntries)+2,padx=PADVAL,pady=PADVAL)
        self.entry = IntegerEntry(self.frm,justify='right',textvariable=self.startingcashvariable)
        self.entry.grid(column = 0,row=len(self.playerEntries)+3,padx=PADVAL,pady=PADVAL)
        self.playerEntries[0].focus_set()
        confirmButton = tki.Button(self.frm,text='Start!',command=self.confirm)
        confirmButton.grid(column=0,row=len(self.playerEntries)+4,padx=PADVAL,pady=PADVAL)
        
    def confirm(self,event=None):
        money = int(self.startingcashvariable.get())
        for v in self.textvariables:
            pName = v.get()
            if pName != '':
                self.accounter.addPlayer(pName,money)
        self.root.updateGUI()
        self.top.destroy()


class PickCompanyDialog:
    root = None
    def __init__(self,entityType,accounter,removeFunc,kind):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.entityType = entityType
        self.removeFunc = removeFunc
        text = ['Deactivate:','Delete:'][kind == 'delete']
        lbl = tki.Label(self.frm,text = text)
        lbl.grid(column=0,row=0,sticky='W',padx=PADVAL,pady=PADVAL)
        if self.entityType == 'player':
            self.playerVars = {}
            for i,playerName in enumerate(self.accounter['players'].keys()):
                thisVar = tki.IntVar()
                cb = tki.Checkbutton(self.frm,text=playerName,variable=thisVar)
                cb.grid(column=0,row=i+1,sticky='W',padx=PADVAL,pady=PADVAL)
                cb.deselect()
                self.playerVars[playerName] = thisVar
                varLen = i+1
        elif self.entityType == 'company':
            self.companyVars = {}
            varLen = 0
            for i,cKey in enumerate(self.accounter.getCompanyKeys()):
                if kind == 'delete' or self.accounter.companies[cKey].active:
                    divisor = 16
                    row = i%divisor
                    column = int(i/divisor)
                    cName = self.accounter.companies[cKey].title
                    thisVar = tki.IntVar()
                    cb = tki.Checkbutton(self.frm,text=cName,variable=thisVar,fg=self.accounter.getCompanyColor(cKey,'fg'),bg=self.accounter.getCompanyColor(cKey,'bg'))
                    cb.grid(column=column,row=row+1,sticky='W',padx=PADVAL,pady=PADVAL)
                    cb.deselect()
                    self.companyVars[cKey] = thisVar
                    varLen = i+1
        confirmButton = tki.Button(self.frm,text='Select',command=self.confirm)
        confirmButton.grid(column=0,row=varLen+2,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=varLen+2,padx=PADVAL,pady=PADVAL)
        
    def confirm(self,event=None):
        if self.entityType == 'player':
            for playerName,killVar in self.playerVars.items():
                if killVar.get() == 1:
                    self.removeFunc(playerName)
#                    self.accounter.removePlayer(playerName)
        elif self.entityType == 'company':
            for companyName,killVar in self.companyVars.items():
                if killVar.get() == 1:
#                    print 'killing {0}'.format(companyName)
                    self.removeFunc(companyName)
        self.top.destroy()

class NetWorthDialog(object):
    root = None
    def __init__(self,accounter):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.bigFont = tkFont.Font(size=20)
        listy = []
        for i,pKey in enumerate(self.accounter['players'].keys()):
            pName,pVal = self.accounter.calcNetWorth(pKey)
            listy.append([pVal,pName])
        listy.sort(reverse=True)
        now = datetime.datetime.today()
        duration = now - self.accounter.startTime
        durationSeconds = duration.seconds
#        print duration.seconds
        hoursTaken = int(durationSeconds / 3600)
        minutesTaken = int(durationSeconds / 60) - hoursTaken * 60
        secondsTaken = durationSeconds%60
        timeTaken = 'Time taken: {0}h {1}m {2}s'.format(hoursTaken,minutesTaken,secondsTaken)
        timeTaken = '' 
        idText = self.accounter.scenario + '\n'+now.strftime('%Y/%m/%d %H:%M')+'\n'+timeTaken
        idLabel = tki.Label(self.frm,text = idText,font=self.bigFont)
        idLabel.grid(column=0,row=0,columnspan=2)
        for i,(netWorth,pName) in enumerate(listy):
            pNameLbl = tki.Label(self.frm,text=pName+':',font=self.bigFont)
            pNameLbl.grid(column=0,row=i+1,sticky='W',padx=PADVAL,pady=PADVAL)
            pValLbl = tki.Label(self.frm,text = accounter['currencySign']+str(netWorth),font=self.bigFont)
            pValLbl.grid(column=1,row=i+1,sticky='E',padx=PADVAL,pady=PADVAL)

class SetStockValueDialog(object):
    root = None
    def __init__(self,accounter):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.companyVars = {}
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        divisor = 16
        for i,cKey in enumerate(self.accounter.getCompanyKeys()):
            row = i%divisor
            column = int(i/divisor)*2
            cName = self.accounter.cName(cKey)
            fg = self.accounter.getCompanyColor(cKey,'fg')
            bg = self.accounter.getCompanyColor(cKey,'bg')
            cNameLbl = tki.Label(self.frm,text=cName,fg=fg,bg=bg)
            cNameLbl.grid(column=column,row=row,sticky='W',padx=PADVAL,pady=PADVAL)
            thisVar = tki.StringVar()
            thisVar.set(str(self.accounter.getStockValue(cKey)))
            self.companyVars[cKey] = thisVar
            entry = IntegerEntry(self.frm,textvariable=thisVar,width=5)
            entry.grid(column=column+1,row=row,sticky='W',padx=PADVAL,pady=PADVAL)
            varLen = i+1
        confirmButton = tki.Button(self.frm,text='Set',command=self.confirm)
        confirmButton.grid(column=0,row=varLen+2,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=varLen+2,padx=PADVAL,pady=PADVAL)
    def confirm(self,event=None):
        goToExit = dict((cKey,False) for cKey in self.companyVars.keys())
        for cKey,cVar in self.companyVars.items():
            try:
                data = int(cVar.get())
                self.accounter.setStockValue(cKey,data)
                goToExit[cKey] = True
            except ValueError:
                showinfo(message = u'Must enter an integer for {0}.\n(Thou maternal fornicator).'.format(self.accounter.cName(cKey)))
        if all(goToExit.values()):
            self.top.destroy()
            nwd = NetWorthDialog(self.accounter)


class StockChangeDialog(object):
    root = None
    def __init__(self,activeEntity,accounter,entityType,buysell):
        self.actor = (activeEntity,entityType)
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.accounter = accounter
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.buysell = buysell#tki.IntVar(value=0)
        self.tofrom = tki.StringVar(value='from')
        self.sharenum = tki.StringVar(value='1')
        self.relevantCompany = tki.IntVar()
        self.totalCash = tki.StringVar(value='0')
        self.stockVar = tki.StringVar()
        ## First row: Buy / sell radiobutton
#        r1f = tki.Frame(self.frm)
#        r1f.grid(row=0,column=0)
#        rabb = ttk.Radiobutton(r1f,value=0,text='Buy',variable=self.buysell,command=self.onRB1)
#        rabb.grid(column=0,row=0)
#        rabs = ttk.Radiobutton(r1f,value=1,text='Sell',variable=self.buysell,command=self.onRB1)
#        rabs.grid(column=1,row=0)
        ## First row: Entry of number of shares (self.sharenum; defaults to 1)
        r2f = tki.Frame(self.frm)
        r2f.grid(row=1,column=0)
        shNumberEntr = IntegerEntry(r2f,textvariable=self.sharenum,width=2)
        shNumberEntr.grid(column=0,row=1)
        lbl = tki.Label(r2f,text=' shares of')
        lbl.grid(column=1,row=1)
        ## Second row: getting the relevantCompany.
        self.setupRelevantCompanyRBs()
        ## Third row: just to or from ^_^
        blbl = tki.Label(self.frm,textvariable=self.tofrom)
        blbl.grid(column=0,row=3)
        ## Fourth: source of the relevantCompany.
        ## Is filled out by command onRB2 upon selecting the relevant company.
        self.sourceFrame = tki.Frame(self.frm)
        self.sourceFrame.grid(column=0,row=4)
        klbl = tki.Label(self.sourceFrame,text='[sources]')
        klbl.grid(column=0,row=3)
        ## Sixth row: price row.
        r5f = tki.Frame(self.frm)
        r5f.grid(row=5,column=0)
        p1lbl = tki.Label(r5f,text='For {0} '.format(accounter['currencySign']))
        p1lbl.grid(row=0,column=0)
        p2lbl = tki.Label(r5f,text=' per share.')
        p2lbl.grid(row=0,column=2)
        self.priceEntry = IntegerEntry(r5f,width=4,textvariable=self.stockVar)
        self.priceEntry.grid(row=0,column=1)
        ## Last row: buttons.
        r6f = tki.Frame(self.frm)
        r6f.grid(row=6,column=0)
        confirmButton = tki.Button(r6f,text='Confirm',command=self.confirm)
        confirmButton.grid(column=0,row=0,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(r6f,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=0,padx=PADVAL,pady=PADVAL)
        self.fromVar = tki.StringVar(value=self.accounter.pool.key)
        self.tofrom.set(['from','to'][self.buysell=='sell'])

    def setupRelevantCompanyRBs(self):
        (activeEntity,entityType) = self.actor
        self.r3f = tki.Frame(self.frm)
        self.r3f.grid(row=2,column=0)
        ## Only in case of a sale is the number of relevantCompanies limited,
        ## and then only if shorts are not allowed.
        if self.buysell == 'sell':
            relevantCompanies = self.accounter.getSaleableCompanies(activeEntity)
        ## Buying? Check what the actor may buy.
        else:
            relevantCompanies = self.accounter.getBuyableCompanies(activeEntity,entityType)
        ## Set up the actual radioButtons.
        if len(relevantCompanies) > 0:
            for i,companyKey in enumerate(relevantCompanies):
                company = self.accounter.companies[companyKey]
                companyName = company.title
                if company.active:
                    rbStyle = ttk.Style()
                    rbStyle.configure(
                        '{0}.TRadiobutton'.format(companyName),
                        foreground = company.colors['fg'],
                        background = company.colors['bg']
                    )
                    thisRB = ttk.Radiobutton(
                        self.r3f,
                        value = companyKey,
                        text = '{0}'.format(companyName),
                        variable=self.relevantCompany,
                        ## Once a radioButton is clicked, set up the sources.
                        style = '{0}.TRadiobutton'.format(companyName),
                        command = self.onRB2
                    )
                    thisRB.grid(column=i,row=0)
        else:
            boolbl = tki.Label(self.r3f,text = 'No salable items.')
            boolbl.grid(column=0,row=0)
            self.relevantCompany.set(-1)

    def confirm(self,event=None):
        if int(self.relevantCompany.get()) == -1:
            return None
        try:
            pricePerShare = int(self.priceEntry.get())
        except ValueError:
            showinfo(message = 'Must enter an integer for price.\n(You turdjuggler.)')
            return None
        try:
            sharenum = int(self.sharenum.get())
        except ValueError:
            showinfo(message = 'Must enter an integer for stock amount.\n(You freckled poop.)')
            return None
        ## Now, actually execute the trade.
        price = pricePerShare * sharenum
        activeEntity,activeEntityType = self.actor
        [passiveEntityType,passiveEntity] = self.fromVar.get().split('.')
        passiveEntity = int(passiveEntity)
        shareCompanyKey = int(self.relevantCompany.get())
#        shareCompanyKey = self.accounter.cKey(shareCompany)
        oldPricePerShare = self.accounter.getStockValue(shareCompanyKey)
        funcList = [
            self.accounter.changeStockHoldings,
            self.accounter.changeStockHoldings,
            self.accounter.changeMoney,
            self.accounter.changeMoney,
            self.accounter.setStockValue,
            self.root.updateLabels
        ]
        if self.buysell == 'buy':
            ## Purchase
#            self.accounter.changeStockHoldings(passiveEntity,passiveEntityType,shareCompany,-sharenum)
#            self.accounter.changeStockHoldings(activeEntity,activeEntityType,shareCompany,sharenum)
#            self.accounter.changeMoney(passiveEntity,passiveEntityType,price)
#            self.accounter.changeMoney(activeEntity,activeEntityType,-price)
            doVarList = [
                    [passiveEntity,shareCompanyKey,-sharenum],
                    [activeEntity,shareCompanyKey,sharenum],
                    [passiveEntity,price],
                    [activeEntity,-price],
                    [shareCompanyKey,pricePerShare],
                    []
            ]
            undoVarList = [
                    [passiveEntity,shareCompanyKey,sharenum],
                    [activeEntity,shareCompanyKey,-sharenum],
                    [passiveEntity,-price],
                    [activeEntity,price],
                    [shareCompanyKey,oldPricePerShare],
                    []
            ]
            ## check validity: does the source have enough shares?
            ## Does the seller have enough money?
            if not self.accounter.isTradeValid(shareCompanyKey,passiveEntity,
                 activeEntity,sharenum,
                 price):
                showinfo(message = 'Invalid trade.\nCheck whether seller has enough stock and whether the buyer has enough cash.')
            else:
                Memento(
                    "{0} bought {1} shares of {2} from {3} for {5}{4} per share.".format(
                            self.accounter.eName(activeEntity),sharenum,
                            self.accounter.eName(shareCompanyKey),
                            self.accounter.eName(passiveEntity),pricePerShare,
                            self.accounter['currencySign']
                    ),
                    funcList,
                    doVarList,
                    funcList,
                    undoVarList
                )
                self.top.destroy()
        else:
            ## Sale
            doVarList = [
                    [passiveEntity,shareCompanyKey,sharenum],
                    [activeEntity,shareCompanyKey,-sharenum],
                    [passiveEntity,-price],
                    [activeEntity,price],
                    [shareCompanyKey,pricePerShare],
                    [],
            ]
            undoVarList = [
                    [passiveEntity,shareCompanyKey,-sharenum],
                    [activeEntity,shareCompanyKey,sharenum],
                    [passiveEntity,price],
                    [activeEntity,-price],
                    [shareCompanyKey,oldPricePerShare],
                    [],
            ]
            if not self.accounter.isTradeValid(shareCompanyKey,activeEntity,
                 passiveEntity,sharenum,
                 price):
                showinfo(message = 'Invalid trade.\nCheck whether seller has enough stock and whether the buyer has enough cash.')
            else:
#            self.accounter.changeStockHoldings(passiveEntity,passiveEntityType,shareCompany,sharenum)
#            self.accounter.changeStockHoldings(activeEntity,activeEntityType,shareCompany,-sharenum)
#            self.accounter.changeMoney(passiveEntity,passiveEntityType,-price)
#            self.accounter.changeMoney(activeEntity,activeEntityType,price)
                Memento(
                    "{0} sold {1} shares of {2} to {3} for {5}{4} per share.".format(
                            self.accounter.eName(activeEntity),sharenum,
                            self.accounter.eName(shareCompanyKey),
                            self.accounter.eName(passiveEntity),pricePerShare,
                            self.accounter['currencySign']),
                    funcList,
                    doVarList,
                    funcList,
                    undoVarList
                )
                self.top.destroy()

        ## Record the stock price.
#        self.accounter.setStockValue(self.relevantCompany.get(),pricePerShare)
        ## Then get rid of the dialog.


#    def onRB1(self,event=None):
#        self.tofrom.set(['from','to'][self.buysell.get()])
#        if self.relevantCompany.get() != '':
#            self.onRB2()
#        self.r3f.destroy()
#        self.setupRelevantCompanyRBs()

    def onRB2(self,event=None):
        cKey = int(self.relevantCompany.get())
#        print [(a.key, a.title) for a in self.accounter.allEntities.values()]
#        print self.accounter.allEntities[0].possessions.items()
        inIPO,inPool,playerOwned,companyOwned = self.accounter.findStocks(cKey)
        ## Set the value of the stock to the last recorded value of relevantCo
        stockValue = self.accounter.companies[cKey].shareValue
        self.stockVar.set(str(stockValue))
        self.sourceFrame.destroy()
        self.sourceFrame = tki.Frame(self.frm)
        self.sourceFrame.grid(column=0,row=4)
        bankFrame = tki.LabelFrame(self.sourceFrame,text='Bank')
        bankFrame.grid(row=0,column=0)
        playerFrame = tki.LabelFrame(self.sourceFrame,text='Players')
        playerFrame.grid(row=0,column=1)
        companyFrame = tki.LabelFrame(self.sourceFrame,text='Companies')
        companyFrame.grid(row=0,column=2)
        radioButtonRecipes = []
        if inPool is not None:
            if inPool > 0 or self.buysell == 'sell':
                pool = 'bank.{0}'.format(self.accounter.pool.key)
                radioButtonRecipes.append(
                        (None,bankFrame,u'Pool ({0})'.format(inPool),pool)
                )
        if inIPO is not None:
            if inIPO > 0 or (self.buysell == 'sell' and self.accounter.ipo != None):
                ipo = 'bank.{0}'.format(self.accounter.ipo.key)
                radioButtonRecipes.append(
                        (None,bankFrame,u'IPO ({0})'.format(inIPO),ipo)
                )
        for playerKey,amount in playerOwned:
            playerName = self.accounter.players[playerKey].title
            if amount > 0 or self.buysell == 'sell':
                radioButtonRecipes.append(
                    (
                        None,
                        playerFrame,
                        u'{0} ({1})'.format(playerName,amount),
                        u'player.{0}'.format(playerKey),
                    )
                )
        for companyKey,amount in companyOwned:
            companyName = self.accounter.cName(companyKey)
            if (    (amount > 0 or self.buysell == 'sell') and 
                    self.accounter.companies[companyKey].active
                ):
                radioButtonRecipes.append(
                    (
                        companyKey,
                        companyFrame,
                        u'{0} ({1})'.format(companyName,amount),
                        u'company.{0}'.format(companyKey)
                    )
                )
        curIdx = 0
        if self.buysell == 'sell':
            self.fromVar.set(radioButtonRecipes[1][2])
        else:
            self.fromVar.set(radioButtonRecipes[0][2])
#        rbList = []
        for (companyKey,frame,text,value) in radioButtonRecipes:
            if companyKey != None:
                company = self.accounter.companies[companyKey]
                rbStyle = ttk.Style()
                rbStyle.configure(
                    '{0}.TRadiobutton'.format(companyKey),
                    foreground = company.colors['fg'],
                    background = company.colors['bg']
                )
                thisRB = ttk.Radiobutton(
                        frame,
                        variable=self.fromVar,
                        text = text,
                        value = value,
                        style = '{0}.TRadiobutton'.format(companyKey),

                        command=self.priceFocus
                )
            else:
                thisRB = ttk.Radiobutton(
                        frame,
                        variable=self.fromVar,
                        text=text,
                        value=value,
                        command=self.priceFocus
                )
            thisRB.grid(column=curIdx,row=0)
#            rbList.append(thisRB)
            curIdx += 1
#        print '***************************'
        self.fromVar.set(radioButtonRecipes[0][3])
#        print value
        self.priceFocus()

    def priceFocus(self,event=None):
        self.priceEntry.focus_set()
#        klbl = tki.Label(bankFrame,text='!!')
#        klbl.pack()
#
#class MainCompanyFrame(tki.Frame):
#    pass

class CompanyContainer:
    def __init__(self,parent,activeFrame,inactiveFrame,colNum,companyReference,accounter):
        ## Get a reference to a company object from the accounter.
        ## Also get pool reference and if relevant, ipo.
        self.company = companyReference
        self.accounter = accounter
        self.parent = parent
        self.activeFrame = activeFrame
        self.inactiveFrame = inactiveFrame
        self.colNum = colNum
        ## setup string vars.
        self.revenueVar = tki.StringVar(value='0')
        self.revenueVar.set(self.company.lastRecordedRevenue)
        self.shareNumVar = tki.StringVar(value='')
        self.holdingLabelDict = {}
        self.companyHeldStockVarDict = {}
        for subCKey in self.accounter.getCompanyKeys():
            if subCKey != self.company.key:
                cName = self.accounter.cName(subCKey)
                num = self.company.possessions[subCKey]
                self.companyHeldStockVarDict[subCKey] = tki.StringVar(value = cName + ':' + str(num))
        self.cashVar = tki.StringVar(value='0'.format(self.accounter['currencySign']))
        self.ownStockVar = tki.StringVar(value='0')
    
    def update(self):
        ## update the string variables from company object / accounter
#        self.revenueVar = tki.StringVar(value='0')
#        self.revenueVar.set(self.company.lastRecordedRevenue)
        self.shareNumVar.set(str(self.company.shareNum))
        self.ownStockVar.set(self.company.possessions[self.company.key])
        self.companyHeldStockVarDict = dict(
            (c,tki.StringVar(value='')) 
            for c in self.accounter.getCompanyKeys()
        )
        del(self.companyHeldStockVarDict[self.company.key])
        if self.company.active:
            i = 0
            for subCKey in self.companyHeldStockVarDict.keys():
                try:
                    self.holdingLabelDict[subCKey].destroy()
                except KeyError:
                    pass
                if self.accounter.companies[subCKey].active:
                    cName = self.accounter.cName(subCKey)
                    before = self.companyHeldStockVarDict[subCKey].get()
                    num = self.company.possessions[subCKey]
                    if num != 0 and before == '':
                        self.companyHeldStockVarDict[subCKey].set(cName + ':' + str(num))
                        subfgcolor = self.accounter.getCompanyColor(subCKey,'fg')
                        subbgcolor = self.accounter.getCompanyColor(subCKey,'bg')
                        text = self.companyHeldStockVarDict[subCKey]
                        self.holdingLabelDict[subCKey] =  tki.Label(
                            self.activeFrame,
                            textvariable = text,
                            font=self.parent.smallFont,
                            fg=subfgcolor,bg=subbgcolor
                        )
                        self.holdingLabelDict[subCKey].grid(column=self.colNum+2,row=self.rowFrom+13+i,padx=PADVAL,pady=PADVAL)
                        i += 1
                    elif num != 0 and before != '':
                        self.companyHeldStockVarDict[subCKey].set(cName + ':' + str(num))
                        subfgcolor = self.accounter.getCompanyColor(subCKey,'fg')
                        subbgcolor = self.accounter.getCompanyColor(subCKey,'bg')
                        text = self.companyHeldStockVarDict[subCKey]
                        self.holdingLabelDict[subCKey] =  tki.Label(
                            self.activeFrame,
                            textvariable = text,
                            font=self.parent.smallFont,
                            fg=subfgcolor,bg=subbgcolor
                        )
                        self.holdingLabelDict[subCKey].grid(column=self.colNum+2,row=self.rowFrom+13+i,padx=PADVAL,pady=PADVAL)
    
                        self.holdingLabelDict[subCKey].grid_remove()
                        self.holdingLabelDict[subCKey].grid(column=self.colNum+2,row=self.rowFrom+13+i,padx=PADVAL,pady=PADVAL)
                        i += 1
                    elif num == 0 and before != '':
                        self.companyHeldStockVarDict.set('')
                        self.holdingLabelDict[subCKey].grid_remove()
                    else:
                        ## maintain status quo.
                        pass
#                    print (cName, num)
#                    print self.companyHeldStockVarDict[subCKey].get()        
        self.cashVar.set('{0}'.format(self.company.cash))
        
    
#    
#    def editDialog(self):
#        ecd = EditCompanyDialog(self.accounter,self.edit,cKey)
        
#    def edit(self,cKey,newVarDict):
#        ## NEEDS WORK WHEN IMPLEMENTING COMPANY PROFILES
#        ## NYI & SHIT
#        previousVarDict = self.company.__dict__.copy()
#        doVarList = [[cKey,newVarDict],[]]
#        undoVarList = [[cKey,previousVarDict],[]]
##        self.accounter.addPlayer(pName,pCash)
#        Memento(
#                "Edited company '{0}' to {1} shares.".format(cName,stockNum),
#                [self.accounter.editCompany,self.updateGUI],
#                doVarList,
#                [self.accounter.editCompany,self.updateGUI],
#                undoVarList
#        )
        
    def activate(self,activeCompaniesFrame):
        self.activeFrame = activeCompaniesFrame
        self.company.active = True
        
    def draw(self,rowFrom):
        self.rowFrom = rowFrom
        cKey = self.company.key
        fgcolor = self.company.colors['fg']
        bgcolor = self.company.colors['bg']
        col = self.colNum
        if self.company.active:
#==============================================================================
#      Draw active pill.
#==============================================================================
#            ## create active state widgets
            activeCompaniesFrame = self.activeFrame
#            ownFrame = tki.Frame(self.activeFrame,borderwidth=3,relief='ridge')
#            ownFrame.grid(column=col+2,row=rowFrom+1,padx=PADVAL,pady=PADVAL)
            ownStockLabel =  tki.Label(
                    activeCompaniesFrame,
                    textvariable = self.ownStockVar,
                    font=self.parent.bigFont,
                    fg=fgcolor,bg=bgcolor
            )
            ownStockLabel.grid(column=col+2,row=rowFrom-1,padx=PADVAL,pady=PADVAL)
            nameLabel =  tki.Label(
                    activeCompaniesFrame,
                    text = self.company.title,
                    font=self.parent.bigFont,
                    fg=fgcolor,bg=bgcolor
            )
            nameLabel.grid(column=col+2,row=rowFrom+1,padx=PADVAL,pady=PADVAL)
#            shareNumLabel = tki.Label(activeCompaniesFrame,textvariable = self.shareNumVar,
#                        font=self.parent.smallFont)
#            shareNumLabel.grid(column=col+2,row=rowFrom+2,padx=PADVAL,pady=PADVAL)
            def editCompanyButtonHandler(entityKey=cKey):
                return self.parent.editCompanyDialog(entityKey)
            editButton = tki.Button(
                    activeCompaniesFrame,
                    textvariable=self.shareNumVar,
                    command = editCompanyButtonHandler, 
                    width = 5,fg=fgcolor, bg=bgcolor
                    )
            editButton.grid(column=col+2,row=rowFrom+2,padx=PADVAL,pady=PADVAL)
            def moneyButtonHandler(entityKey=cKey,entityType='company'):
                return self.parent.transferMoneyDialog(entityType,entityKey)
            moneyButton = tki.Button(
                    activeCompaniesFrame,text='>{0}>'.format(self.accounter.currencySign),
                    command=moneyButtonHandler, 
                    width = 5,fg=fgcolor, bg=bgcolor
                    )
            moneyButton.grid(column=col+2,row=rowFrom+3,padx=PADVAL,pady=PADVAL)

            cashLabel = tki.Label(
                activeCompaniesFrame,
                textvariable=self.cashVar,font=self.parent.bigFont
                )
            cashLabel.grid(column=col+2,row=rowFrom+4,padx=PADVAL,pady=PADVAL,sticky= 'E')
            operateEntry = IntegerEntry(
                    activeCompaniesFrame,
                    textvariable=self.revenueVar,
                    justify='right',
                    font=self.parent.bigFont,width=4
                    )
            operateEntry.bind('<FocusOut>', self.updateEntry)
            operateEntry.grid(column=col+2,row=rowFrom+5,padx=PADVAL,pady=PADVAL)
            def dividendButtonHandler(companyKey=cKey):
                return self.payDividend(companyKey)
            def withholdButtonHandler(companyKey=cKey):
                return self.payDividend(companyKey,'withhold')
            def fiftyfiftyButtonHandler(companyKey=cKey):
                return self.payDividend(companyKey,'50/50')
            operateButton = tki.Button(
                    activeCompaniesFrame,text='Pay',
                    command=dividendButtonHandler, 
                    width = 5,
                    fg=fgcolor, bg=bgcolor
                    )
            if self.company.canOperate:
                operateButton.grid(column=col+2,row=rowFrom+6,padx=PADVAL,pady=PADVAL)
            fiftyfiftyButton = tki.Button(
                    activeCompaniesFrame,text='Split',command=fiftyfiftyButtonHandler, 
                    width = 5,fg=fgcolor, bg=bgcolor
                    )
            if self.company.canFiftyFifty:
                fiftyfiftyButton.grid(column=col+2,row=rowFrom+7,padx=PADVAL,pady=PADVAL)  
            withholdButton = tki.Button(
                    activeCompaniesFrame,text='Keep',command=withholdButtonHandler, 
                    width = 5,fg=fgcolor, bg=bgcolor
                    )
            if self.company.canWithhold:
                withholdButton.grid(column=col+2,row=rowFrom+8,padx=PADVAL,pady=PADVAL)            
            pws = ttk.Frame(activeCompaniesFrame)
            pws.grid(column=col+2,row=rowFrom+9, padx=2*PADVAL)
            def stockBuyButtonHandler(entityKey=cKey,entityType='company',buysell='buy'):
                return self.parent.changeStockDialog(entityKey,entityType,buysell)
            def stockSellButtonHandler(entityKey=cKey,entityType='company',buysell='sell' ):
                return self.parent.changeStockDialog(entityKey,entityType,buysell)
            stockBuyButton = tki.Button(
                    activeCompaniesFrame,text='Buy',command = stockBuyButtonHandler, 
                    width = 5,fg=fgcolor, bg=bgcolor
                    )
    
            stockSellButton = tki.Button(
                    activeCompaniesFrame,text='Sell',command = stockSellButtonHandler, 
                    width = 5,fg=fgcolor, bg=bgcolor
                    )
            pss = ttk.Frame(activeCompaniesFrame)
            pss.grid(column=col+2,row=rowFrom+12,padx=2*PADVAL)
#    #        print self.company.__dict__.items()
            if len(self.company.canOwnTypes) > 0:
                stockBuyButton.grid(column=col+2,row=rowFrom+9,padx=PADVAL,pady=PADVAL)
                stockSellButton.grid(column=col+2,row=rowFrom+10,padx=PADVAL,pady=PADVAL)
        else:
            divisor = 16
            baseColumn = int(self.colNum / divisor) * 2
            row = self.colNum % divisor
            inactiveCompanyFrame = self.inactiveFrame
            nameLabel =  tki.Label(
                    inactiveCompanyFrame,
                    text = self.company.title,
                    font=self.parent.bigFont,
                    fg=fgcolor,bg=bgcolor
            )
            nameLabel.grid(column=baseColumn,row=row,padx=PADVAL,pady=PADVAL)
            def companyActivationHandler(entityKey=cKey):
                return self.parent.activateCompany(entityKey)
            activateButton = tki.Button(
                    inactiveCompanyFrame,text='Activate',
                    command=companyActivationHandler, 
                    fg=fgcolor, bg=bgcolor
            )
            activateButton.grid(column=baseColumn+1,row=row,padx=PADVAL,pady=PADVAL)
       
    def deactivate(self,inactiveCompanyFrame=None):
        if inactiveCompanyFrame == None:
            inactiveCompanyFrame = self.inactiveFrame
        else:
            self.inactiveFrame = inactiveCompanyFrame        
        self.company.active = False        
    
    def updateEntry(self,event=None):
        ## Put whatever is in enterable widgets (i.e. revenues) into the
        ## accounter.
        try:
            thisVar = int(self.revenueVar.get())
            self.company.lastRecordedRevenues = thisVar
        except ValueError:
            thisVar.set(str(self.company.lastRecordedRevenues))

    def payDividend(self,cKey,kind='100%'):
        text = {'100%':'Paying full.','50/50':'Paying 50/50.','withhold':'Withholding.'}
        try:
            revenue = int(eval(self.revenueVar.get()))
#            withhold = bool(self.withholdVars[companyName].get())
            doVarList = [[cKey,revenue,kind]]
            undoVarList = [[cKey,-revenue,kind]]
#            self.accounter.companyDividend(*doVars)
            Memento(
                    '{0} operated for {3}{1}. {2}'.format(
                            self.accounter.eName(cKey),
                            revenue,text[kind],
                            self.accounter['currencySign']),
                    [self.accounter.companyDividend],
                    doVarList,
                    [self.accounter.companyDividend],
                    undoVarList
            )
        except ValueError:
            showinfo(message = 'Must enter an integer in revenue box.')
        self.parent.updateLabels()
                    
class Tk18xx:
    def __init__(self,accounter=None):
        if accounter == None:
            a = Accounter()  
            a.setup('',[],
                    separateIPO=False,currencySign = '$')
            a.bankSize = 1
            self.setup(a)
            self.openScenario()
        else:
            self.setup(accounter)
    def setup(self,accounter):
        ## Do not autosave while building up.
        self.autosave = False
        ## First things first: call the Tkinter constructor.
        self.gui = tki.Tk()
        self.saveName = '18xx.sav'
        self.gui.wm_title('18xx Administrator')
        ## Then, set up the Accounter.
        self.accounter = accounter
        ## Set up the menu.
        self.menu = tki.Menu(self.gui)
        self.gui.config(menu=self.menu)
        filemenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="New", command=self.new)
        filemenu.add_command(label="Load", command=self.openFile)
        filemenu.add_command(label="Load Scenario",command=self.openScenario)
        filemenu.add_command(label="Save As...", command=self.saveFile)
        filemenu.add_separator()
        filemenu.add_command(label="Show Log", command=self.showLog)
        filemenu.add_command(label="Set Bank", command=self.setBank)

        editmenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Edit", menu=editmenu)
        editmenu.add_command(label="Undo", command=self.undo)
        self.gui.bind('<Control-Key-z>',self.undo)
        editmenu.add_command(label="Redo", command=self.redo)
        self.gui.bind('<Control-Shift-Key-z>',self.redo)

        playermenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Players", menu=playermenu)
        playermenu.add_command(label='Add player',command=self.addPlayerDialog)
        playermenu.add_command(label='Remove player',command=self.removePlayerDialog)
        playermenu.add_command(
            label='Set Stock Value & Display Net Worth',
            command=self.setStockValueDialog
        )
        companymenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Companies", menu=companymenu)
        companymenu.add_command(label='Add company',command=self.addCompanyDialog)
        companymenu.add_command(label='Deactivate company',command=self.deactivateCompanyDialog)
        companymenu.add_command(label='Delete company',command=self.removeCompanyDialog)
        optionmenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Options", menu=optionmenu)
        optionmenu.add_command(label='Change Font',command=self.changeFontDialog)
        ## OptionVars + menu items

        ## Set up some GUI globals.
        self.headerFont = tkFont.Font(size=HEADERFONT)
        self.bigFont = tkFont.Font(size=BIGFONT)
        self.smallFont = tkFont.Font(size=SMALLFONT)
        self.companyNotebook = ttk.Notebook(self.gui)
        self.companyNotebook.grid(
            row=3,column=0,columnspan=3,padx=PADVAL,pady=PADVAL,sticky='W')
        self.mainFrame = ttk.Frame(self.companyNotebook,padding=3)
        self.companyNotebook.add(self.mainFrame,text='Active Sheet')

        self.playerFrame = tki.Frame(self.mainFrame,
            borderwidth=3)
        self.playerFrame.grid(
            row=1,column=0,columnspan=2,sticky='W',padx=PADVAL,pady=PADVAL)


        self.companyFrame0 = ttk.Frame(
            self.mainFrame,padding=3,relief='ridge')
        self.companyFrame0.grid(row=2,column=0)
        
#        self.companyNotebook.enable_traversal()
        self.companyFrame1 = ttk.Frame(
            self.mainFrame,padding=3,relief='ridge')
        self.companyFrame1.grid(row=2,column=1)
#        self.companyNotebook.add(self.companyFrame1,text='Active Companies (2)')
        self.inactiveFrame = ttk.Frame(self.companyNotebook,padding=3)
        self.companyNotebook.add(self.inactiveFrame,text='Reserve Companies')

#==============================================================================
#       Company container objects
#==============================================================================
        self.companyContainers = dict(
                (company.key,CompanyContainer(
                        self,self.playerFrame,self.inactiveFrame,
                        i,company,self.accounter
                        )
                ) 
                for i,company in enumerate(self.accounter.companies.values())
        )

#==============================================================================
#         Containers filled with variables for use in GUI
#==============================================================================
        ## Players
        self.playerCashVars = dict(
            (pKey,tki.StringVar(value='0'))
            for pKey
            in self.accounter.getPlayerKeys()
        )
        self.playerHeldStockVarDicts = dict(
            (pKey,dict(
                (c,tki.StringVar(value=''))
                for c
                in self.accounter.getCompanyKeys()
            ))
            for pKey
            in self.accounter.getPlayerKeys()
        )
        ## IPO & pool
        if self.accounter.ipo != None:
            self.ipoHeldStockVarDict = dict(
                (cKey,tki.StringVar(value='$$'))
                for cKey
                in self.accounter.getCompanyKeys()
            )
        self.poolHeldStockVarDict = dict(
                (cKey,tki.StringVar(value='$$'))
                for cKey
                in self.accounter.getCompanyKeys()
            )
        ## Overall
        self.moneyLeftVar = tki.StringVar(
            value='Left in bank:\n{1}{0}'.format(self.accounter.doAccounts(),accounter['currencySign'])
        )
        self.updateGUI()

    def undo(self,event=None):
        try:
            UNDOSTACK[-1].undo()
            self.updateLabels()
        except IndexError:
            pass

    def redo(self,event=None):
        try:
            REDOSTACK[-1].do()
            REDOSTACK.pop()
            self.updateLabels()
        except IndexError:
            pass

    def showLog(self,event=None):
        if self.autosave:
            self.saveFile(autosave=True)
        print('####################################################')
        for logMemento in UNDOSTACK:
            print(logMemento.logMessage)

    def updateLabels(self,event=None):
        for container in self.companyContainers.values():
            container.update()
        ## Players
        for pKey,player in sorted(self.accounter.players.items()):
            self.playerCashVars[pKey].set('{1} {0}'.format(player.cash,self.accounter['currencySign']))
#            self.playerHeldStockVarDicts[pName] = dict((pName,[]) for pName in self.accounter.players)
            for subCKey,var in sorted(self.playerHeldStockVarDicts[pKey].items()):
                shareCount = player.possessions[subCKey]
                stockText = ['',str(shareCount)][shareCount!=0]
                var.set(stockText)
        ## IPO
        if self.accounter.ipo != None:
            for i,(cKey,company) in enumerate(self.accounter.companies.items()):
                if company.active == True:
                    var = str(self.accounter.ipo.possessions[cKey])
                else:
                    var = ''
                self.ipoHeldStockVarDict[cKey].set(var)
        ## Pool
        for i,(cKey,company) in enumerate(self.accounter.companies.items()):
            if company.active == True:
                var = str(self.accounter.pool.possessions[cKey])
            else:
                var = ''
            self.poolHeldStockVarDict[cKey].set(var)
        
        ## Overall
        moneyLeft = self.accounter.doAccounts()
        if moneyLeft <= 0:
            showinfo(message='The bank has broken.')
        self.moneyLeftVar.set('Left in bank: {1}{0}'.format(moneyLeft,self.accounter['currencySign']))
        self.showLog()
        
    def updateGUI(self,event=None):
        ## PLAYERS & GENERAL
        rowIndex = 1
        newPlayers = self.accounter.getPlayerKeys()[len(self.playerCashVars):]
        for newPlayerKey in newPlayers:
            self.playerCashVars[newPlayerKey] = tki.StringVar(value='0')
            self.playerHeldStockVarDicts[newPlayerKey] = dict(
                (c,tki.StringVar(value=''))
                for c in self.accounter.getCompanyKeys()
            )
        ## check whether every company has the requisite vars
        companyCount = len(self.accounter.getCompanyKeys())
        self.moneyLeftVar.set(value='Left in bank:\n{0}{1}'.format(self.accounter['currencySign'],self.accounter.doAccounts()))
        ## now, the players part of the GUI
        ## teardown
        for kid in list(self.playerFrame.children.values()):
            kid.destroy()
#        nameColHeader = tki.Label(self.playerFrame,font=self.headerFont,text='Name')
#        nameColHeader.grid(column=0,row=0,padx=PADVAL,pady=PADVAL)
#        cashColHeader = tki.Label(self.playerFrame,font=self.headerFont,text='Cash')
#        cashColHeader.grid(column=1,row=0,padx=PADVAL,pady=PADVAL)
        moneyLeftLabel = tki.Label(self.playerFrame,textvariable = self.moneyLeftVar,font=self.bigFont)
        moneyLeftLabel.grid(column=0,row=0,columnspan=2,padx=PADVAL,pady=PADVAL)

        for i,(companyKey,company) in enumerate(self.accounter.companies.items()):
            if company.active == True:
                companyHeader = tki.Label(self.playerFrame,text = company.title,
                        font=self.bigFont,
                        fg=company.colors['fg'],
                        bg=company.colors['bg'])
                companyHeader.grid(column=2+i,row=0,padx=PADVAL,pady=PADVAL)
        for i,(playerKey,player) in enumerate(sorted(self.accounter.players.items())):
            pNameLabel = tki.Label(self.playerFrame,text = player.title,font=self.bigFont)
            pNameLabel.grid(column=0,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='W')
            pCashLabel = tki.Label(self.playerFrame,textvariable = self.playerCashVars[playerKey],font=self.bigFont)
            pCashLabel.grid(column=1,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            for j,companyKey in enumerate(self.accounter.getCompanyKeys()):
                company = self.accounter.companies[companyKey]
                if company.active == True:
                    companyLabel = tki.Label(
                        self.playerFrame,
                        textvariable = self.playerHeldStockVarDicts[playerKey][companyKey],
                        font=self.bigFont
                    )
                    companyLabel.grid(column=2+j,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            def stockBuyButtonHandler(entityKey=playerKey,entityType='player',buysell='buy'):
                return self.changeStockDialog(entityKey,entityType,buysell)
            def stockSellButtonHandler(entityKey=playerKey,entityType='player',buysell='sell'):
                return self.changeStockDialog(entityKey,entityType,buysell)
            stockBuyButton = tki.Button(self.playerFrame,text='Buy Stock',command = stockBuyButtonHandler,height=1)
            stockBuyButton.grid(column=companyCount+2,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            stockSellButton = tki.Button(self.playerFrame,text='Sell Stock',command = stockSellButtonHandler)
            stockSellButton.grid(column=companyCount+3,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            def moneyButtonHandler(entityKey=playerKey,entityType='player'):
                return self.transferMoneyDialog(entityType,entityKey)
            moneyButton = tki.Button(self.playerFrame,text='Transfer Money',command=moneyButtonHandler)
            moneyButton.grid(column=companyCount+4,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            if i < len(self.accounter.players):
                sep = ttk.Separator(self.playerFrame)
                sep.grid(column=0,columnspan=5+companyCount,row=i*2+1,padx=PADVAL,pady=PADVAL,sticky='EW')
            rowIndex = i * 2 + 3
#        rowIndex+=1
        ## make IPO row, if applicable
        if self.accounter.ipo != None:
            ipoLabel = tki.Label(self.playerFrame,text = 'Shares in IPO',
                        font=self.bigFont,
                        fg='black',
                        bg='white')
            ipoLabel.grid(column = 0, row = rowIndex,columnspan = 2,padx=PADVAL,pady=PADVAL,sticky='E')
            for i,cKey in enumerate(self.accounter.getCompanyKeys()):
                company = self.accounter.companies[cKey]
                if company.active == True:
                    ipoLabel = tki.Label(self.playerFrame,textvariable = self.ipoHeldStockVarDict[cKey],
                            font=self.bigFont)
                    ipoLabel.grid(column = i +2, row = rowIndex,padx=PADVAL,pady=PADVAL)
            rowIndex+=1
        ## make pool row
        poolLabel = tki.Label(self.playerFrame,text = 'Shares in pool',
                    font=self.bigFont,
                    fg='black',
                    bg='white')
        poolLabel.grid(column = 0, row = rowIndex,columnspan = 2,padx=PADVAL,pady=PADVAL,sticky='E')
        for i,cKey in enumerate(self.accounter.getCompanyKeys()):
            company = self.accounter.companies[cKey]
            if company.active == True:
                poolCLabel = tki.Label(self.playerFrame,textvariable = self.poolHeldStockVarDict[cKey],
                        font=self.bigFont)
                poolCLabel.grid(column = i +2, row = rowIndex,padx=PADVAL,pady=PADVAL)
        rowIndex+=1
        if any([c.canOwnOwnStock for c in self.accounter.companies.values()]):
            poolLabel = tki.Label(self.playerFrame,text = 'Shares in company',
                        font=self.bigFont,
                        fg='black',
                        bg='white')
            poolLabel.grid(column = 0, row = rowIndex,columnspan = 2,padx=PADVAL,pady=PADVAL,sticky='E')
        rowIndex+=1
        numSharesLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Total shares (edit)')
        numSharesLabel.grid(column = 0, row = rowIndex+2,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        treasuryLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Treasury {0}'.format(self.accounter.currencySign))
        treasuryLabel.grid(column = 0, row = rowIndex+4,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        transferLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Transfer Money')
        transferLabel.grid(column = 0, row = rowIndex+3,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        revLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Revenue {0}'.format(self.accounter.currencySign))
        revLabel.grid(column = 0, row = rowIndex+5,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        divLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Pay Dividends 100%')
        divLabel.grid(column = 0, row = rowIndex+6,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        fittyfittyLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Keep 50%, Pay 50%')
        fittyfittyLabel.grid(column = 0, row = rowIndex+7,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        keepLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Keep 100%')
        keepLabel.grid(column = 0, row = rowIndex+8,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E')
        buyStockLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Buy Shares')
        buyStockLabel.grid(column = 0, row = rowIndex+9,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E') 
        sellStockLabel =tki.Label(self.playerFrame,font = self.bigFont,
                    fg='black',bg='white',text = 'Sell Shares')
        sellStockLabel.grid(column = 0, row = rowIndex+10,columnspan=2,
                            padx=PADVAL,pady=PADVAL,sticky='E') 
#        for kidid,kid in self.companyFrame0.children.items():
#            kid.destroy()   
#        for kidid,kid in self.companyFrame1.children.items():
#            kid.destroy()   
        for kidid,kid in list(self.inactiveFrame.children.items()):
            kid.destroy()
        for cContainer in self.companyContainers.values():
            cContainer.draw(rowIndex)
       
        self.updateLabels()
        self.showLog()

    def new(self):
        self.accounter = Accounter()
        self.accounter.setup()
        self.updateGUI()

    def setBank(self):
        sbdb = Mbox(['Set the amount of money in the bank:'],self.accounter.__dict__,['bankSize'], typeCast = [int])
        sbdb.b_submit.bind('<Destroy>',self.updateGUI)

    def openFile(self):
        if not os.path.exists('./saves/'):
            os.mkdir('./saves/')
        name = tkFileDialog.askopenfilename(initialdir='./saves',filetypes=[('sav','*.sav')])
        if name != '':
            nameParts = name.split('.')
            name = nameParts[0]
            fileType = nameParts[-1]
            if fileType == 'sav':
                print(name)
#            self.accounter.load(name+'.sav')
            newAccounter = Accounter()
            newAccounter.setup('',[])
            newAccounter.load(name+'.sav')
            self.gui.destroy()
            self.__init__(newAccounter)
#            self.updateGUI()

    def openScenario(self):
        self.openFile()
#        print 'call up a player dialog'
        lsd = LoadScenarioDialog(self.accounter)
        lsd.root = self

    def saveFile(self,autosave=False):
        if not os.path.exists('./saves/'):
            os.mkdir('./saves/')
        if not autosave:
            self.saveName = tkFileDialog.asksaveasfilename(initialdir='./saves',filetypes=[('sav','*.sav')])
        else:
            self.accounter.serialize('auto.sav')
#        print name
        if self.saveName.split('.')[-1] != 'sav':
            self.accounter.serialize(self.saveName+'.sav')
            print('add')

        elif self.saveName != '':
            self.accounter.serialize(self.saveName)
        else:
            pass

    def addPlayerDialog(self):
        self.tempDict = {'pname':None,'pcash':0}
        apdb = Mbox(['Player name:','Player cash:'],self.tempDict,['pname','pcash'],self.addPlayer,[None,int])

    def changeFontDialog(self):
        self.tempDict = {'bigFont':self.bigFont,'smallFont':self.smallFont}
        cfdb = Mbox(['Big Font:','Small Font:'],self.tempDict,['bigFont','smallFont'],self.changeFont,[int,int])

    def changeFont(self):
#        print self.tempDict
        self.smallFont = tkFont.Font(size=self.tempDict['smallFont'])
        self.bigFont = tkFont.Font(size=self.tempDict['bigFont'])
        self.updateGUI()

    def addPlayer(self):
        pName = self.tempDict['pname']
        pCash = self.tempDict['pcash']
        doVarList = [[pName,pCash],[]]
        undoVarList = [[pName],[]]
#        self.accounter.addPlayer(pName,pCash)
        Memento(
                "Added player '{0}' with {2}{1}.".format(
                        pName,pCash,self.accounter['currencySign']),
                [self.accounter.addPlayer,self.updateGUI],
                doVarList,
                [self.accounter.removePlayer,self.updateGUI],
                undoVarList
        )

    def addCompany(self,cName,stockNum,ownIPO,mainColor,bgColor):
#        self.accounter.addCompany(cName,stockNum)
#        self.accounter.setCompanyColors(cName,mainColor,bgColor)
## TODO: ADJUST FOR COMPANYPROFILES
        try:
            doVarList = [[cName,stockNum,ownIPO],[cName,mainColor,bgColor],[]]
            undoVarList = [[cName],[]]
    #        self.accounter.addPlayer(pName,pCash)
            Memento(
                    "Added company '{0}' with {1} shares.".format(cName,stockNum),
                    [self.accounter.addCompany,self.accounter.setCompanyColors,self.updateGUI],
                    doVarList,
                    [self.accounter.removeCompany,self.updateGUI],
                    undoVarList
            )
        except ValueError:
            showinfo(message = 'That company name is already taken. Please choose another.')

    def activateCompany(self,cKey):
#        rowNum = len(self.activeCompanyKeys) * 2
#        self.inactiveCompanyKeys.remove(cKey)
#        self.activeCompanyKeys[pageNum].append(cKey)
        cC = self.companyContainers[cKey]
        activeFrame = cC.activeFrame
        doVarList = [[activeFrame],[]]
        undoVarList = [[],[]]
        Memento(
            'Activating company {0}.'.format(self.accounter.eName(cKey)),
            [cC.activate,self.updateGUI],
            doVarList,
            [cC.deactivate,self.updateGUI],
            undoVarList
        )
        
    def deactivateCompany(self,cKey):
        doVarList = [[],[]]
        cC = self.companyContainers[cKey]
        undoVarList = [[cC.activeFrame],[]]
        Memento(
            'Deactivating company {0}.'.format(self.accounter.eName(cKey)),
            [cC.deactivate,self.updateGUI],
            doVarList,
            [cC.activate,self.updateGUI],
            undoVarList
        )
        
    def removeCompany(self,cKey):
        doVarList = [[cKey],[]]
        company = self.accounter.companies[cKey]
        cData = company.__dict__
#        print "WARNING! Need to implement undoing companies' holdings and cash on undo."
        undoVarList = [[cData],[]]
        Memento(
            'Removing company {0}.'.format(self.accounter.eName(cKey)),
            [self.removeCompanyHandler,self.updateGUI],
            doVarList,
            [self.accounter.rezCompany,self.updateGUI],
            undoVarList
        )

    def removePlayer(self,pKey):
#        self.removePlayerHelper(pName)
#        self.accounter.removePlayer(pName)
#        self.updateGUI()
        pData = self.accounter.getPlayerData(pKey)
        doVarList = [[pKey],[]]
        undoVarList = [[pData],[]]
        print("WARNING! Need to implement undoing players' holdings and cash on undo.")
        Memento(
            'Removing player {0}.'.format(self.accounter.eName(pKey)),
            [self.removePlayerHandler,self.updateGUI],
            doVarList,
            [self.accounter.rezPlayer,self.updateGUI],
            undoVarList
        )

    def removeCompanyHandler(self,cKey):
        ## Remove deleted company vars.
#        company = self.accounter.companies[cKey]
        self.accounter.removeCompany(cKey)
        for subCKey in self.revenueVars.keys():
            del(self.companyHeldStockVarDicts[subCKey][cKey])
        for pKey in self.playerCashVars.keys():
            del(self.playerHeldStockVarDicts[pKey][cKey])
        del(self.companyContainers[cKey])
        del(self.revenueVars[cKey])
        del(self.withholdVars[cKey])
        del(self.IPOvars[cKey])
        del(self.poolVars[cKey])
        del(self.companyHeldStockVarDicts[cKey])
        del(self.companyCashVars[cKey])

    def removePlayerHandler(self,pKey):
        ## Remove deleted player vars.
        self.accounter.removePlayer(pKey)
        del(self.playerCashVars[pKey])
        del(self.playerHeldStockVarDicts[pKey])

    def removePlayerDialog(self):
#        self.tempDict = {'pname':None,'pcash':0}
        dd = DeleteDialog('player',self.accounter,self.removePlayer)
#        dd.frm.bind('<Destroy>',self.updateGUI)

    def removeCompanyDialog(self):
#        self.tempDict = {'pname':None,'pcash':0}
        dd = PickCompanyDialog('company',self.accounter,self.removeCompany,'delete')
#        dd.frm.bind('<Destroy>',self.updateGUI)

    def deactivateCompanyDialog(self):
        dad = PickCompanyDialog('company',self.accounter,self.deactivateCompany,'deactivate')
#        self.tempDict = {'pname':None,'pcash':0}
#        dd = DeleteDialog('company',self.accounter,self.removeCompany)
#        dd.frm.bind('<Destroy>',self.updateGUI)


    def addCompanyDialog(self):
        ccd = AddCompanyDialog(self.accounter,self.addCompany)
#        ccd.frm.bind('<Destroy>',self.updateGUI)
#        self.tempDict = {'cname':None}
#        acdb = Mbox(['Company name:','Total company stocks available:'],self.tempDict,['cname','stockNum'],self.addCompany,[None,int])

    def editCompanyDialog(self,cKey):
        ecd = EditCompanyDialog(self.accounter,self.editCompany,cKey)

    def editCompany(self,cKey,stockNum,mainColor,bgColor):
#        EXCEPTIONMONGOLS
        company = self.accounter.companies[cKey]
        previousStockNum =company.shareNum
        previousMainColour,previousBgColour = [company.colors[key] for key in ['fg','bg']]
        doVarList = [[cKey,stockNum],[cKey,mainColor,bgColor],[]]
        undoVarList = [[cKey,previousStockNum],[cKey,previousMainColour,previousBgColour],[]]
#        self.accounter.addPlayer(pName,pCash)
        cName = self.accounter.cName(cKey)
        Memento(
                "Edited company '{0}' to {1} shares.".format(cName,stockNum),
                [self.accounter.editCompany,self.accounter.setCompanyColors,self.updateGUI],
                doVarList,
                [self.accounter.editCompany,self.accounter.setCompanyColors,self.updateGUI],
                undoVarList
        )
    def changeStockDialog(self,entityKey,entityType,buysell):
        pcsd = StockChangeDialog(entityKey,self.accounter,entityType,buysell)
        pcsd.root = self
#        pcsd.frm.bind('<Destroy>',self.updateLabels)

    def transferMoneyDialog(self,entityType,entityKey):
        tmd = TransferMoneyDialog(entityType,entityKey,self.accounter)
        tmd.root = self
        
#    def changeMoneyDialog(self,entityName,entityType):
#        self.tempDict = {'name':entityName,'cash':0}
#        if entityType == 'player':
#            relevantFnc = self.changePlayerMoney
#        elif entityType == 'company':
#            relevantFnc = self.changeCompanyMoney
#        cpmdb = Mbox(
#            [u'Change cash of {0} by:\n(use negative number to subtract)'.format(entityName)],\
#            self.tempDict,
#            ['cash'],
#            relevantFnc,[int]
#        )
#        cpmdb.frm.bind('<Destroy>',self.updateLabels())

    def setStockValueDialog(self,event=None):
        ssvd = SetStockValueDialog(self.accounter)
        ssvd.root = self

    def changePlayerMoney(self,event=None):
        doVarList = [[self.tempDict['name'],'player',self.tempDict['cash']]]
        undoVarList = [[self.tempDict['name'],'player',-self.tempDict['cash']]]
        Memento(
                'Changed money of player {0} by {2}{1}.'.format(doVarList[0][0],doVarList[0][2],self.accounter['currencySign']),
                [self.accounter.changeMoney],
                doVarList,
                [self.accounter.changeMoney],
                undoVarList
        )
        self.updateLabels()
        
    def updateEntry(self,event=None):
        ## Put whatever is in enterable widgets (i.e. revenues) into the
        ## accounter.
        ## Also check inputs.
        for cKey in self.accounter.getCompanyKeys():
            thisVar = self.revenueVars[cKey]
            try:
                thisVar = int(thisVar.get())
                self.accounter.companies[cKey].lastRecordedRevenues = thisVar
            except ValueError:
#                showinfo(message = 'Must enter an integer in revenue box.\n(You smegmerchant).')
                thisVar.set(str(self.accounter.companies[cKey].lastRecordedRevenues))


#    def changeCompanyMoney(self,event=None):
#        doVarList = [[self.tempDict['name'],'company',self.tempDict['cash']]]
#        undoVarList = [[self.tempDict['name'],'company',-self.tempDict['cash']]]
#        Memento(
#                'Changed money of company {0} by {2}{1}.'.format(doVarList[0][0],doVarList[0][2],self.accounter['currencySign']),
#                [self.accounter.changeMoney],
#                doVarList,
#                [self.accounter.changeMoney],
#                undoVarList
#        )
#        self.updateLabels()

if __name__ == '__main__':
    DEFAULT_PLAYERS = []
#    a = Accounter()  
#    a.setup([],
#            separateIPO=False,currencySign = '$')
    tk18xx = Tk18xx()
    tk18xx.updateLabels()
    tk18xx.autosave = True
    tk18xx.gui.mainloop()
