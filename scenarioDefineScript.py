# -*- coding: utf-8 -*-
"""
Created on Fri Jun 01 17:54:09 2018

@author: Oddman
"""

from Administrator18xx import CompanyProfile, Accounter, Tk18xx

cp_1830Corp = CompanyProfile(
        '1830Corp',
        canOwnTypes=['private'],
        poolGoesToCompany=True,
        initialStockLocation='ipo'
)
cp_private = CompanyProfile('private',canWithhold=False)
cp_genericMinor = CompanyProfile(
        'genericMinor', canWithhold=False, canFiftyFifty = True,
        canOperate=False,
        canOwnTypes=['private'],
        initialStockLocation='pool')
cp_partialCapCorp = CompanyProfile(
        'partialCapCorp',canOwnTypes=['private','genericMinor'],
        canOwnOwnStock=True,initialStockLocation='self')
cp_partialCapCorp5050 = CompanyProfile(
        'partialCapCorp5050',canOwnTypes=['private','genericMinor'],
        canOwnOwnStock=True,initialStockLocation='self',
        canFiftyFifty = True)
cp_partialCapCorp1817 = CompanyProfile(
        'partialCapCorp1817',canOwnTypes=['partialCapCorp1817'],
        canOwnOwnStock=True,initialStockLocation='self',
        shortingAllowed = True,
        canFiftyFifty = True)

cp_1862EAcorp = CompanyProfile(
        '1862EACorp',canOwnTypes=[],
        canOwnOwnStock=True,initialStockLocation='self',
        canFiftyFifty = False, shortingAllowed=False)

cp_urNation = CompanyProfile('urNation',canOwnTypes=[],revenueMode='ur',
        canOwnOwnStock=True,initialStockLocation='self',
        canFiftyFifty = False, shortingAllowed=False)

cp_fullCapCorp1860 = CompanyProfile(
        'major1860',canOwnTypes=[],
        canOwnOwnStock=False,initialStockLocation='ipo',
        canFiftyFifty = False)

cp_partialCapCorp1867 = CompanyProfile(
    'partialCapCorp1867',canOwnTypes=['private'],
    canOwnOwnStock=True,initialStockLocation='self',
    canFiftyFifty = True)

cp_czBig = CompanyProfile(
    'czBig',canOwnTypes=['czMedium','czSmall','private'])
cp_czMedium = CompanyProfile(
    'czMedium',canOwnTypes=['czSmall','private'])
cp_czSmall = CompanyProfile(
    'czSmall',canOwnTypes=['private'])

cp_investor1880 = CompanyProfile(
    'investor1880',canOwnTypes=['private'],
    canOwnOwnStock=True,initialStockLocation='self',
    canFiftyFifty = False)

cp_partialCapCorp1880 = CompanyProfile(
    'major1880',canOwnTypes=[],
    canOwnOwnStock=False,initialStockLocation='pool',
    canFiftyFifty = False)

cp_fullCapCorp18GB = CompanyProfile(
        'major18GB',canOwnTypes=[],
        canOwnOwnStock=False,initialStockLocation='ipo',
        canFiftyFifty = False,IPOGoesToCompany=True,stockDeltaLocation='pool')

setup1830 = False
setup1835 = False
setup1870 = False
setupCalc = False

setupSteamOverHolland = False
setup1853 = False
setup1800 = False
setup1857 = False
setup1844 = False
setup18IR = False
setup18MEX = False
setup18VA = False
setup1880 = False
setup1895 = False
setup1861 = False
setup1865 = False
setup1858 = False
setup1822 = False
setup18FL = False
setup18Neb = False
setup18EU = False
setup1824 = False
setup1837 = False
setup18nw = False

if setup1837:
    a.currencySign = 'Kr'
    a.bankSize = 14268
    a.setOption('ownOwnStockByDefault',False)
    
if setup18nw:
    a.bankSize = 12000
    
if setup18FL:
    a.bankSize = 8000
    a.setOption('poolGoesToCompany',True)
    a.setOption('ownOwnStockByDefault',False)
    a.addCompany('TR',1)
    a.addCompany('POSC',1)
    a.addCompany('Term.',1)
    a.addCompany('ACL',5,ownIPO=False)
    a.setCompanyColors('ACL','#ffffff','#aa00aa')
    a.addCompany('FECR',5,ownIPO=False)
    a.setCompanyColors('FECR','#000000','#ff0000')

    a.addCompany('L&NR',5,ownIPO=False)
    a.setCompanyColors('L&NR','#ffffff','#0000ff')
    a.addCompany('Plant',5,ownIPO=False)
    a.setCompanyColors('Plant','#000000','#aaaaff')
    a.addCompany('SAL',5,ownIPO=False)
    a.setCompanyColors('SAL','#000000','#ff9900')
    a.addCompany('SR',5,ownIPO=False)
    a.setCompanyColors('SR','#000000','#00aa00')
    
if setup18Neb:
    a.bankSize = 6000
    a.setOption('ownOwnStockByDefault',True)
    a.addCompany('P1',1)
    a.addCompany('P2',1)
    a.addCompany('P3',1)
    a.addCompany('P4',1)
    a.addCompany('P5',1)
    a.addCompany('P6',1)

if setup1861:
    a.currencySign = 'R.'
    a.bankSize = 14000  
    a.addCompany('P1',1)
    a.addCompany('P2',1)
    a.addCompany('P3',1)
    a.addCompany('P4',1)
    a.addCompany('P5',1)

if setup1824:
    a.currencySign = 'G'
    a.bankSize = 12000   
    a.setOption('ownOwnStockByDefault',False)
    ## mountain RRs
    a.addCompany('M',6)
    a.lastRecordedRevenues['M'] = 150
    a.setStockValue('M',120)
    a.setCompanyColors('M','#000000','#ffffff')
    
    a.addCompany('BK/K1',2,ownIPO=True)
    a.setCompanyColors('BK/K1','#000000','#0080ff')
    
    a.addCompany('MS/K2',2,ownIPO=True)
    a.setCompanyColors('MS/K2','#000000','#ffff00')
    
    a.addCompany('CL/K3',2,ownIPO=True)
    a.setCompanyColors('CL/K3','#eeeeee','#000000')
    
    a.addCompany('SB/K4',2,ownIPO=True)
    a.setCompanyColors('SB/K4','#000000','#00c020')
    
    a.addCompany('BH',10)
    a.setCompanyColors('BH','#000000','#ff2020')
    
    a.addCompany('SB',2,ownIPO=True)
    a.setStockValue('SB',120)
    a.setCompanyColors('SB','#000000','#ff7000')
    
    a.addCompany('Sb2',1,ownIPO=True)
    a.setStockValue('Sb2',120)
    a.setCompanyColors('Sb2','#000000','#ff7000')
    
    a.addCompany('Sb3',1,ownIPO=True)
    a.setStockValue('Sb3',120)
    a.setCompanyColors('Sb3','#000000','#ff7000')

    a.addCompany('K&K',2,ownIPO=True)
    a.setStockValue('K&K',120)
    a.setCompanyColors('K&K','#000000','#ffbb80')
    
    a.addCompany('Kk/2',1,ownIPO=True)
    a.setStockValue('Kk/2',120)
    a.setCompanyColors('Kk/2','#000000','#ffbb80')

    a.addCompany('UG',2,ownIPO=True)
    a.setStockValue('UG',120)
    a.setCompanyColors('UG','#000000','#e80070')
    
    a.addCompany('Ug2',1,ownIPO=True)
    a.setStockValue('Ug2',120)
    a.setCompanyColors('Ug2','#000000','#e80070')

elif setup1835:
    a.bankSize = 12000
    a.setOption('poolGoesToCompany',True)
    a.currencySign = 'RM'

elif setup1830:
    a.bankSize = 12000
    a.setOption('poolGoesToCompany',True)
#        a.addPlayer('ot',600)
#        a.addPlayer('mies',600)
#        a.addPlayer('borbor',600)
    a.addCompany('PRR',10)
    a.setCompanyColors('PRR','#ffffff','#008040')
    a.addCompany('B&O',10)
    a.setCompanyColors('B&O','#000000','#5eaeff')
    a.addCompany('C&O',10)
    a.setCompanyColors('C&O','#ffff00','#0000ff')
    a.addCompany('NY&NH',10)
    a.setCompanyColors('NY&NH','#000000','#ff8040')
    a.addCompany('NYC',10)
    a.setCompanyColors('NYC','#ffffff','#000000')
    a.addCompany('B&M',10)
    a.setCompanyColors('B&M','#f2cf0b','#408080')
    a.addCompany('CanPac',10)
    a.setCompanyColors('CanPac','#ffffff','#ff0000')
    a.addCompany('Erie',10)
    a.setCompanyColors('Erie','#ffff00','#000000')
    a.addCompany('SV',1)
    a.addCompany('C&SL',1)
    a.addCompany('DH',1)
    a.addCompany('MH',1)
    a.addCompany('C&A',1)
    a.addCompany('BO(m)',1)
    a.lastRecordedRevenues['SV'] = 5
    a.lastRecordedRevenues['C&SL'] = 10
    a.lastRecordedRevenues['DH'] = 15
    a.lastRecordedRevenues['MH'] = 20
    a.lastRecordedRevenues['C&A'] = 25
    a.lastRecordedRevenues['BO(m)'] = 30
    a.setStockValue('SV',20)
    a.setStockValue('C&SL',40)
    a.setStockValue('DH',70)
    a.setStockValue('MH',110)
    a.setStockValue('C&A',160)
    a.setStockValue('BO(m)',220)
    a.doAccounts()
elif setup1844:
    a.bankSize = 12000
    a.setOption('poolGoesToCompany',True)
    a.addCompany('P',21)
    a.lastRecordedRevenues['P'] = 105
    a.addCompany('FNM',10)
    a.setCompanyColors('FNM','#ffffff','#009000')
#        a.changeStockHoldings('bank','IPO','FNM',-3)
#        a.changeStockHoldings('bank','pool','FNM',3)
    a.addCompany('MOB',10)
    a.setCompanyColors('MOB','#ffffff','#ffa020')
#        a.addCompany('MOB',10)
#        a.setCompanyColors('MOB','#ffff00','#0000ff')
    a.addCompany('RhB',10)
    a.setCompanyColors('RhB','#ffffff','#ff0000')
    a.addCompany('BLS',10)
    a.setCompanyColors('BLS','#ffffff','#eeee00')
    a.addCompany('STB',10)
    a.setCompanyColors('STB','#ffffff','#2020ff')
    a.addCompany('AB',10)
    a.setCompanyColors('AB','#2020ff','#dddddd')

    a.addCompany('NOB',4)
    a.setCompanyColors('NOB','#2020bb','#dddddd')
    a.addCompany('SCB',4)
    a.setCompanyColors('SCB','#ff0000','#000000')
    a.addCompany('VSB',4)
    a.setCompanyColors('VSB','#ffffff','#40ff40')
    a.addCompany('JS',4)
    a.setCompanyColors('JS','#10cc10','#dddddd')
    a.addCompany('GB',4)
    a.setCompanyColors('GB','#000000','#ffff00')

    a.addCompany('JN',5)
    a.setCompanyColors('JN','#ffffff','#40ff40')
    a.addCompany('ChA',5)
    a.setCompanyColors('ChA','#ffffff','#4040ff')
    a.addCompany('VZ',5)
    a.setCompanyColors('VZ','#ffffff','#ff4040')

    a.doAccounts()

elif setup18VA:
    a.currencySign = '$'
    a.bankSize = 8000
    a.addCompany('B&O',10,ownIPO=True,minorness=False)
    a.setCompanyColors('B&O','#ffffff','#0000ff')
    a.setOption('internalCompanyRevenue',False)
    a.setOption('poolGoesToCompany',True)
    a.addCompany('SR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('SR','#000000','#00ac00')        
    a.addCompany('N&W',5,ownIPO=True,minorness=False)
    a.setCompanyColors('N&W','#ffffff','#000000')          
    a.addCompany('RF&P',5,ownIPO=True,minorness=False)
    a.setCompanyColors('RF&P','#000000','#ff0000')
    a.addCompany('VGN',5,ownIPO=True,minorness=False)
    a.setCompanyColors('VGN','#ffffff','#800080')
    a.addCompany('WM',5,ownIPO=True,minorness=False)
    a.setCompanyColors('WM','#000000','#ff8000')
    a.addCompany('C&O',5,ownIPO=True,minorness=False)
    a.setCompanyColors('C&O','#000000','#0080ff')
    a.addCompany('REA',1)
    a.addCompany('TIW',1)
    a.addCompany('PY',1)
    
elif setupSteamOverHolland:
    a.currencySign = 'Æ’'
    a.bankSize = 999999
    a.addCompany('P',19,ownIPO=False,minorness=True)
    a.setCompanyColors('P','#000000','#bbbbbb')
    a.addCompany('AR',10,ownIPO=True,minorness=False)
    a.setCompanyColors('AR','#000000','#ff0000')
    a.addCompany('NBD',10,ownIPO=True,minorness=False)
    a.setCompanyColors('NBD','#ffffff','#0000ff')
    a.addCompany('NCS',10,ownIPO=True,minorness=False)
    a.setCompanyColors('NCS','#ffffff','#800080')
    a.addCompany('NRS',10,ownIPO=True,minorness=False)
    a.setCompanyColors('NRS','#000000','#ffff00')
    a.addCompany('NSM',10,ownIPO=True,minorness=False)
    a.setCompanyColors('NSM','#000000','#ffffff')
    a.addCompany('HYSM',10,ownIPO=True,minorness=False)
    a.setCompanyColors('HYSM','#000000','#00ac00')
    a.addCompany('OSM',10,ownIPO=True,minorness=False)
    a.setCompanyColors('OSM','#ffffff','#000000')
    

elif setup1858:
    a.currencySign = 'P'
    a.bankSize = 12000
    a.addCompany('RPR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('RPR','#000000','#ff0000')
    a.addCompany('AR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('AR','#ffffff','#0000bb')
    a.addCompany('TBFR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('TBFR','#ffffff','#800080')
    a.addCompany('MZAR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('MZAR','#000000','#ffff00')
    a.addCompany('ZPBR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('ZPBR','#000000','#ff8000')
    a.addCompany('WR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('WR','#000000','#00ac00')
    a.addCompany('NR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('NR','#ffffff','#000000')
    a.addCompany('AVTR',5,ownIPO=True,minorness=False)
    a.setCompanyColors('AVTR','#000000','#00aaff')

elif setup18MEX:
    a.currencySign = '$'
    a.setOption('poolGoesToCompany',True)
    a.bankSize = 9000
    a.addCompany('CHI',10,ownIPO=False,minorness=False)
    a.setCompanyColors('CHI','#000000','#ff0000')
    a.addCompany('SP',10,ownIPO=False,minorness=False)
    a.setCompanyColors('SP','#000000','#0080ff')
    a.addCompany('UdeY',10,ownIPO=False,minorness=False)
    a.setCompanyColors('UdeY','#ffffff','#800080')
    a.addCompany('FCP',10,ownIPO=False,minorness=False)
    a.setCompanyColors('FCP','#000000','#ffff00')
    a.addCompany('TEX',10,ownIPO=False,minorness=False)
    a.setCompanyColors('TEX','#000000','#ff8000')
    a.addCompany('NdM',10,ownIPO=False,minorness=False)
    a.setCompanyColors('NdM','#000000','#00ac00')
    a.addCompany('MCR',10,ownIPO=False,minorness=False)
    a.setCompanyColors('MCR','#ffffff','#000000')
    a.addCompany('MEX',10,ownIPO=False,minorness=False)
    a.setCompanyColors('MEX','#000000','#aaaaaa')

    a.addCompany('1',1,ownIPO=False,minorness=False)
    a.setCompanyColors('1','#000000','#ffffff')
    a.addCompany('2',1,ownIPO=False,minorness=False)
    a.setCompanyColors('2','#000000','#ffffff')
    a.addCompany('A',2,ownIPO=False,minorness=False)
    a.setCompanyColors('A','#000000','#ffffff')
    a.addCompany('B',2,ownIPO=False,minorness=False)
    a.setCompanyColors('B','#000000','#ffffff')
    a.addCompany('C',2,ownIPO=False,minorness=False)
    a.setCompanyColors('C','#000000','#ffffff')
    a.addCompany('6',1,ownIPO=False,minorness=False)
    a.setCompanyColors('6','#000000','#ffffff')
    a.addCompany('7',1,ownIPO=False,minorness=False)
    a.setCompanyColors('7','#000000','#ffffff')

elif setup1895:
    a.currencySign = 'RM'
    a.bankSize = 6528
    a.addCompany('ML',1)
    a.setStockValue('ML',20)
    a.lastRecordedRevenues['ML'] = 5
    a.addCompany('STA',10)
    a.setCompanyColors('STA','#ffffff','#0000ff')
    a.changeStockHoldings('STA','company','STA',10)
    a.changeStockHoldings('IPO','bank','STA',-10)
    a.addCompany('OME',10)
    a.setCompanyColors('OME','#000000','#ff0000')
    a.changeStockHoldings('OME','company','OME',10)
    a.changeStockHoldings('IPO','bank','OME',-10)
    a.addCompany('SD',10)
    a.setCompanyColors('SD','#ffffff','#000000')
    a.changeStockHoldings('SD','company','SD',10)
    a.changeStockHoldings('IPO','bank','SD',-10)
    a.addCompany('OB',10)
    a.setCompanyColors('OB','#000000','#00ac00')
    a.changeStockHoldings('OB','company','OB',10)
    a.changeStockHoldings('IPO','bank','OB',-10)
    a.addCompany('NS',10)
    a.setCompanyColors('NS','#000000','#ffff00')
    a.changeStockHoldings('NS','company','NS',10)
    a.changeStockHoldings('IPO','bank','NS',-10)

elif setup1800:
    a.bankSize = 3720
    a.addCompany('D&RGW',10)
    a.setCompanyColors('D&RGW','#000000','#ff0000')
    a.changeStockHoldings('D&RGW','company','D&RGW',10)
    a.changeStockHoldings('IPO','bank','D&RGW',-10)
    a.addCompany('C&S',10)
    a.setCompanyColors('C&S','#ffffff','#0000ff')
    a.changeStockHoldings('C&S','company','C&S',10)
    a.changeStockHoldings('IPO','bank','C&S',-10)
    a.addCompany('C&W',10)
    a.setCompanyColors('C&W','#ffffff','#00ff00')
    a.changeStockHoldings('C&W','company','C&W',10)
    a.changeStockHoldings('IPO','bank','C&W',-10)
    a.addCompany('MT',1)
    a.addCompany('DSL',1)
    a.addCompany('RGS',1)
    a.lastRecordedRevenues['MT'] = 5
    a.lastRecordedRevenues['DSL'] = 10
    a.lastRecordedRevenues['RGS'] = 20
    a.setStockValue('MT',20)
    a.setStockValue('DSL',70)
    a.setStockValue('RGS',150)
    a.doAccounts()

elif setup1870:
    a.bankSize = 12000
    a.setOption('IPOGoesToCompany',True)
    a.addCompany('Santa Fe',10)
    a.setCompanyColors('Santa Fe','#ffffff','#53a9ff')
    a.addCompany('MP',10)
    a.setCompanyColors('MP','#ffffff','#ff4242')
    a.addCompany('KATY',10)
    a.setCompanyColors('KATY','#ffffff','#007712')
    a.addCompany('FW',10)
    a.setCompanyColors('FW','#000000','#ff0000')
    a.addCompany('Frisco',10)
    a.setCompanyColors('Frisco','#ffffff','#ca0000')
    a.addCompany('TP',10)
    a.setCompanyColors('TP','#000000','#ffffff')
    a.addCompany('GMO',10)
    a.setCompanyColors('GMO','#ffffff','#f42650')
    a.addCompany('SSW',10)
    a.setCompanyColors('SSW','#ffffff','#000080')
    a.addCompany('IC',10)
    a.setCompanyColors('IC','#ffffff','#000000')
    a.addCompany('SP',10)
    a.setCompanyColors('SP','#ff4600','#000000')
    a.addCompany('GRSC',1,minorness=True)
    a.setStockValue('GRSC',20)
    a.lastRecordedRevenues['GRSC'] = 5
    a.addCompany('MRBC',1,minorness=True)
    a.setStockValue('MRBC',40)
    a.lastRecordedRevenues['MRBC'] = 10
    a.addCompany('SCC',1,minorness=True)
    a.setStockValue('SCC',50)
    a.lastRecordedRevenues['SCC'] = 10
    a.addCompany('GSC',1,minorness=True)
    a.setStockValue('GSC',80)
    a.lastRecordedRevenues['GSC'] = 15
    a.addCompany('MKT(m)',1,minorness=True)
    a.setStockValue('MKT(m)',160)
    a.lastRecordedRevenues['MKT(m)'] = 20
    


elif setup1857:
    a.bankSize = 12000
    a.setOption('poolGoesToCompany',True)
    a.addCompany('CC',10)
    a.setCompanyColors('CC','#000000','#ff8040')
    a.addCompany('SF',10)
    a.setCompanyColors('SF','#000000','#ff0000')
    a.addCompany('CA',10)
    a.setCompanyColors('CA','#000000','#900090')
    a.addCompany('BAC',10)
    a.setCompanyColors('BAC','#000000','#00ffff')
    a.addCompany('AGW',10)
    a.setCompanyColors('AGW','#000000','#ffff00')
    a.addCompany('NCA',10)
    a.setCompanyColors('NCA','#000000','#bbbbbb')
    a.addCompany('BBN',10)
    a.setCompanyColors('BBN','#000000','#ff9000')
    a.addCompany('BAS',10)
    a.setCompanyColors('BAS','#000000','#00ff00')

    a.addCompany('CHBA',1,minorness=True)
    a.setStockValue('CHBA',40)
    a.lastRecordedRevenues['CHBA'] = 10
    a.addCompany('SST',1,minorness=True)
    a.setStockValue('SST',40)
    a.lastRecordedRevenues['SST'] = 10
    a.addCompany('SDCC',1,minorness=True)
    a.setStockValue('SDCC',40)
    a.lastRecordedRevenues['SDCC'] = 10
    a.addCompany('SPSC',1,minorness=True)
    a.setStockValue('SPSC',40)
    a.lastRecordedRevenues['SPSC'] = 10
    a.addCompany('SBM',1,minorness=True)
    a.setStockValue('SBM',40)
    a.lastRecordedRevenues['SBM'] = 10
    a.addCompany('SZLC',1,minorness=True)
    a.setStockValue('SZLC',40)
    a.lastRecordedRevenues['SZLC'] = 10

elif setup18IR:
    a.bankSize = 4000
    a.currencySign = '£'
    
#elif setup1862EA:
#    a.bankSize = 15000
#    a.currencySign = '£'

elif setup1865:
    a.bankSize = 8000
    a.currencySign = 'L.'

    a.addCompany('FMS',10)
    a.setCompanyColors('FMS','#000000','#ff0000')
    a.changeStockHoldings('FMS','company','FMS',10)
    a.changeStockHoldings('IPO','bank','FMS',-10)

    a.addCompany('SFSS',10)
    a.setCompanyColors('SFSS','#ffffff','#0000ff')
    a.changeStockHoldings('SFSS','company','SFSS',10)
    a.changeStockHoldings('IPO','bank','SFSS',-10)

    a.addCompany('FCS',10)
    a.setCompanyColors('FCS','#ffffff','#804000')
    a.changeStockHoldings('FCS','company','FCS',10)
    a.changeStockHoldings('IPO','bank','FCS',-10)

    a.addCompany('FA',10)
    a.setCompanyColors('FA','#000000','#ffff00')
    a.changeStockHoldings('FA','company','FA',10)
    a.changeStockHoldings('IPO','bank','FA',-10)

    a.addCompany('CFC',10)
    a.setCompanyColors('CFC','#000000','#bbbbbb')
    a.changeStockHoldings('CFC','company','CFC',10)
    a.changeStockHoldings('IPO','bank','CFC',-10)

    a.addCompany('CFD',10)
    a.setCompanyColors('CFD','#ffffff','#000000')
    a.changeStockHoldings('CFD','company','CFD',10)
    a.changeStockHoldings('IPO','bank','CFD',-10)

    a.addCompany('RCSF',10)
    a.setCompanyColors('RCSF','#000000','#00ac00')
    a.changeStockHoldings('RCSF','company','RCSF',10)
    a.changeStockHoldings('IPO','bank','RCSF',-10)

    a.addCompany('SFS',10)
    a.setCompanyColors('SFS','#000000','#ff8000')
    a.changeStockHoldings('SFS','company','SFS',10)
    a.changeStockHoldings('IPO','bank','SFS',-10)

    a.addCompany('Maritimes',8)
    a.setCompanyColors('Maritimes','#000000','#ffffff')

elif setup1853:
    a.currencySign = '£'
    
    a.bankSize = 12000
    a.addCompany('EIR',10)
    a.setCompanyColors('EIR','#ffffff','#cc2020')
    a.setStockValue('EIR',100)
    a.addCompany('GIP',10)
    a.setCompanyColors('GIP','#ffffff','#dd9000')
    a.setStockValue('GIP',96)
    a.addCompany('NWR',10)
    a.setCompanyColors('NWR','#ffffff','#404040')
    a.setStockValue('NWR',92)
    a.addCompany('BNR',10)
    a.setCompanyColors('BNR','#ffffff','#109010')
    a.setStockValue('BNR',88)
    a.addCompany('BBCI',10)
    a.setCompanyColors('BBCI','#ffffff','#2020cc')
    a.setStockValue('BBCI',84)
    a.addCompany('MSM',10)
    a.setCompanyColors('MSM','#ffffff','#bb00ff')
    a.setStockValue('MSM',80)
    a.addCompany('SIR',10)
    a.setCompanyColors('SIR','#000000','#ffff00')
    a.setStockValue('SIR',75)
    a.addCompany('ECR',10)
    a.setCompanyColors('ECR','#ffffff','#8080ff')
    a.setStockValue('ECR',70)
    
def setup(title,accounter):
    a = accounter
    title = str(title)
    if title == '1822':
        a.setup('1822: The Railways of Great Britain',
                [cp_private,cp_genericMinor,cp_partialCapCorp5050],
                separateIPO=False,currencySign = '£')
        a.currencySign = '£'
        a.bankSize = 12000
        for i in range(30):
            a.addCompany(str(i+1),'genericMinor',2,active=False)
        a.addCompany('LNWR','partialCapCorp5050',10,'#ffffff','#000000',False)
        a.addCompany('GWR','partialCapCorp5050',10,'#ffffff','#009900',False)
        a.addCompany('CR','partialCapCorp5050',10,'#ffffff','#0000ff',False)
        a.addCompany('LYR','partialCapCorp5050',10,'#ffffff','#a000ff',False)
        a.addCompany('LBSCR','partialCapCorp5050',10,'#000000','#ffff00',False)
        a.addCompany('MR','partialCapCorp5050',10,'#000000','#ff0000',False)
        a.addCompany('NER','partialCapCorp5050',10,'#000000','#66ff66',False)
        a.addCompany('NBR','partialCapCorp5050',10,'#ffffff','#996600',False)
        a.addCompany('SECR','partialCapCorp5050',10,'#000000','#ff9900',False)
        a.addCompany('SWR','partialCapCorp5050',10,'#000000','#999999',False)
        a.addCompany('HR','genericMinor',1,'#0000ff','#ffffff',False)
        a.addCompany('CWR','genericMinor',1,'#ff0000','#ffffff',False)

    if title == '1822CA':
        a.setup(title,[cp_private,cp_genericMinor,cp_partialCapCorp5050],
                separateIPO=False,currencySign = '£')
        a.currencySign = 'C$'
        a.bankSize = 12000
        for i in range(30):
            a.addCompany(str(i+1),'genericMinor',2,active=False)
        a.addCompany('GT','partialCapCorp5050',10,'#ffffff','#000000',False)
        a.addCompany('CNoR','partialCapCorp5050',10,'#ffffff','#009900',False)
        a.addCompany('GWR','partialCapCorp5050',10,'#ffffff','#0000ff',False)
        a.addCompany('QMOO','partialCapCorp5050',10,'#ffffff','#a000ff',False)
        a.addCompany('ICR','partialCapCorp5050',10,'#000000','#ffff00',False)
        a.addCompany('CPR','partialCapCorp5050',10,'#000000','#ff0000',False)
        a.addCompany('PGE','partialCapCorp5050',10,'#000000','#66ff66',False)
        a.addCompany('NTR','partialCapCorp5050',10,'#ffffff','#996600',False)
        a.addCompany('GTP','partialCapCorp5050',10,'#000000','#ff9900',False)
        a.addCompany('GNW','partialCapCorp5050',10,'#000000','#999999',False)
        a.addCompany('P8','genericMinor',1,'#0000ff','#ffffff',False)
        a.addCompany('P9','genericMinor',1,'#ff0000','#ffffff',False)
                     
    if title in ('1862','1862EA'):
        a.setup('1862: Railway Mania in the Eastern Counties',
                [cp_partialCapCorp], separateIPO=True, currencySign = '£')
        a.addCompany('E&H','partialCapCorp',10,'#ff8040','#ffff80')
        a.addCompany('ECR','partialCapCorp',10,'#ff80b0','#400080')           
        a.addCompany('ENR','partialCapCorp',10,'#0080ff','#ffff00')
        a.addCompany('ESR','partialCapCorp',10,'#80ff80','#008040')
        a.addCompany('EUR','partialCapCorp',10,'#400040','#ff0000')
        a.addCompany('FDR','partialCapCorp',10,'#b00000','#ff8040')
        a.addCompany('I&B','partialCapCorp',10,'#b0b0b0','#000000')
        a.addCompany('L&D','partialCapCorp',10,'#0000ff','#ff8000')
        a.addCompany('L&E','partialCapCorp',10,'#ffff00','#ff0000')
        a.addCompany('L&H','partialCapCorp',10,'#004080','#80b0ff')
        a.addCompany('N&B','partialCapCorp',10,'#804000','#b0b0b0')
        a.addCompany('N&E','partialCapCorp',10,'#004000','#b0b0b0')
        a.addCompany('N&S','partialCapCorp',10,'#ff0000','#80b0ff')                 
        a.addCompany('NGC','partialCapCorp',10,'#000000','#ffff00')
        a.addCompany('SVR','partialCapCorp',10,'#004000','#808000')
        a.addCompany('W&F','partialCapCorp',10,'#80ff80','#008000')
        a.addCompany('WNR','partialCapCorp',10,'#ff4040','#606030')
        a.addCompany('WVR','partialCapCorp',10,'#ff80b0','#800040')
        a.addCompany('WStI','partialCapCorp',10,'#ffff00','#800040')
        a.addCompany('Y&N','partialCapCorp',10,'#ff0000','#000000')
        a.bankSize = 15000
        
    if title == '1834':
        a.setup('1834 [Winsome; Belgium]',[cp_private,cp_1830Corp],
                separateIPO=True,currencySign = '$')
        a.bankSize = 7500
        a.scenario = '1834'
        a.addCompany('P','private',15)
    
        a.addCompany('LUX','1830Corp',10,'#ffffff','#ff00ff')
        a.addCompany('OST','1830Corp',10,'#ffffff','#0000ff')
        a.addCompany('MON','1830Corp',10,'#000000','#00ff00')
        a.addCompany('ANT','1830Corp',10,'#000000','#ffff00')
        a.addCompany('LIE','1830Corp',10,'#ffffff','#ff0000')

    if title == '1849':
        cp_partialCapCorp1849 = CompanyProfile(
            'partialCapCorp1849',canOwnTypes=['private'],
            canOwnOwnStock=True,initialStockLocation='self')
        a.setup('1849: The Game of Sicilian Railways',[cp_private,cp_partialCapCorp1849],
                separateIPO=False,currencySign = 'L.')
        a.bankSize = 7760
        a.addCompany('Akragas','partialCapCorp1849',10,'#000000','#ff0080')
        a.changeMoney(1,-40)
    
        a.addCompany('Garibaldi','partialCapCorp1849',10,'#ffffff','#ff0000')
        a.changeMoney(2,-40)
    
        a.addCompany('RCS','partialCapCorp1849',10,'#000000','#ff8000')
        a.changeMoney(3,-130)
    
        a.addCompany('IFT','partialCapCorp1849',10,'#ffffff','#000070')
        a.changeMoney(4,-90)
    
        a.addCompany('Lilibeo','partialCapCorp1849',10,'#000000','#ffff00')
        a.changeMoney(5,-40)
    
        a.addCompany('Archimede','partialCapCorp1849',10,'#ffffff','#008000')
        a.changeMoney(6,-30)
    
        a.addCompany('SCE','private',1)
        a.addCompany('SIGI','private',1)
        a.addCompany('CNM','private',1)
        a.addCompany('SMS','private',1)
        a.addCompany('RSA','private',1)
        sceKey = a.cKey('SCE')
        a.companies[sceKey].lastRecordedRevenue = 5
        a.setStockValue(sceKey,20)
        sigiKey = a.cKey('SIGI')
        a.companies[sigiKey].lastRecordedRevenue = 10
        a.setStockValue(sigiKey,45)
        cnmKey = a.cKey('CNM')
        a.companies[cnmKey].lastRecordedRevenue = 15
        a.setStockValue(cnmKey,75)
        smsKey = a.cKey('SMS')
        a.companies[smsKey].lastRecordedRevenue = 20
        a.setStockValue(smsKey,110)
        rsaKey = a.cKey('RSA')
        a.companies[rsaKey].lastRecordedRevenue = 25
        a.setStockValue(rsaKey,150)

    if title == '1860':
        a.setup('1860: Railways on the Isle of Wight',[cp_private,cp_fullCapCorp1860],
                separateIPO=True,currencySign = '£')
        a.bankSize = 10000
        a.addCompany('P','private',18)
        a.addCompany('C&N','major1860',10,'#ffffff','#0000ff')
        a.addCompany('IOW','major1860',10,'#ffffff','#ff0000')
        a.addCompany('IWNJ','major1860',10,'#ffffff','#000000')
        a.addCompany('FYN','major1860',10,'#ffffff','#009000')
        a.addCompany('BHI&R','major1860',10,'#ffffff','#ff20ff')
        a.addCompany('NGStL','major1860',10,'#000000','#ffff00')
        a.addCompany('S&C','major1860',10,'#dd00dd','#ff9000')
        a.addCompany('VYSC','major1860',10,'#000000','#40ff40')
                     
    if title == '1846':
        a.setup('1846: The Race for the Midwest',[cp_private,cp_genericMinor,cp_partialCapCorp5050],
                separateIPO=False,currencySign = '$')
        a.bankSize = 6000
        a.scenario = '1846: Race for the Midwest'
        a.addCompany('P','private',20)
        a.addCompany('MS','genericMinor',1)
        a.changeMoney(a.cKey('MS'),60)
        a.addCompany('B4','genericMinor',1)
        a.changeMoney(a.cKey('B4'),40)
        a.addCompany('PRR','partialCapCorp5050',10,'#ffffff','#ff0000')
        a.addCompany('B&O','partialCapCorp5050',10,'#ffff00','#0000ff')
        a.addCompany('C&O','partialCapCorp5050',10,'#000000','#5eaeff')
        a.addCompany('NYC','partialCapCorp5050',10,'#ffffff','#000000')
        a.addCompany('GTW','partialCapCorp5050',10,'#ffffff','#ff0000')
        a.addCompany('Erie','partialCapCorp5050',10,'#000000','#ffff00')
        a.addCompany('IC','partialCapCorp5050',10,'#ffffff','#006000') 
                     
    if title == '1817':
        a.setup('1817',[cp_partialCapCorp1817],
                separateIPO=False,currencySign = '$')
        a.bankSize = 100000
        a.addCompany('ASR','partialCapCorp1817',2,'#000000','#ff0000')
        a.addCompany('U','partialCapCorp1817',2,'#ffffff','#0000ff')
        a.addCompany('DLW','partialCapCorp1817',2,'#ffffff','#804000')
        a.addCompany('AAR','partialCapCorp1817',2,'#000000','#ffff00')
        a.addCompany('GTW','partialCapCorp1817',2,'#000000','#ff4040')
        a.addCompany('BLE','partialCapCorp1817',2,'#ffffff','#000000')
        a.addCompany('PSN','partialCapCorp1817',2,'#000000','#00ac00')
        a.addCompany('W&T','partialCapCorp1817',2,'#000000','#ff8000')
        a.addCompany('H','partialCapCorp1817',2,'#000000','#66ccff')
        a.addCompany('S','partialCapCorp1817',2,'#000000','#ffcc00')
        a.addCompany('BAR','partialCapCorp1817',2,'#ffffff','#ff0000')
        a.addCompany('J','partialCapCorp1817',2,'#ffffff','#009900')
        a.addCompany('BRC','partialCapCorp1817',2,'#000000','#ffcc00')
        a.addCompany('W','partialCapCorp1817',2,'#800000','#ffff00')
        a.addCompany('STR','partialCapCorp1817',2,'#ffff00','#ff2000')
        a.addCompany('WC','partialCapCorp1817',2,'#ffffff','#800000')
        a.addCompany('ME','partialCapCorp1817',2,'#ff0000','#ffff00')
        a.addCompany('PW','partialCapCorp1817',2,'#804000','#ffbb80')
        a.addCompany('RUT','partialCapCorp1817',2,'#ffff00','#008000')
        a.addCompany('PLE','partialCapCorp1817',2,'#000000','#80ff00')
                     
    if title == 'Ur':
        a.setup('Ur 1830 BC',[cp_urNation],
                separateIPO=False,currencySign = 'Spl')
        a.bankSize = 50000
        a.addCompany('A','urNation',20)
        a.addCompany('B','urNation',20)
        a.addCompany('C','urNation',20)
        a.addCompany('D','urNation',20)
        
    if title in ('18Lilliput','lilliput','18lilliput'):
        a.setup('18Lilliput',[cp_private,cp_czBig,cp_czMedium,cp_czSmall],
                separateIPO=False,currencySign = '£')
        a.bankSize = 99999
        a.addCompany('SR','czBig',10,'#000000','#8080ff')
        a.addCompany('MR','czBig',10,'#000000','#ff0000')
        a.addCompany('GVR','czBig',10,'#000000','#00ff00')
        a.addCompany('LR','czBig',10,'#000000','#ffff00')
        a.addCompany('PR','czBig',10,'#eeeeee','#111111') 
        a.addCompany('TR','czBig',10,'#eeeeee','#ff00ff')
        a.addCompany('WR','czBig',10,'#000000','#ff8000')
        a.addCompany('BNR','czBig',10,'#eeeeee','#f04000')
                     
    if title == '1867':
        a.setup('1867: The Railways of Canada',
                [cp_private,cp_genericMinor,cp_partialCapCorp1867],
                separateIPO=False,currencySign = '$')
        a.bankSize = 15000
        
        ## p'tjes
        a.addCompany('P','private',20)
        a.companies[a.cKey('P')].lastRecordedRevenue = 100
        ## yellow minors
        a.addCompany('BBG','genericMinor',2)
        a.addCompany('BO','genericMinor',2)
        a.addCompany('CS','genericMinor',2)
        a.addCompany('CV','genericMinor',2)
        a.addCompany('KP','genericMinor',2)
        a.addCompany('LPS','genericMinor',2)
        a.addCompany('OP','genericMinor',2)
        a.addCompany('SLA','genericMinor',2)
        a.addCompany('TGB','genericMinor',2)
        a.addCompany('TN','genericMinor',2)
        
        ## green minors
        a.addCompany('AE','genericMinor',2)
        a.addCompany('CA','genericMinor',2)
        a.addCompany('NYO','genericMinor',2)
        a.addCompany('PM','genericMinor',2)
        a.addCompany('QLS','genericMinor',2)
        a.addCompany('THB','genericMinor',2)
    
        ## majors
        a.addCompany('CN','partialCapCorp1867',10)
        a.addCompany('CO','partialCapCorp1867',10)
        a.addCompany('CP','partialCapCorp1867',10)
        a.addCompany('GT','partialCapCorp1867',10)
        a.addCompany('GW','partialCapCorp1867',10)
        a.addCompany('IC','partialCapCorp1867',10)
        a.addCompany('NT','partialCapCorp1867',10)
        a.addCompany('NYC','partialCapCorp1867',10)
    
    if title == '18CZ':
        a.setup('18CZ',[cp_private,cp_czBig,cp_czMedium,cp_czSmall],
                separateIPO=False,currencySign = 'Kr')
        a.currencySign = 'Kr'
        a.scenario = '18CZ'
        a.bankSize = 99999
        a.addCompany('P','private',42)
        a.setCompanyColors(a.cKey('P'),'#000000','#ffffff')
        a.companies[a.cKey('P')].lastRecordedRevenue = 210
        a.addCompany('VBW','czSmall',4,'#eeeeee','#20dd20')
        a.addCompany('EKJ','czSmall',4,'#000000','#ffffff')
        a.addCompany('OFE','czSmall',4,'#eeeeee','#ff0000')
        a.addCompany('MW','czSmall',4,'#eeeeee','#40aa40')
        a.addCompany('BCB','czSmall',4,'#eeeeee','#804000')
        
        a.addCompany('ATE','czMedium',5,'#000000','#ffff00')
        a.addCompany('KFN','czMedium',5,'#000000','#80ffff')
        a.addCompany('BTE','czMedium',5,'#000000','#c0ff80')
        a.addCompany('NWB','czMedium',5,'#000000','#ffff80')
        a.addCompany('BN','czMedium',5,'#000000','#bbbbbb')
    
        a.addCompany('PR','czBig',10,'#eeeeee','#000000')
        a.addCompany('kk','czBig',10,'#eeeeee','#802000')
        a.addCompany('BY','czBig',10,'#eeeeee','#004080')
        a.addCompany('SX','czBig',10,'#eeeeee','#c00000')
        a.addCompany('Ug','czBig',10,'#eeeeee','#800040')

    if title == '1880':
        a.setup('1880: China',
                [cp_private,cp_investor1880,cp_partialCapCorp1880],
                separateIPO=False,currencySign = '¥')
        a.bankSize = 100000
        ## p'tjes
        a.addCompany('P','private',15)
        a.companies[a.cKey('P')].lastRecordedRevenue = 75
        ## yellow minors
        a.addCompany('1','investor1880',1)
        a.addCompany('2','investor1880',1)
        a.addCompany('3','investor1880',1)
        a.addCompany('4','investor1880',1)
        a.addCompany('5','investor1880',1)
        a.addCompany('6','investor1880',1)
        a.addCompany('7','investor1880',1)
    
        ## majors
        a.addCompany('BCR','major1880',10,'#d0d0d0','#000080',True)
        a.addCompany('SCR','major1880',10,'#d0d0d0','#008000',False)
        a.addCompany('LHR','major1880',10,'#000000','#b0b0b0',False)
        a.addCompany('HKR','major1880',10,'#000000','#b0ff80',False)
        a.addCompany('WNR','major1880',10,'#000000','#ffff80',False)
        a.addCompany('NJR','major1880',10,'#000000','#b08000',False)
        a.addCompany('CKR','major1880',10,'#000000','#ff0000',False)
        a.addCompany('QSR','major1880',10,'#000000','#0080ff',False)
        a.addCompany('JHA','major1880',10,'#000000','#ff8000',False)
        a.addCompany('BZU','major1880',10,'#000000','#e0e0d0',False)
        a.addCompany('JLR','major1880',10,'#000000','#a0b0c0',False)
        a.addCompany('JHU','major1880',10,'#d0d0d0','#000000',False)
        a.addCompany('NXR','major1880',10,'#000000','#ff80b0',False)
        a.addCompany('JGG','major1880',10,'#000000','#b080b0',False)

    if title == '18EU':
        a.setup('18EU',
                [cp_genericMinor,cp_partialCapCorp5050],
                separateIPO=False,currencySign = '£')
        a.bankSize = 12000   
        for i in range(15):
            a.addCompany('M{0}'.format(str(i+1)),'genericMinor',1)
        a.addCompany('SNCF','partialCapCorp5050',10,'#000000','#ff0000',False)
        a.addCompany('NS','partialCapCorp5050',10,'#000000','#ffff00',False)
        a.addCompany('KBS','partialCapCorp5050',10,'#000000','#a0ffff',False)
        a.addCompany('KPEV','partialCapCorp5050',10,'#000000','#0080ff',False)
        a.addCompany('DRB','partialCapCorp5050',10,'#e0e0e0','#000000',False)
        a.addCompany('KKOS','partialCapCorp5050',10,'#000000','#ffffb0',False)
        a.addCompany('B','partialCapCorp5050',10,'#000000','#ff8000',False)
        a.addCompany('FS','partialCapCorp5050',10,'#000000','#00b000',False)

    if title == '1830':
        a.setup('1830: Railways & Robber Barons',
                [cp_private,cp_1830Corp],
                separateIPO=True,currencySign = '$')
        a.bankSize = 12000
                    
        a.addCompany('SV','private',1)
        svKey = a.cKey('SV')
        a.companies[svKey].lastRecordedRevenue = 5
        a.setStockValue(svKey,20)
        
        a.addCompany('C&StL','private',1)
        cstlKey = a.cKey('C&StL')
        a.companies[cstlKey].lastRecordedRevenue = 10
        a.setStockValue(cstlKey,40)
        
        a.addCompany('D&H','private',1)
        dhKey = a.cKey('D&H')
        a.companies[dhKey].lastRecordedRevenue = 15
        a.setStockValue(dhKey,70)
        
        a.addCompany('M&H','private',1)
        mhKey = a.cKey('M&H')
        a.companies[mhKey].lastRecordedRevenue = 20
        a.setStockValue(mhKey,110)    

        a.addCompany('C&A','private',1)
        caKey = a.cKey('C&A')
        a.companies[caKey].lastRecordedRevenue = 25
        a.setStockValue(caKey,160)    

        a.addCompany('B&O (m)','private',1)
        boKey = a.cKey('B&O (m)')
        a.companies[boKey].lastRecordedRevenue = 30
        a.setStockValue(boKey,220)  

        a.addCompany('B&O','1830Corp',10,'#f0f0f0','#5eaeff',True)        
        a.addCompany('PRR','1830Corp',10,'#f0f0f0','#008040',True)
        a.addCompany('C&O','1830Corp',10,'#ffff00','#0000ff',False)
        a.addCompany('NNH','1830Corp',10,'#000000','#ff8040',False)
        a.addCompany('NYC','1830Corp',10,'#f0f0f0','#000000',False)
        a.addCompany('B&M','1830Corp',10,'#f2cf0b','#408080',False)
        a.addCompany('CPR','1830Corp',10,'#000000','#ff0000',False)
        a.addCompany('ER','1830Corp',10,'#ffff00','#000000',False)

    if title == '18MEX':
        a.setup('18MEX',
                [cp_private,cp_1830Corp,cp_genericMinor],
                separateIPO=True,currencySign = '$')
        a.bankSize = 9000
                    
        a.addCompany('1','private',1)
        a.addCompany('2','private',1)
        a.addCompany('A','genericMinor',1)
        a.addCompany('B','genericMinor',1)
        a.addCompany('C','genericMinor',1)
        a.addCompany('6','private',1)
        a.addCompany('7','private',1)
        

        a.addCompany('CHI','1830Corp',10,'#000000','#ff0000',False)        
        a.addCompany('SP','1830Corp',10,'#000000','#0080ff',False)
        a.addCompany('UdeY','1830Corp',10,'#ffffff','#800080',False)
        a.addCompany('FCP','1830Corp',10,'#000000','#ffff00',False)
        a.addCompany('TEX','1830Corp',10,'#000000','#ff8000',False)
        a.addCompany('NdM','1830Corp',10,'#000000','#00ac00',False)
        a.addCompany('MCR','1830Corp',10,'#ffffff','#000000',False)
        a.addCompany('MEX','1830Corp',10,'#000000','#aaaaaa',False)
                     
    if title == '18GB':
        a.setup('18GB: The Railways of Great Britain',[cp_private,cp_fullCapCorp18GB],
                separateIPO=True,currencySign = '£')
        a.bankSize = 100000
        a.addCompany('P','private',27)
    
        a.addCompany('LNWR','major18GB',5,'#ffffff','#000000',True)
        a.addCompany('MR','major18GB',5,'#000000','#ff0000',False)
        a.addCompany('LYR','major18GB',5,'#ffffff','#a000ff',False)     
        a.addCompany('NER','major18GB',5,'#000000','#66ff66',False) 
        a.addCompany('NBR','major18GB',5,'#000000','#ff9900',False)
        a.addCompany('GWR','major18GB',5,'#ffffff','#009900',False)
        a.addCompany('CR','major18GB',5,'#ffffff','#0000ff',False)
        a.addCompany('LSWR','major18GB',5,'#000000','#ffff00',False)
        a.addCompany('GER','major18GB',5,'#000000','#80c0ff',False)
        a.addCompany('SWR','major18GB',5,'#000000','#999999',False)
        a.addCompany('MSLR','major18GB',5,'#ffffff','#996600',False)
        a.addCompany('GSWR','major18GB',5,'#000000','#ff80c0',False)

#### PLAYERS
#a.addPlayer('Boris',400)
#a.addPlayer('Xeryus',480)
#a.addPlayer('Ot',400)
#a.addPlayer('Ewoud',400)
#a.addPlayer('Tom',400)
#a.addPlayer('Jasper',400)
#a.addPlayer('Mel',400)
#a.addPlayer('Floor',400)
#a.addPlayer('Boris',525)
#a.addPlayer('Mischa',525)
#a.addPlayer('Ot',525)
#a.addPlayer('Jasper',400)
#a.addPlayer('Mel',400)
#a.addPlayer('Floor',400)
#a.addPlayer('Tristan',0)
#a.addPlayer('Eelco',0)
#a.addPlayer('Rene',420)
#a.addPlayer('Wouter',400)
#a.addPlayer('Gilles',480)

#a.addPlayer('4. Red (aggro)',400)
#a.addPlayer('1. White (opportuno)',400)
#a.addPlayer('3. Blue (conservative)',400)
#a.addPlayer('2. Green (longterm)',400)

#DEFAULT_PLAYERS = ['Boris','Ot','Xeryus']
        
if __name__ == '__main__':
    [   '1862EA',1822,'1822CA',1849,1834,1846,
         1817,1867,'18CZ','Ur','18lilliput',
         1880,'18EU',1830,'18MEX','18GB'
     ]
    title = '18GB'
    a = Accounter()
    setup(title, a)
    tk18xx = Tk18xx(a)
    tk18xx.updateLabels()
    tk18xx.autosave = True
    tk18xx.gui.mainloop()