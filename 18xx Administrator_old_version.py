# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 18:07:57 2018

@author: Oddman
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 21:07:20 2015

TODO:
    
* Score screen: date & 18xx-variant
* No company share possessions
* undo-shit
    # delete player
    # delete company
* Popup-locations!
* Scrolling / collapsing company list / scaling
* company sorting
* Scenarios
* transfer to/from IPO/pool button
* Select company, show buttons

* Who-is-president-of-what
* specify minorness

switching companies between tabs

@author: Oddman
"""

import Tkinter as tki
import ttk as ttk
import cPickle as pkl
import tkFont
import tkFileDialog
import tkColorChooser
#from os import path
import os
import sqlite3
from tkMessageBox import showinfo
#import matplotlib
#import numpy as np

PADVAL = 1
HEADERFONT = 14
BIGFONT = 14
SHOW_COMPSTOCK = True

UNDOSTACK = []
REDOSTACK = []

class Memento:
    def __init__(self,logMessage,doFuncList,doVarList,undoFuncList,undoVarList):
        global REDOSTACK
        self.logMessage = logMessage
        self.doFuncList = doFuncList
        self.doVarList = doVarList
        self.undoFuncList = undoFuncList
        self.undoVarList = undoVarList
        ## Done creatin'? Fire!
        self.do()
        ## Also clear the redo stack, because a new thing has been done.
        REDOSTACK = []

    def do(self):
        UNDOSTACK.append(self)
#        print self.doFuncList
        for i,doFunc in enumerate(self.doFuncList):
#            print doFunc
            doFunc(*self.doVarList[i])

    def undo(self):
        REDOSTACK.append(self)
        UNDOSTACK.pop()
        for i,undoFunc in enumerate(self.undoFuncList):
            undoFunc(*self.undoVarList[i])

class Accounter:
    def setup(self):
        self.logbook = []
        ## Options (18xx-specific stuff)
        self.poolGoesToCompany = False
        self.IPOGoesToCompany = False
        self.internalCompanyRevenue = True
        self.ownOwnStockByDefault = True
        self.urMode = False
        self.shortingAllowed = False
        ## </options>
        self.bankSize = 0
        self.playerNames = []
        self.players = {}
        self.companyNames = []
        self.companies = {}
        self.companyTotalStock = {}
        self.lastRecordedRevenues = {}
        self.stockValues = {}
        self.stockPool = []
        self.stockIPO = []
        self.companyColors = []
        self.currencySign = '$'
        self.companyMinorness = {}

    def __getitem__(self,key):
        return self.__dict__[key]

    def __setitem__(self,key,value):
        self.__dict__.__setitem__(key,value)

    def serialize(self,filename):
        with open(filename,'w') as outFile:
            pkl.dump(self.__dict__,outFile)

    def load(self,filename):
        with open(filename,'r') as inFile:
            self.__dict__ = pkl.load(inFile)
#            print self.__dict__

    def doAccounts(self):
        moneyUsed = 0
        header = 'entityName,entityType,subName,value'
        outList = [header]
        for playerName,playerData in self.players.items():
            outList.append(','.join([playerName,'playerMoney','',str(playerData[0])]))
            moneyUsed += playerData[0]
            for i,companyName in enumerate(self.companyNames):
                outList.append(','.join([playerName,'stockHolding',companyName,str(playerData[i+1])]))
        for companyName,companyData in self.companies.items():
            outList.append(','.join([companyName,'companyMoney','',str(companyData[0])]))
            moneyUsed += companyData[0]
            for i,shareName in enumerate(self.companyNames):
                outList.append(','.join([companyName,'stockHolding',shareName,str(companyData[i+1])]))
        outList.append("moneyUsed,moneyUsed,,{0}".format(moneyUsed))
#        print '\n'.join(outList)
        self.stats = outList
#        print self.stats
        return self.bankSize-moneyUsed

    def addPlayer(self,pName,startingFunds):
        assert pName.find('.') == -1, 'Illegal character "." in player name'
        self.playerNames.append(pName)
        self.players[pName] = [startingFunds] + [0 for company in self.companyNames]

    def addCompany(self,companyName,totalStock,ownIPO=False,minorness=False):
        assert companyName.find('.') == -1, 'Illegal character "." in company name'
#        print 'Own IPO? :'+str(ownIPO)
        if companyName in self.companyNames:
            raise ValueError
        else:
            self.companyNames.append(companyName)
            self.companyTotalStock[companyName] = totalStock
            self.lastRecordedRevenues[companyName] = 0
            for playerName,playerHoldings in self.players.items():
                playerHoldings.append(0)
            for cName,cHoldings in self.companies.items():
                cHoldings.append(0)
            self.stockPool.append(0)
            self.companyColors.append(['#000000','#ffffff'])
            self.companies[companyName] = [0]+[0 for company in self.companyNames]
            self.stockValues[companyName] = 0
            if ownIPO:
                self.stockIPO.append(0)
                self.companies[companyName][self.companyNames.index(companyName)+1] = totalStock
            else:
                self.stockIPO.append(totalStock)

    def editCompany(self,cName,stockNum,ownIPO):
        originalStockNum = self.companyTotalStock[cName]
        stockDiff = stockNum - originalStockNum
        cInd = self.companyNames.index(cName)
        self.companyTotalStock[cName] += stockDiff
        if ownIPO:
            self.companies[cName][cInd+1] += stockDiff
        else:
            self.stockPool[cInd] += stockDiff

    def changeStockHoldings(self,entityName,entityType,stockCompanyName,stockDelta):
        stockIndex = self.companyNames.index(stockCompanyName)
        if entityType == 'bank':
            if entityName == 'IPO':
                self.stockIPO[stockIndex] += stockDelta
            elif entityName == 'pool':
                self.stockPool[stockIndex] += stockDelta
            else:
                return None
        if entityType == 'player':
            self.players[entityName][stockIndex + 1] += stockDelta
        elif entityType == 'company':
            self.companies[entityName][stockIndex + 1] += stockDelta

    def changeMoney(self,entityName,entityType,moneyDelta,relevantCompany=None):
        if entityType == 'bank':
            if relevantCompany != None and entityType == 'pool' and self.poolGoesToCompany:
                self.changeMoney(relevantCompany,'company',moneyDelta)
            return None
        if entityType == 'player':
            self.players[entityName][0] += moneyDelta
        elif entityType == 'company':
            self.companies[entityName][0] += moneyDelta

    def findStocks(self,companyName):
        stockIndex = self.companyNames.index(companyName)
        inIPO = self.stockIPO[stockIndex]
        inPool = self.stockPool[stockIndex]
        found = 0
        playerHeld = []
        companyHeld = []
        for playerName,playerHoldings in self.players.items():
            found += playerHoldings[stockIndex+1]
            playerHeld.append((playerName,playerHoldings[stockIndex+1]))
        for cName,cHoldings in self.companies.items():
            found += cHoldings[stockIndex+1]
            companyHeld.append((cName,cHoldings[stockIndex+1]))
        return (inIPO,inPool,playerHeld,companyHeld)

    def removeCompany(self,companyName):
#        self.log('Deleted company {0} - lost $ {1}',companyName,self.companies[companyName][0])
        deadCompanyIndex = self.companyNames.index(companyName)
#        print deadCompanyIndex
#        print self.accounter.companies[deadCompanyIndex]
        ## remove references to company in player holdings.
        self.players = dict(
            (playerName,playerHoldings[:deadCompanyIndex+1]+ playerHoldings[deadCompanyIndex+2:])
            for playerName,playerHoldings
            in self.players.items()
        )
        self.companies.__delitem__(companyName)
        self.companyTotalStock.__delitem__(companyName)
        self.lastRecordedRevenues.__delitem__(companyName)
        self.companies = dict(
            (cName,cHoldings[:deadCompanyIndex+1]+ cHoldings[deadCompanyIndex+2:])
            for cName,cHoldings
            in self.companies.items()
        )
        self.companyNames.remove(companyName)
        self.stockValues.__delitem__(companyName)
        del self.stockIPO[deadCompanyIndex]
        del self.stockPool[deadCompanyIndex]
        del self.companyColors[deadCompanyIndex]
#        del self.companyMinorness[deadCompanyIndex]
#        print self.companies

    def removePlayer(self,playerName):
        self.playerNames.remove(playerName)
        self.players.__delitem__(playerName)

    def calcNetWorth(self,playerName):
        playerHoldings = self.players[playerName]
        totalWorth = playerHoldings[0]
        for i,cName in enumerate(self.companyNames):
            cI = i+1
            totalWorth += playerHoldings[cI] * self.stockValues[cName]
        return totalWorth

    def getTotalHeldStock(self,companyName):
        companyIndex = self.companyNames.index(companyName)
        if self.urMode:
            totalHeld = self.stockPool[companyIndex] + sum(
                    [
                            playerHoldings[companyIndex+1]
                            for playerName,playerHoldings 
                            in self.players.items()
                    ]
            )
        else:
            totalHeld = self.companyTotalStock[companyName]
        return totalHeld
    
    def getCompanyColor(self,companyName,fgbg):
        companyIndex = self.companyNames.index(companyName)
        if fgbg == 'fg':
            idx = 0
        elif fgbg == 'bg':
            idx = 1
        else:
            raise ValueError
        return self.companyColors[companyIndex][idx]
    
    def getStockValue(self,companyName):
        return self.stockValues[companyName]
    
    def getEntityPossessedCompanies(self,entity,entityType):
        if entityType == 'player':
            relevantCompanies = [
                self.companyNames[i]
                for i,holding
                in enumerate(self.players[entity][1:])
                if holding > 0
            ]
#                print salableCompanies
        if entityType == 'company':
            relevantCompanies = [
                self.companyNames[i]
                for i,holding
                in enumerate(self.companies[entity][1:])
                if holding > 0
            ]
        return relevantCompanies
    
    def isTradeValid(self,shareCompany,sourceEntityName,sourceEntityType,
               targetEntityName,targetEntityType,shareNum,totalAmount):
        shareIndex = self.companyNames.index(shareCompany)
#        sourceEntityStock = self.getStockCount(sourceEntityName,sourceEntityType)
        if sourceEntityType == 'player':
            sourceEntityStock = self.players[sourceEntityName][shareIndex+1]
        elif sourceEntityType == 'company':
            sourceEntityStock = self.companies[sourceEntityName][shareIndex+1]
        elif sourceEntityType == 'bank':
            inIPO,inPool,playerOwned,companyOwned = self.findStocks(shareCompany)
            if sourceEntityName == 'IPO':
                sourceEntityStock = inIPO
            elif sourceEntityName == 'pool':
                sourceEntityStock = inPool
        else:
            raise KeyError
#        print sourceEntityType
        if targetEntityType == 'player':
            targetEntityMoney = self.players[targetEntityName][0]
        elif targetEntityType == 'company':
            targetEntityMoney = self.companies[targetEntityName][0]
        elif targetEntityType == 'bank':
            targetEntityMoney = self.bankSize * 100
        else:
            raise KeyError
        if (
            ((sourceEntityStock < shareNum) and not self.accounter.shortingAllowed)
            or (targetEntityMoney < totalAmount)
        ):
            return False
        else:
            return True
        
    def payDividend(self,amount,companyName):
        companyIndex = self.companyNames.index(companyName)
        totalHeld = self.getTotalHeldStock(companyName)
#            income = int(])
        incomePerStock = amount/float(totalHeld)
#        print 'CTS = {0}'.format(self.companyTotalStock[companyName])
        landedMoney = 0
        if self.poolGoesToCompany:
            income = int(self.stockPool[companyIndex]*incomePerStock)
#            print income
            self.changeMoney(
                companyName,'company',income
            )
        if self.IPOGoesToCompany:
            income = int(self.stockIPO[companyIndex]*incomePerStock)
            self.changeMoney(
                companyName,'company',income
            )
        for playerName,playerHoldings in self.players.items():
            income = int(playerHoldings[companyIndex+1]*incomePerStock)
            self.changeMoney(
                playerName,'player',income
            )
            landedMoney+=income
        
#            print 'company revenuing'
        for cName,companyHoldings in self.companies.items():
            if self.internalCompanyRevenue:
                income = int(companyHoldings[companyIndex+1]*incomePerStock)
                self.changeMoney(
                    cName,'company',income
                )
                landedMoney+=income
#        remainder = amount - self.companyTotalStock[companyName] * incomePerStock
        landedMoney += int(self.stockPool[companyIndex]*incomePerStock)
        if not self.urMode:
            landedMoney += int(self.stockIPO[companyIndex]*incomePerStock)
        remainder = amount - landedMoney
#        print remainder
        self.changeMoney(companyName,'company',remainder)

    def companyDividend(self,companyName,totalIncome,kind='100%'):
#        self.log('Operating {0} for ${1}; withhold = {2}',companyName,totalIncome,withhold)
#        companyIndex = self.companyNames.index(companyName)
#        self.lastRecordedRevenues[companyName] = totalIncome
        if kind == '100%':
            self.payDividend(totalIncome,companyName)
        elif kind == 'withhold':
            self.changeMoney(companyName,'company',totalIncome)
        elif kind == '50/50':
            half = int(totalIncome / 2.)
#            print half
            if (    self.companyTotalStock[companyName] == 10 
                    and (totalIncome % 10 == 0) 
                    and self.urMode == False
                ):
                remainder = ((totalIncome / 10) % 2) * 5
                remainder = remainder * -1 if totalIncome < 0 else remainder
#                print remainder
                self.payDividend(half + remainder,companyName)
                self.changeMoney(companyName,'company',half - remainder)
            else:
#                print '?!?!'
                remainder = totalIncome - (2*half)
                self.payDividend(half,companyName)
                self.changeMoney(companyName,'company',half + remainder)
#            print '50/50!'

    def setStockValue(self,companyName,value):
#        self.log('Setting stock value of {0} to ${1}.',companyName,value)
        self.stockValues[companyName] = value

    def setOption(self,optionVar,optionVal):
        if optionVar == 'poolGoesToCompany':
#            self.log('Setting dividend from pool going to company (1830-style ) to {0}.',optionVal)
            self.poolGoesToCompany = optionVal
        elif optionVar == 'IPOGoesToCompany':
#            self.log('Setting dividend from IPO going to company (1830-style ) to {0}.',optionVal)
            self.IPOGoesToCompany = optionVal
        elif optionVar == 'ownOwnStockByDefault':
            self.ownOwnStockByDefault = optionVal
        elif optionVar == 'internalCompanyRevenue':
            self.internalCompanyRevenue = optionVal
        elif optionVar == 'urMode':
            self.urMode = optionVal
        else:
            raise ValueError('Unknown option: {0}'.format(optionVar))

    def setCompanyColors(self,companyName,mainColor,bgColor):
#        self.log('Setting colors of {0} to {1} and {2}.',companyName,mainColor,bgColor)
        cInd = self.companyNames.index(companyName)
        self.companyColors[cInd] = [mainColor,bgColor]

class IntegerEntry(tki.Entry):
    def __init__(self,*args,**keywords):
        self.targetDict = {}
        tki.Entry.__init__(self,*args,**keywords)
        self.entryVar = keywords['textvariable']
        self.bind('<Button-3>',self.popDialog)
    def popDialog(self,event):
#        print self.__dict__
        integerDialog = IntegerDialog(self,self.targetDict,self.entryVar)
#        print 'derp!'
#        print self.master

class Mbox(object):
    root = None
    def __init__(self, msg, targetDict, targetKeys, targetFunc = None, typeCast = [None]):
        assert len(msg) == len(targetKeys)
        self.targetDict = targetDict
        self.typeCast = typeCast
        self.targetKeys = targetKeys
        self.targetFunc = targetFunc
        self.top = tki.Toplevel(Mbox.root)
#        tki.Toplevel()
        self.entries = []
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        for i,dictKey in enumerate(self.targetKeys):
            label = tki.Label(self.frm, text=msg[i])
            label.grid(row=i,column=0,padx=PADVAL,pady=PADVAL)
            thisEntry = tki.Entry(self.frm)
            self.entries.append(thisEntry)
            thisEntry.grid(row=i,column=1,padx=PADVAL,pady=PADVAL)
        self.b_submit = tki.Button(self.frm, text='Submit',command = self.entry_to_dict)
        self.b_submit.grid(row=i+1,column=0,padx=PADVAL,pady=PADVAL)
        self.top.bind('<Key-Return>',self.entry_to_dict)
        self.top.bind('<Key-KP_Enter>',self.entry_to_dict)
        b_cancel = tki.Button(self.frm, text='Cancel')
        b_cancel['command'] = self.top.destroy
        b_cancel.grid(row=i+1,column=1,padx=PADVAL,pady=PADVAL)
        self.entries[0].focus_set()

    def entry_to_dict(self,event=None):
        goToExit = [False for i in self.targetKeys]
        for i,targetKey in enumerate(self.targetKeys):
            data = self.entries[i].get()
            if data:
                if self.typeCast[i] == int:
                    try:
                        data = int(data)
                        self.targetDict[targetKey] = data
                        goToExit[i] = True
                    except ValueError:
                        showinfo(message = 'Must enter an integer in box {0}.\n(You smegmerchant).'.format(i+1))
                else:
                    self.targetDict[targetKey] = data
                    goToExit[i] = True
        if all(goToExit):
            if self.targetFunc is not None:
                self.targetFunc()
            self.top.destroy()

def incrementer(self,multiple,i):
    try:
        v = self.textvariable.get()
        v = int(v)
        v += multiple * (10**(6-i))
        self.textvariable.set(str(v))
    except ValueError:
        self.textvariable.set(self.origVal)

class IntegerDialog:
    def __init__(self,superEntry,targetDict,textvariable):
        self.mainFont = tkFont.Font(size=27)
        self.tinyFont = tkFont.Font(size=14)
        self.orders = 3
        self.superEntry = superEntry
        self.textvariable = textvariable
        self.textvariable.set(['0',self.textvariable.get()][self.textvariable.get()!=''])
        self.origVal = self.textvariable.get()
        self.top = tki.Toplevel(self.superEntry.master)
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.grid(row = 0,column=0)
        self.entry = tki.Entry(self.frm,textvariable=self.textvariable,font=self.mainFont,justify='right',width=7)
        self.entry.grid(row = 3,column=0,columnspan=self.orders)
        self.entry.focus_set()
        for i in range(self.orders):
            def thisButton(i=i):
                self.incrementer(1,i)
            plusButton = tki.Button(self.frm,text='+1' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=2)
            def thisButton(i=i):
                self.incrementer(2,i)
            plusButton = tki.Button(self.frm,text='+2' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=1)
            def thisButton(i=i):
                self.incrementer(5,i)
            plusButton = tki.Button(self.frm,text='+5' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=0)
            def thisButton(i=i):
                self.incrementer(-1,i)
            plusButton = tki.Button(self.frm,text='-1' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=4)
            def thisButton(i=i):
                self.incrementer(-2,i)
            plusButton = tki.Button(self.frm,text='-2' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=5)
            def thisButton(i=i):
                self.incrementer(-5,i)
            plusButton = tki.Button(self.frm,text='-5' + '0' * (self.orders - i - 1),command=thisButton,font=self.tinyFont,width=3)
            plusButton.grid(column=i,row=6)
        confirmButton = tki.Button(self.frm,text='Confirm',command=self.top.destroy)
        confirmButton.grid(column=0,row=7,padx=PADVAL,pady=PADVAL,columnspan=3)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.cancel)
        cancelButton.grid(column=2,row=7,padx=PADVAL,pady=PADVAL,columnspan=3)
        resetButton = tki.Button(self.frm,text='Set at 0',command=self.reset,height=7)
        resetButton.grid(column=3,row=0,padx=PADVAL,pady=PADVAL,rowspan=7)
    def cancel(self,event=None):
        self.textvariable.set(self.origVal)
        self.top.destroy()
    def confirm(self,event=None):
        self.top.destroy()
    def reset(self,event=None):
        self.textvariable.set('0')
    def incrementer(self,multiple,i):
        try:
            v = self.textvariable.get()
            v = int(v)
            v += multiple * (10**(self.orders-1-i))
            self.textvariable.set(str(v))
        except ValueError:
            self.textvariable.set(self.origVal)


class AddCompanyDialog:
    root = None
    def __init__(self,accounter,addCompanyFunc):
        self.top = tki.Toplevel(AddCompanyDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.mainColor = '#000000'
        self.bgColor = '#c0c0c0'
        self.stockVar = tki.StringVar(value='10')
        self.cNameVar = tki.StringVar()
        self.ownIPOVar = tki.BooleanVar(value=accounter['ownOwnStockByDefault'])
        self.addCompanyFunc = addCompanyFunc
        nameLabel = tki.Label(self.frm,text='Name:')
        nameLabel.grid(column = 0, row = 0,padx=PADVAL,pady=PADVAL,sticky='W')
        nameEntry = tki.Entry(self.frm,textvariable = self.cNameVar)
        nameEntry.grid(column = 1, row = 0,padx=PADVAL,pady=PADVAL,sticky='W')
        stockNumLabel = tki.Label(self.frm,text='Number of stocks:')
        stockNumLabel.grid(column = 0, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        stockNumEntry = IntegerEntry(self.frm,textvariable=self.stockVar,width=3)
        stockNumEntry.grid(column = 1, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        ownIPOLabel = tki.Label(self.frm,text='Owns own stock')
        ownIPOLabel.grid(column = 0, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        ownIPOCheck = tki.Checkbutton(self.frm,variable=self.ownIPOVar)
        ownIPOCheck.grid(column = 1, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        colorChooserButton1 = tki.Button(self.frm,text='Main Color',command = self.pickMainColor)
        colorChooserButton1.grid(column = 0, row = 3,padx=PADVAL,pady=PADVAL,sticky='E')
        colorChooserButton2 = tki.Button(self.frm,text='Background Color',command = self.pickBackgroundColor)
        colorChooserButton2.grid(column = 1, row = 3,padx=PADVAL,pady=PADVAL,sticky='W')
        confirmButton = tki.Button(self.frm,text='Confirm',command=self.confirm)
        confirmButton.grid(column=0,row=4,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=4,padx=PADVAL,pady=PADVAL)
        nameEntry.focus_set()
    def pickMainColor(self):
        void,self.mainColor = tkColorChooser.askcolor(color=self.mainColor,parent=self.top)
    def pickBackgroundColor(self):
        void,self.bgColor = tkColorChooser.askcolor(color=self.bgColor,parent=self.top)
    def confirm(self,event=None):
        cName = self.cNameVar.get()
        try:
            stockNum = int(self.stockVar.get())
        except ValueError:
            showinfo(message = u'Stock number must be an integer.\n(You smelly viking.)'.format(cName))
        self.addCompanyFunc(cName,stockNum,self.ownIPOVar.get(),self.mainColor,self.bgColor)
#        self.accounter.addCompany()
#        self.accounter.setCompanyColors()
        self.top.destroy()

class EditCompanyDialog:
    root = None
    def __init__(self,accounter,editCompanyFunc,cName):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.mainColor = self.accounter.getCompanyColor(cName,'fg')
        self.bgColor = self.accounter.getCompanyColor(cName,'bg')
        self.stockVar = tki.StringVar(value='10')
        self.cNameVar = tki.StringVar()
        self.cName = cName
        self.ownIPOVar = tki.BooleanVar(value=self.accounter['ownOwnStockByDefault'])
        self.editCompanyFunc = editCompanyFunc
        stockNumLabel = tki.Label(self.frm,text='Set number of stocks to:')
        stockNumLabel.grid(column = 0, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        stockNumEntry = IntegerEntry(self.frm,textvariable=self.stockVar,width=3)
        stockNumEntry.grid(column = 1, row = 1,padx=PADVAL,pady=PADVAL,sticky='W')
        ownIPOLabel = tki.Label(self.frm,text='Stock source is company (pool otherwise)')
        ownIPOLabel.grid(column = 0, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        ownIPOCheck = tki.Checkbutton(self.frm,variable=self.ownIPOVar)
        ownIPOCheck.grid(column = 1, row = 2,padx=PADVAL,pady=PADVAL,sticky='W')
        colorChooserButton1 = tki.Button(self.frm,text='Main Color',command = self.pickMainColor)
        colorChooserButton1.grid(column = 0, row = 3,padx=PADVAL,pady=PADVAL,sticky='E')
        colorChooserButton2 = tki.Button(self.frm,text='Background Color',command = self.pickBackgroundColor)
        colorChooserButton2.grid(column = 1, row = 3,padx=PADVAL,pady=PADVAL,sticky='W')
        confirmButton = tki.Button(self.frm,text='Confirm',command=self.confirm)
        confirmButton.grid(column=0,row=4,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=4,padx=PADVAL,pady=PADVAL)
        stockNumEntry.focus_set()
    def pickMainColor(self):
        void,self.mainColor = tkColorChooser.askcolor(color=self.mainColor,parent=self.top)
    def pickBackgroundColor(self):
        void,self.bgColor = tkColorChooser.askcolor(color=self.bgColor,parent=self.top)
    def confirm(self,event=None):
        try:
            stockNum = int(self.stockVar.get())
        except ValueError:
            showinfo(message = u'Stock number must be an integer.\n(You smelly viking.)'.format(cName))
        self.editCompanyFunc(self.cName,stockNum,self.ownIPOVar.get(),self.mainColor,self.bgColor)
#        self.accounter.addCompany()
#        self.accounter.setCompanyColors()
        self.top.destroy()

class TransferMoneyDialog:
    root = None
    def __init__(self,entityType,entityName,accounter):
        self.top = tki.Toplevel(TransferMoneyDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.accounter = accounter
        self.textvariable = tki.StringVar(value='')
        self.entityType = entityType
        self.entityName = entityName
        self.bankFrame = tki.LabelFrame(self.frm,text='Bank')
        self.bankFrame.grid(row=1,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        self.playerFrame = tki.LabelFrame(self.frm,text='Players')
        self.playerFrame.grid(row=2,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        self.companyFrame = tki.LabelFrame(self.frm,text='Companies')
        self.companyFrame.grid(row=3,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        self.reasonFrame = tki.LabelFrame(self.frm,text='Reason')
        self.reasonFrame.grid(row=4,column=0,padx=PADVAL,pady=PADVAL,columnspan=3)
        label1 = tki.Label(self.frm,text='Transfer {0}'.format(self.accounter['currencySign']))
        label1.grid(row = 0,column=0)
        self.entry = IntegerEntry(self.frm,justify='right',textvariable=self.textvariable)
        self.entry.grid(row = 0,column=1)
        self.entry.focus_set()
        label2 = tki.Label(self.frm,text='to')
        label2.grid(row = 0,column=2)
        self.target = tki.StringVar()
        self.target.set('bank.bank')
        self.reason = tki.StringVar()
        self.reason.set('another reason')
        thisRB = ttk.Radiobutton(
                self.bankFrame,
                value='bank.bank',
                text='Bank',
                variable=self.target,
                ## Once a radioButton is clicked, set up the sources.
#                style = '{0}.TRadiobutton'.format(companyName),
#                command = self.onRB2
        )
        thisRB.grid(column=0,row=0)
        for i,companyName in enumerate(self.accounter.companyNames):
#            cInd = self.accounter['companyNames'].index(companyName)
            rbStyle = ttk.Style()
            rbStyle.configure(
                '{0}.TRadiobutton'.format(companyName),
                foreground = self.accounter.getCompanyColor(companyName,'fg'),
                background = self.accounter.getCompanyColor(companyName,'bg')
            )
            thisRB = ttk.Radiobutton(
                self.companyFrame,
                value='company.{0}'.format(companyName),text='{0}'.format(companyName),
                variable=self.target,
                ## Once a radioButton is clicked, set up the sources.
                style = '{0}.TRadiobutton'.format(companyName),
#                command = self.onRB2
            )
            thisRB.grid(column=i,row=0)

        for i,playerName in enumerate(self.accounter.playerNames):
#            pInd = self.accounter['playerNames'].index(playerName)
#            rbStyle = ttk.Style()
#            rbStyle.configure(
#                '{0}.TRadiobutton'.format(companyName),
#                foreground = self.accounter['companyColors'][cInd][0],
#                background=self.accounter['companyColors'][cInd][1]
#            )
            thisRB = ttk.Radiobutton(
                self.playerFrame,
                value='player.{0}'.format(playerName),
                text='{0}'.format(playerName),
                variable=self.target,
                ## Once a radioButton is clicked, set up the sources.
#                style = '{0}.TRadiobutton'.format(companyName),
#                command = self.onRB2
            )
            thisRB.grid(column=i,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='a train',
            text='a train',
            variable=self.reason,
        )
        thisRB.grid(column=0,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='track laying',
            text='track laying',
            variable=self.reason,
        )
        thisRB.grid(column=1,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='loan/bond',
            text='loan/bond',
            variable=self.reason,
        )
        thisRB.grid(column=2,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='interest',
            text='interest',
            variable=self.reason,
        )
        thisRB.grid(column=3,row=0)
        thisRB = ttk.Radiobutton(
            self.reasonFrame,
            value='another reason',
            text='another reason',
            variable=self.reason,
        )
        thisRB.grid(column=4,row=0)
        confirmButton = tki.Button(self.frm,text='Transfer',command=self.confirm)
        confirmButton.grid(column=0,row=5,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=2,row=5,padx=PADVAL,pady=PADVAL)
    def confirm(self,event=None):
        try:
            amount = int(self.entry.get())
            targetType,targetName = self.target.get().split('.')
#            if self.entityType == 'player':
            funcList = [self.accounter.changeMoney,self.accounter.changeMoney]
            doVarList = [[self.entityName,self.entityType,-amount],[targetName,targetType,amount]]
            undoVarList = [[self.entityName,self.entityType,amount],[targetName,targetType,-amount]]
#            elif self.entityType == 'company':
#                funcList = [self.accounter.changeMoney,self.accounter.changeMoney]
#                doVarList = [[self.entityName,self.entityType,-amount],[targetName,targetType,amount]]
#                undoVarList = [[self.entityName,self.entityType,amount],[targetName,targetType,-amount]]
            Memento(
                "{0} transferred {1}{2} to {3} for {4}.".format(self.entityName,self.accounter['currencySign'],amount,targetName,self.reason.get()),
                funcList,
                doVarList,
                funcList,
                undoVarList
            )
            self.root.updateLabels()
            self.top.destroy()
        except ValueError:
            showinfo(message = 'Must enter an amount in integers.')

class LoadScenarioDialog:
    root = None
    def __init__(self,accounter,entityType='bank',entityName='bank'):
        self.top = tki.Toplevel(LoadScenarioDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
#        self.top.bind('<Key-Return>',self.confirm)
#        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.accounter = accounter
        defaults = ['' for i in range(10)]
        defaults[:len(DEFAULT_PLAYERS)] = DEFAULT_PLAYERS
        self.startingcashvariable = tki.StringVar(value='0')
        self.textvariables = [tki.StringVar(value=v) for v in defaults]
        self.playerEntries = [tki.Entry(self.frm,textvariable=v) for v in self.textvariables]
        label1 = tki.Label(self.frm,text='Enter player names:')
        label1.grid(row = 0,column=0)
        for i,pE in enumerate(self.playerEntries):
            pE.grid(column = 0, row = i+1,padx=PADVAL,pady=PADVAL)
        label2 = tki.Label(self.frm,text='Enter starting cash per player\nfor the number of players entered:')
        label2.grid(column = 0,row=len(self.playerEntries)+2,padx=PADVAL,pady=PADVAL)
        self.entry = IntegerEntry(self.frm,justify='right',textvariable=self.startingcashvariable)
        self.entry.grid(column = 0,row=len(self.playerEntries)+3,padx=PADVAL,pady=PADVAL)
        self.playerEntries[0].focus_set()
        confirmButton = tki.Button(self.frm,text='Start!',command=self.confirm)
        confirmButton.grid(column=0,row=len(self.playerEntries)+4,padx=PADVAL,pady=PADVAL)
        
    def confirm(self,event=None):
        money = int(self.startingcashvariable.get())
        for v in self.textvariables:
            pName = v.get()
            if pName != '':
                self.accounter.addPlayer(pName,money)
        self.root.updateGUI()
        self.top.destroy()


class DeleteDialog:
    root = None
    def __init__(self,entityType,accounter,removeFunc):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.entityType = entityType
        self.removeFunc = removeFunc
        lbl = tki.Label(self.frm,text = 'Remove:')
        lbl.grid(column=0,row=0,sticky='W',padx=PADVAL,pady=PADVAL)
        if self.entityType == 'player':
            self.playerVars = {}
            for i,playerName in enumerate(self.accounter['players'].keys()):
                thisVar = tki.IntVar()
                cb = tki.Checkbutton(self.frm,text=playerName,variable=thisVar)
                cb.grid(column=0,row=i+1,sticky='W',padx=PADVAL,pady=PADVAL)
                cb.deselect()
                self.playerVars[playerName] = thisVar
                varLen = i+1
        elif self.entityType == 'company':
            self.companyVars = {}
            for i,cName in enumerate(self.accounter['companyNames']):
                thisVar = tki.IntVar()
                cb = tki.Checkbutton(self.frm,text=cName,variable=thisVar,fg=self.accounter.getCompanyColor(cName,'fg'),bg=self.accounter.getCompanyColor(cName,'bg'))
                cb.grid(column=0,row=i+1,sticky='W',padx=PADVAL,pady=PADVAL)
                cb.deselect()
                self.companyVars[cName] = thisVar
                varLen = i+1
        confirmButton = tki.Button(self.frm,text='Delete Permanently',command=self.confirm)
        confirmButton.grid(column=0,row=varLen+2,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=varLen+2,padx=PADVAL,pady=PADVAL)
    def confirm(self,event=None):
        if self.entityType == 'player':
            for playerName,killVar in self.playerVars.items():
                if killVar.get() == 1:
                    self.removeFunc(playerName)
#                    self.accounter.removePlayer(playerName)
        elif self.entityType == 'company':
            for companyName,killVar in self.companyVars.items():
                if killVar.get() == 1:
                    print 'killing {0}'.format(companyName)
                    self.removeFunc(companyName)
        self.top.destroy()

class NetWorthDialog(object):
    root = None
    def __init__(self,accounter):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.bigFont = tkFont.Font(size=20)
        listy = []
        for i,pName in enumerate(self.accounter['players'].keys()):
            netWorth = self.accounter.calcNetWorth(pName)
            listy.append((netWorth,pName))
        listy.sort(reverse=True)
        for i,(netWorth,pName) in enumerate(listy):
            pNameLbl = tki.Label(self.frm,text=pName+':  '+accounter['currencySign']+str(netWorth),font=self.bigFont)
            pNameLbl.grid(column=0,row=i,sticky='W',padx=PADVAL,pady=PADVAL)

class SetStockValueDialog(object):
    root = None
    def __init__(self,accounter):
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.accounter = accounter
        self.companyVars = {}
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        for i,cName in enumerate(self.accounter['companyNames']):
            cNameLbl = tki.Label(self.frm,text=cName,fg=self.accounter['companyColors'][i][0],bg=self.accounter['companyColors'][i][1])
            cNameLbl.grid(column=0,row=i,sticky='W',padx=PADVAL,pady=PADVAL)
            thisVar = tki.StringVar()
            thisVar.set(str(self.accounter.getStockValue(cName)))
            self.companyVars[cName] = thisVar
            entry = IntegerEntry(self.frm,textvariable=thisVar,width=5)
            entry.grid(column=1,row=i,sticky='W',padx=PADVAL,pady=PADVAL)
            varLen = i+1
        confirmButton = tki.Button(self.frm,text='Set',command=self.confirm)
        confirmButton.grid(column=0,row=varLen+2,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(self.frm,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=varLen+2,padx=PADVAL,pady=PADVAL)
    def confirm(self,event=None):
        goToExit = dict((cName,False) for cName in self.companyVars.keys())
        for cName,cVar in self.companyVars.items():
            try:
                data = int(cVar.get())
                self.accounter.setStockValue(cName,data)
                goToExit[cName] = True
            except ValueError:
                showinfo(message = u'Must enter an integer for {0}.\n(Thou maternal fornicator).'.format(cName))
        if all(goToExit.values()):
            self.top.destroy()
            nwd = NetWorthDialog(self.accounter)


class StockChangeDialog(object):
    root = None
    def __init__(self,activeEntity,accounter,entityType,buysell):
        self.actor = (activeEntity,entityType)
        self.top = tki.Toplevel(StockChangeDialog.root)
        self.top.bind('<Key-Return>',self.confirm)
        self.top.bind('<Key-KP_Enter>',self.confirm)
        self.accounter = accounter
        self.frm = tki.Frame(self.top, borderwidth=4, relief='ridge')
        self.frm.pack(fill='both', expand=True)
        self.buysell = buysell#tki.IntVar(value=0)
        self.tofrom = tki.StringVar(value='from')
        self.sharenum = tki.StringVar(value='1')
        self.relevantCompany = tki.StringVar()
        self.totalCash = tki.StringVar(value='0')
        self.stockVar = tki.StringVar()
        ## First row: Buy / sell radiobutton
#        r1f = tki.Frame(self.frm)
#        r1f.grid(row=0,column=0)
#        rabb = ttk.Radiobutton(r1f,value=0,text='Buy',variable=self.buysell,command=self.onRB1)
#        rabb.grid(column=0,row=0)
#        rabs = ttk.Radiobutton(r1f,value=1,text='Sell',variable=self.buysell,command=self.onRB1)
#        rabs.grid(column=1,row=0)
        ## First row: Entry of number of shares (self.sharenum; defaults to 1)
        r2f = tki.Frame(self.frm)
        r2f.grid(row=1,column=0)
        shNumberEntr = IntegerEntry(r2f,textvariable=self.sharenum,width=2)
        shNumberEntr.grid(column=0,row=1)
        lbl = tki.Label(r2f,text=' shares of')
        lbl.grid(column=1,row=1)
        ## Second row: getting the relevantCompany.
        self.setupRelevantCompanyRBs()
        ## Third row: just to or from ^_^
        blbl = tki.Label(self.frm,textvariable=self.tofrom)
        blbl.grid(column=0,row=3)
        ## Fourth: source of the relevantCompany.
        ## Is filled out by command onRB2 upon selecting the relevant company.
        self.sourceFrame = tki.Frame(self.frm)
        self.sourceFrame.grid(column=0,row=4)
        klbl = tki.Label(self.sourceFrame,text='[sources]')
        klbl.grid(column=0,row=3)
        ## Sixth row: price row.
        r5f = tki.Frame(self.frm)
        r5f.grid(row=5,column=0)
        p1lbl = tki.Label(r5f,text='For {0} '.format(accounter['currencySign']))
        p1lbl.grid(row=0,column=0)
        p2lbl = tki.Label(r5f,text=' per share.')
        p2lbl.grid(row=0,column=2)
        self.priceEntry = IntegerEntry(r5f,width=4,textvariable=self.stockVar)
        self.priceEntry.grid(row=0,column=1)
        ## Last row: buttons.
        r6f = tki.Frame(self.frm)
        r6f.grid(row=6,column=0)
        confirmButton = tki.Button(r6f,text='Confirm',command=self.confirm)
        confirmButton.grid(column=0,row=0,padx=PADVAL,pady=PADVAL)
        cancelButton = tki.Button(r6f,text='Cancel',command=self.top.destroy)
        cancelButton.grid(column=1,row=0,padx=PADVAL,pady=PADVAL)
        self.fromVar = tki.StringVar(value='bank.pool')
        self.tofrom.set(['from','to'][self.buysell=='sell'])

    def setupRelevantCompanyRBs(self):
        self.r3f = tki.Frame(self.frm)
        self.r3f.grid(row=2,column=0)
        ## Only in case of a sale is the number of relevantCompanies limited,
        ## and then only if shorts are not allowed.
        if self.buysell == 'sell' and self.accounter.shortingAllowed == False:
            relevantCompanies = self.accounter.getEntityPossessedCompanies(*self.actor)
        ## Buying or shorting? All companies are fair game.
        else:
            relevantCompanies = self.accounter['companyNames']
        ## Set up the actual radioButtons.
        if len(relevantCompanies) > 0:
            for i,companyName in enumerate(relevantCompanies):
                cInd = self.accounter['companyNames'].index(companyName)
                rbStyle = ttk.Style()
                rbStyle.configure(
                    '{0}.TRadiobutton'.format(companyName),
                    foreground = self.accounter['companyColors'][cInd][0],
                    background=self.accounter['companyColors'][cInd][1]
                )
                thisRB = ttk.Radiobutton(
                    self.r3f,
                    value=companyName,text='{0}'.format(companyName),
                    variable=self.relevantCompany,
                    ## Once a radioButton is clicked, set up the sources.
                    style = '{0}.TRadiobutton'.format(companyName),
                    command = self.onRB2
                )
                thisRB.grid(column=i,row=0)
        else:
            boolbl = tki.Label(self.r3f,text = 'No salable items.')
            boolbl.grid(column=0,row=0)
            self.relevantCompany.set('')

    def confirm(self,event=None):
        if self.relevantCompany.get() == '':
            return None
        try:
            pricePerShare = int(self.priceEntry.get())
        except ValueError:
            showinfo(message = 'Must enter an integer for price.\n(You turdjuggler.)')
            return None
        try:
            sharenum = int(self.sharenum.get())
        except ValueError:
            showinfo(message = 'Must enter an integer for stock amount.\n(You freckled poop.)')
            return None
        ## Now, actually execute the trade.
        price = pricePerShare * sharenum
        activeEntity,activeEntityType = self.actor
        [passiveEntityType,passiveEntity] = self.fromVar.get().split('.')
        shareCompany = self.relevantCompany.get()
        oldPricePerShare = self.accounter.getStockValue(shareCompany)
        funcList = [self.accounter.changeStockHoldings,self.accounter.changeStockHoldings,self.accounter.changeMoney,self.accounter.changeMoney,self.accounter.setStockValue,self.root.updateLabels]
        if self.buysell == 'buy':
            ## Purchase
#            self.accounter.changeStockHoldings(passiveEntity,passiveEntityType,shareCompany,-sharenum)
#            self.accounter.changeStockHoldings(activeEntity,activeEntityType,shareCompany,sharenum)
#            self.accounter.changeMoney(passiveEntity,passiveEntityType,price)
#            self.accounter.changeMoney(activeEntity,activeEntityType,-price)
            doVarList = [
                    [passiveEntity,passiveEntityType,shareCompany,-sharenum],
                    [activeEntity,activeEntityType,shareCompany,sharenum],
                    [passiveEntity,passiveEntityType,price],
                    [activeEntity,activeEntityType,-price],
                    [shareCompany,pricePerShare],
                    []
            ]
            undoVarList = [
                    [passiveEntity,passiveEntityType,shareCompany,sharenum],
                    [activeEntity,activeEntityType,shareCompany,-sharenum],
                    [passiveEntity,passiveEntityType,-price],
                    [activeEntity,activeEntityType,price],
                    [shareCompany,oldPricePerShare],
                    []
            ]
            ## check validity: does the source have enough shares?
            ## Does the seller have enough money?
            if not self.accounter.isTradeValid(shareCompany,passiveEntity,
                 passiveEntityType,activeEntity,activeEntityType,sharenum,
                 price):
                showinfo(message = 'Invalid trade.\nCheck whether seller has enough stock and whether the buyer has enough cash.')
            else:
                Memento(
                    "{0} bought {1} shares of {2} from {3} for {5}{4} per share.".format(activeEntity,sharenum,shareCompany,passiveEntity,pricePerShare,self.accounter['currencySign']),
                    funcList,
                    doVarList,
                    funcList,
                    undoVarList
                )
                self.top.destroy()
        else:
            ## Sale
            doVarList = [
                    [passiveEntity,passiveEntityType,shareCompany,sharenum],
                    [activeEntity,activeEntityType,shareCompany,-sharenum],
                    [passiveEntity,passiveEntityType,-price],
                    [activeEntity,activeEntityType,price],
                    [shareCompany,pricePerShare],
                    [],
            ]
            undoVarList = [
                    [passiveEntity,passiveEntityType,shareCompany,-sharenum],
                    [activeEntity,activeEntityType,shareCompany,sharenum],
                    [passiveEntity,passiveEntityType,price],
                    [activeEntity,activeEntityType,-price],
                    [shareCompany,oldPricePerShare],
                    [],
            ]
            if not self.accounter.isTradeValid(shareCompany,activeEntity,
                 activeEntityType,passiveEntity,passiveEntityType,sharenum,
                 price):
                showinfo(message = 'Invalid trade.\nCheck whether seller has enough stock and whether the buyer has enough cash.')
            else:
#            self.accounter.changeStockHoldings(passiveEntity,passiveEntityType,shareCompany,sharenum)
#            self.accounter.changeStockHoldings(activeEntity,activeEntityType,shareCompany,-sharenum)
#            self.accounter.changeMoney(passiveEntity,passiveEntityType,-price)
#            self.accounter.changeMoney(activeEntity,activeEntityType,price)
                Memento(
                    "{0} sold {1} shares of {2} to {3} for {5}{4} per share.".format(activeEntity,sharenum,shareCompany,passiveEntity,pricePerShare,self.accounter['currencySign']),
                    funcList,
                    doVarList,
                    funcList,
                    undoVarList
                )
                self.top.destroy()

        ## Record the stock price.
#        self.accounter.setStockValue(self.relevantCompany.get(),pricePerShare)
        ## Then get rid of the dialog.


#    def onRB1(self,event=None):
#        self.tofrom.set(['from','to'][self.buysell.get()])
#        if self.relevantCompany.get() != '':
#            self.onRB2()
#        self.r3f.destroy()
#        self.setupRelevantCompanyRBs()

    def onRB2(self,event=None):
        inIPO,inPool,playerOwned,companyOwned = self.accounter.findStocks(self.relevantCompany.get())
        ## Set the value of the stock to the last recorded value of relevantCo
        self.stockVar.set(str(self.accounter.stockValues[self.relevantCompany.get()]))
        self.sourceFrame.destroy()
        self.sourceFrame = tki.Frame(self.frm)
        self.sourceFrame.grid(column=0,row=4)
        bankFrame = tki.LabelFrame(self.sourceFrame,text='Bank')
        bankFrame.grid(row=0,column=0)
        playerFrame = tki.LabelFrame(self.sourceFrame,text='Players')
        playerFrame.grid(row=0,column=1)
        companyFrame = tki.LabelFrame(self.sourceFrame,text='Companies')
        companyFrame.grid(row=0,column=2)
        radioButtonRecipes = []
        if inPool > 0 or self.buysell == 'sell':
            radioButtonRecipes.append(
                    (None,bankFrame,u'Pool ({0})'.format(inPool),'bank.pool',None)
            )
        if inIPO > 0 or self.buysell == 'sell':
            radioButtonRecipes.append(
                    (None,bankFrame,u'IPO ({0})'.format(inIPO),'bank.IPO',None)
            )
        for player,amount in playerOwned:
            if amount > 0 or self.buysell == 'sell':
                radioButtonRecipes.append(
                        (
                            None,
                            playerFrame,
                            u'{0} ({1})'.format(player,amount),
                            u'player.{0}'.format(player),
                            None
                        )
                )
        for company,amount in companyOwned:
            if amount > 0 or self.buysell == 'sell':
                radioButtonRecipes.append(
                        (
                            company,
                            companyFrame,
                            u'{0} ({1})'.format(company,amount),
                            u'company.{0}'.format(company),
                            self.accounter.companyNames.index(company)
                        )
                )
        curIdx = 0
        if self.buysell == 'sell':
            self.fromVar.set(radioButtonRecipes[1][2])
        else:
            self.fromVar.set(radioButtonRecipes[0][2])
#        rbList = []
        for (companyName,frame,text,value,cInd) in radioButtonRecipes:
            if companyName != None:
                rbStyle = ttk.Style()
                rbStyle.configure(
                    '{0}.TRadiobutton'.format(companyName),
                    foreground = self.accounter.getCompanyColor(companyName,'fg'),
                    background = self.accounter.getCompanyColor(companyName,'bg')
                )
                thisRB = ttk.Radiobutton(
                        frame,
                        variable=self.fromVar,
                        text=text,
                        value=value,
                        style = '{0}.TRadiobutton'.format(companyName),

                        command=self.priceFocus
                )
            else:
                thisRB = ttk.Radiobutton(
                        frame,
                        variable=self.fromVar,
                        text=text,
                        value=value,
                        command=self.priceFocus
                )
            thisRB.grid(column=curIdx,row=0)
#            rbList.append(thisRB)
            curIdx += 1
#        print '***************************'
        self.fromVar.set(radioButtonRecipes[0][3])
#        print value
        self.priceFocus()

    def priceFocus(self,event=None):
        self.priceEntry.focus_set()
#        klbl = tki.Label(bankFrame,text='!!')
#        klbl.pack()

class Tk18xx:
    def __init__(self,accounter):
        ## Do not autosave while building up.
        self.autosave = False
        ## First things first: call the Tkinter constructor.
        self.gui = tki.Tk()
        self.saveName = '18xx.sav'
        self.gui.wm_title('18xx Administrator')
        ## Then, set up the Accounter.
        self.accounter = accounter
        ## Set up the menu.
        self.menu = tki.Menu(self.gui)
        self.gui.config(menu=self.menu)
        filemenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="New", command=self.new)
        filemenu.add_command(label="Load", command=self.openFile)
        filemenu.add_command(label="Load Scenario",command=self.openScenario)
        filemenu.add_command(label="Save As...", command=self.saveFile)
        filemenu.add_separator()
        filemenu.add_command(label="Show Log", command=self.showLog)
        filemenu.add_command(label="Set Bank", command=self.setBank)

        editmenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Edit", menu=editmenu)
        editmenu.add_command(label="Undo", command=self.undo)
        self.gui.bind('<Control-Key-z>',self.undo)
        editmenu.add_command(label="Redo", command=self.redo)
        self.gui.bind('<Control-Shift-Key-z>',self.redo)

        playermenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Players", menu=playermenu)
        playermenu.add_command(label='Add player',command=self.addPlayerDialog)
        playermenu.add_command(label='Remove player',command=self.removePlayerDialog)
        playermenu.add_command(
            label='Set Stock Value & Display Net Worth',
            command=self.setStockValueDialog
        )
        companymenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Companies", menu=companymenu)
        companymenu.add_command(label='Add company',command=self.addCompanyDialog)
        companymenu.add_command(label='Remove company',command=self.removeCompanyDialog)
        optionmenu = tki.Menu(self.menu)
        self.menu.add_cascade(label="Options", menu=optionmenu)
        ## OptionVars + menu items
        self.stockPoolOptionVar = tki.IntVar(
            value=int(self.accounter['poolGoesToCompany']))
        optionmenu.add_checkbutton(
            label="Stock in pool pays dividends to company (1830-style)",
            variable=self.stockPoolOptionVar,
            command=self.updateStockPoolOption
        )
        self.stockIPOOptionVar = tki.IntVar(
            value=int(self.accounter['IPOGoesToCompany']))
        optionmenu.add_checkbutton(
            label="Stock in IPO pays dividends to company (1870-style)",
            variable=self.stockIPOOptionVar,
            command=self.updateStockIPOOption
        )
        self.companyOwnIPOVar = tki.IntVar(
            value=int(self.accounter['ownOwnStockByDefault']))
        optionmenu.add_checkbutton(
            label="Floating companies default to owning own stock",
            variable=self.companyOwnIPOVar,
            command=self.updateOwnIPOOption
        )
        
        self.internalCompanyRevenueVar = tki.IntVar(
            value=int(self.accounter['internalCompanyRevenue']))
        optionmenu.add_checkbutton(
            label="Company's stock pays dividends to company",
            variable=self.internalCompanyRevenueVar,
            command=self.updateInternalCompanyRevenueOption
        )
        self.urModeVar = tki.IntVar(
            value=int(self.accounter['urMode']))
        optionmenu.add_checkbutton(
            label="UR MODE",
            variable=self.urModeVar,
            command=self.updateUrModeOption
        )
#        self.companyOwnIPOVar = tki.IntVar(
#            value=int(self.accounter['ownOwnStockByDefault']))
#        optionmenu.add_checkbutton(
#            label="Floating companies default to owning own stock",
#            variable=self.companyOwnIPOVar,
#            command=self.updateOwnIPOOption
#        )        
        
        ## Set up some GUI globals.
        self.headerFont = tkFont.Font(size=HEADERFONT)
        self.bigFont = tkFont.Font(size=BIGFONT)
        self.playerFrame = tki.LabelFrame(self.gui,
            borderwidth=3,relief='ridge',text='Players',font=self.headerFont)
        self.playerFrame.grid(
            row=1,column=0,columnspan=2,sticky='W',padx=PADVAL,pady=PADVAL)
        self.companyNotebook = ttk.Notebook(self.gui)
        self.companyNotebook.grid(
            row=3,column=0,columnspan=3,padx=PADVAL,pady=PADVAL,sticky='W')
        self.companyFrame = ttk.Frame(
            self.companyNotebook,padding=3)
        self.companyNotebook.add(self.companyFrame,text='Companies')
#        self.companyNotebook.enable_traversal()
        self.companyFrame2 = ttk.Frame(
            self.companyNotebook,padding=3)
        self.companyNotebook.add(self.companyFrame2,text='More Companies',state = 'hidden')
        self.privatesFrame = ttk.Frame(self.companyNotebook,padding=3)
        self.companyNotebook.add(self.privatesFrame,text='Privates')

        self.statFrame = tki.Frame(self.gui)
        self.statFrame.grid(
            row=5,column=0,columnspan=2,padx=PADVAL,pady=PADVAL)
#        self.scrollY = tki.Scrollbar(self,orient=tki.VERTICAL,command=self.yview)
#        self.scrollY.grid(column=3,row=0,rowspan=5,sticky='NS')
#==============================================================================
#         Containers filled with variables for use in GUI
#==============================================================================
        ## Companies
        self.revenueVars = dict(
            (cName,tki.StringVar(value='0'))
            for cName
            in self.accounter['companyNames']
        )
        self.totalStockVars = dict(
            (cName,tki.StringVar(value='0'))
            for cName
            in self.accounter['companyNames']
        )
        self.IPOvars = dict(
            (cName,tki.StringVar(value='0'))
            for cName
            in self.accounter['companyNames']
        )
        self.poolVars =  dict(
            (cName,tki.StringVar(value='0'))
            for cName
            in self.accounter['companyNames']
        )
        self.withholdVars = dict(
            (cName,tki.IntVar(value=0))
            for cName
            in self.accounter['companyNames']
        )
        self.companyHeldStockVarLists = dict(
            (cName,[
                tki.StringVar(value='')
                for c
                in self.accounter['companyNames']
                ]
            )
            for cName in self.accounter['companyNames']
        )
        self.companyCashVars = dict(
            (cName,tki.IntVar(value=0))
            for cName
            in self.accounter['companyNames']
        )
        ## Players
        self.playerCashVars = dict(
            (pName,tki.StringVar(value='0'))
            for pName
            in self.accounter['players']
        )
        self.playerHeldStockVarLists = dict(
            (pName,[
                tki.StringVar(value='')
                for c
                in self.accounter['companyNames']
            ])
            for pName
            in self.accounter['players']
        )
        ## Overall
        self.moneyLeftVar = tki.StringVar(
            value='Left in bank:\n{1}{0}'.format(self.accounter.doAccounts(),accounter['currencySign'])
        )
        self.updateGUI()

    def updateStockPoolOption(self,event=None):
        self.accounter.setOption('poolGoesToCompany',bool(self.stockPoolOptionVar.get()))

    def updateStockIPOOption(self,event=None):
        self.accounter.setOption('IPOGoesToCompany',bool(self.stockIPOOptionVar.get()))

    def updateOwnIPOOption(self,event=None):
        self.accounter.setOption('ownOwnStockByDefault',bool(self.companyOwnIPOVar.get()))

    def updateInternalCompanyRevenueOption(self,event=None):
        self.accounter.setOption('internalCompanyRevenue',bool(self.internalCompanyRevenueVar.get()))

    def updateUrModeOption(self,event=None):
        self.accounter.setOption('urMode',bool(self.urModeVar.get()))


    def undo(self,event=None):
        try:
            UNDOSTACK[-1].undo()
            self.updateLabels()
        except IndexError:
            pass

    def redo(self,event=None):
        try:
            REDOSTACK[-1].do()
            REDOSTACK.pop()
            self.updateLabels()
        except IndexError:
            pass

    def showLog(self,event=None):
        if self.autosave:
            self.saveFile(autosave=True)
            print '####################################################'
        for logMemento in UNDOSTACK:
            print logMemento.logMessage

    def updateLabels(self,event=None):
#        print 'updating labels!'
        for cName in self.accounter['companies']:
            cIndex = self.accounter['companyNames'].index(cName)
            self.companyCashVars[cName].set(
                '{1} {0}'.format(self.accounter['companies'][cName][0],self.accounter['currencySign']))
            self.totalStockVars[cName].set(
                '{0}'.format(
                    self.accounter['companyTotalStock'][cName]
                )
            )
            self.IPOvars[cName].set(
                '{0}%'.format(
                    int(self.accounter['stockIPO'][cIndex] / float(self.accounter['companyTotalStock'][cName]) * 100)
                )
            )
            self.poolVars[cName].set(
                '{0}%'.format(
                    int(self.accounter['stockPool'][cIndex] / float(self.accounter['companyTotalStock'][cName]) * 100)
                )
            )
            for i,var in enumerate(self.companyHeldStockVarLists[cName]):
                stockPercentage = (
                    self.accounter['companies'][cName][i+1] /
                    float(self.accounter['companyTotalStock'][self.accounter['companyNames'][i]])
                ) * 100
                stockText = ['',str(int(stockPercentage))+'%'][stockPercentage>0]
                var.set(stockText)

        ## Players
        for pName in self.accounter['players']:
            self.playerCashVars[pName].set('{1} {0}'.format(self.accounter['players'][pName][0],self.accounter['currencySign']))
#            self.playerHeldStockVarLists[pName] = dict((pName,[]) for pName in self.accounter.players)
            for companyIndex,var in enumerate(self.playerHeldStockVarLists[pName]):
                try:
                    stockPercentage = (
                        self.accounter['players'][pName][companyIndex+1] /
                        float(self.accounter.getTotalHeldStock(self.accounter['companyNames'][companyIndex]))
                    ) * 100
                    stockText = ['',str(int(stockPercentage))+'%'][stockPercentage!=0]
                except ZeroDivisionError:
                    stockText = ''
                
                var.set(stockText)
        ## Overall
        moneyLeft = self.accounter.doAccounts()
        if moneyLeft <= 0:
            showinfo(message='The bank has broken.')
        self.moneyLeftVar.set('Left in bank:\n{1}{0}'.format(moneyLeft,self.accounter['currencySign']))
        self.showLog()

    def updateGUI(self,event=None):
#        print 'updating gui'
        ## check whether every player has the requisite vars
        newPlayers = self.accounter['playerNames'][len(self.playerCashVars):]
        for newPlayer in newPlayers:
            self.playerCashVars[newPlayer] = tki.StringVar(value='0')
            self.playerHeldStockVarLists[newPlayer] = [
                tki.StringVar(value='')
                for c in self.accounter['companyNames']
            ]
        bankFrame = tki.Frame(self.gui)
        bankFrame.grid(row=1,column=2)
        moneyLeftLabel = tki.Label(bankFrame,textvariable = self.moneyLeftVar,font=self.bigFont)
        moneyLeftLabel.grid(row=0,column=0)
        def moneyButtonHandler(entityName='bank',entityType='bank'):
            return self.transferMoneyDialog(entityType,entityName)
        moneyButton = tki.Button(bankFrame,text='Transfer Money',command=moneyButtonHandler)
        moneyButton.grid(column=0,row=1,padx=PADVAL,pady=PADVAL)
        ## check whether every company has the requisite vars
        newCompanies = self.accounter['companyNames'][len(self.revenueVars):]
        for cName,varList in self.companyHeldStockVarLists.items():
            for newCompanyName in newCompanies:
                varList.append(tki.StringVar(value='0'))
        for pName,varList in self.playerHeldStockVarLists.items():
            for newCompanyName in newCompanies:
                varList.append(tki.StringVar(value='0'))
        for cName in newCompanies:
            self.revenueVars[cName] = tki.StringVar(value='0')
            self.withholdVars[cName] = tki.IntVar(value=0)
            self.totalStockVars[cName] = tki.StringVar(value='')
            self.IPOvars[cName] =  tki.StringVar(value='')
            self.poolVars[cName] = tki.StringVar(value='')
            self.companyHeldStockVarLists[cName] = [tki.StringVar(value='0') for c in self.accounter.companyNames]
            self.companyCashVars[cName] = tki.StringVar(value='{} 0'.format(self.accounter['currencySign']))
#        print self.revenueVars
        for cName in self.accounter['companyNames']:
            self.revenueVars[cName].set(self.accounter.lastRecordedRevenues[cName])
        ## overall needed:
        if SHOW_COMPSTOCK:
            companyCount = len(self.accounter['companyNames'])
        else:
            companyCount = 0
        self.moneyLeftVar.set(value='Left in bank:\n{0}{1}'.format(self.accounter['currencySign'],self.accounter.doAccounts()))
        ## now, the players part of the GUI
        for kidid,kid in self.playerFrame.children.items():
            kid.destroy()
        nameColHeader = tki.Label(self.playerFrame,font=self.headerFont,text='Name')
        nameColHeader.grid(column=0,row=0,padx=PADVAL,pady=PADVAL)
        cashColHeader = tki.Label(self.playerFrame,font=self.headerFont,text='Cash')
        cashColHeader.grid(column=1,row=0,padx=PADVAL,pady=PADVAL)
        for i,companyName in enumerate(self.accounter['companyNames']):
            companyHeader = tki.Label(self.playerFrame,text = companyName,
                    font=self.headerFont,
                    fg=self.accounter.companyColors[i][0],
                    bg=self.accounter.companyColors[i][1])
            companyHeader.grid(column=2+i,row=0,padx=PADVAL,pady=PADVAL)
        for i,(playerName,playerHoldings) in enumerate(sorted(self.accounter.players.items())):
            pNameLabel = tki.Label(self.playerFrame,text = playerName,font=self.bigFont)
            pNameLabel.grid(column=0,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='W')
            pCashLabel = tki.Label(self.playerFrame,textvariable = self.playerCashVars[playerName],font=self.bigFont)
            pCashLabel.grid(column=1,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            for j,companyName in enumerate(self.accounter['companyNames']):
                companyLabel = tki.Label(
                    self.playerFrame,
                    textvariable = self.playerHeldStockVarLists[playerName][j],
                    font=self.bigFont
                )
                companyLabel.grid(column=2+j,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            def stockBuyButtonHandler(entityName=playerName,entityType='player',buysell='buy'):
                return self.changeStockDialog(entityName,entityType,buysell)
            def stockSellButtonHandler(entityName=playerName,entityType='player',buysell='sell'):
                return self.changeStockDialog(entityName,entityType,buysell)
            stockBuyButton = tki.Button(self.playerFrame,text='Buy Stock',command = stockBuyButtonHandler,height=1)
            stockBuyButton.grid(column=companyCount+2,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            stockSellButton = tki.Button(self.playerFrame,text='Sell Stock',command = stockSellButtonHandler)
            stockSellButton.grid(column=companyCount+3,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            def moneyButtonHandler(entityName=playerName,entityType='player'):
                return self.transferMoneyDialog(entityType,entityName)
            moneyButton = tki.Button(self.playerFrame,text='Transfer Money',command=moneyButtonHandler)
            moneyButton.grid(column=companyCount+4,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            if i < len(self.accounter.players):
                sep = ttk.Separator(self.playerFrame)
                sep.grid(column=0,columnspan=5+companyCount,row=i*2+1,padx=PADVAL,pady=PADVAL,sticky='EW')
        for kidid,kid in self.companyFrame.children.items():
            kid.destroy()

        ## Companies-part
        nameColHeader = tki.Label(self.companyFrame,text='Name',font=self.headerFont)
        nameColHeader.grid(column=0,row=0,padx=PADVAL,pady=PADVAL)
        totalStockColHeader = tki.Label(self.companyFrame,text='#\nstocks',font=self.headerFont)
        totalStockColHeader.grid(column=1,row=0,padx=PADVAL,pady=PADVAL)
        cashColHeader = tki.Label(self.companyFrame,text='Cash',font=self.headerFont)
        cashColHeader.grid(column=3,row=0,padx=PADVAL,pady=PADVAL)

        IPOColHeader = tki.Label(self.companyFrame,text='IPO',font=self.headerFont)
        IPOColHeader.grid(column=4,row=0,padx=PADVAL,pady=PADVAL)
        poolColHeader = tki.Label(self.companyFrame,text='Pool',font=self.headerFont)
        poolColHeader.grid(column=5,row=0,padx=PADVAL,pady=PADVAL)

        revColHeader = tki.Label(self.companyFrame,text='Oper.\nRev.',font=self.headerFont)
        revColHeader.grid(column=6,row=0,padx=PADVAL,pady=PADVAL)
#        whColHeader = tki.Label(self.companyFrame,text='With-\nhold?',font=self.headerFont)
#        whColHeader.grid(column=9,row=0,padx=PADVAL,pady=PADVAL)
        for i,companyName in enumerate(self.accounter['companyNames']):
            companyHeader = tki.Label(
                    self.companyFrame,
                    text = companyName,
                    font=self.headerFont,
                    fg=self.accounter.companyColors[i][0],
                    bg=self.accounter.companyColors[i][1]
            )
            companyHeader.grid(column=10+i,row=0,padx=PADVAL,pady=PADVAL)
        for i,cName in enumerate(self.accounter['companyNames']):
            fgcolor = self.accounter.companyColors[i][0]
            bgcolor = self.accounter.companyColors[i][1]
            cNameLabel = tki.Label(self.companyFrame,text = cName,font=self.bigFont,fg=fgcolor,bg=bgcolor)
            cNameLabel.grid(column=0,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='W')
            cTotalStockLabel = tki.Label(self.companyFrame,textvariable = self.totalStockVars[cName],
                    font=self.bigFont)
            cTotalStockLabel.grid(column=1,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')

            def editCompanyButtonHandler(entityName=cName):
                return self.editCompanyDialog(entityName)
#                self.updateGUI()
            editCompanyButton = tki.Button(self.companyFrame,text='Edit',command = editCompanyButtonHandler, fg=fgcolor, bg=bgcolor)
            editCompanyButton.grid(column=2,row=(i+1)*2,padx=PADVAL,pady=PADVAL)

            cCashLabel = tki.Label(self.companyFrame,textvariable=self.companyCashVars[cName],font=self.bigFont)
            cCashLabel.grid(column=3,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            cIPOLabel = tki.Label(self.companyFrame,textvariable = self.IPOvars[cName],
                    font=self.bigFont)
            cIPOLabel.grid(column=4,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            cPoolLabel = tki.Label(self.companyFrame,textvariable = self.poolVars[cName],font=self.bigFont)
            cPoolLabel.grid(column=5,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            operateValueVar = self.revenueVars[cName]
#            operateEntry = tki.Entry(self.companyFrame,textvariable=operateValueVar,font=self.bigFont,justify='right',width=4)
            operateEntry = IntegerEntry(self.companyFrame,textvariable=operateValueVar,font=self.bigFont,justify='right',width=4)
            operateEntry.bind('<FocusOut>', self.updateEntry)
            operateEntry.grid(column=6,row=(i+1)*2,padx=PADVAL,pady=PADVAL)

            def dividendButtonHandler(companyName=cName):
                return self.payDividend(companyName)
            def withholdButtonHandler(companyName=cName):
                return self.payDividend(companyName,'withhold')
            def fiftyfiftyButtonHandler(companyName=cName):
                return self.payDividend(companyName,'50/50')
            operateButton = tki.Button(self.companyFrame,text='Operate',command=dividendButtonHandler, fg=fgcolor, bg=bgcolor)
            operateButton.grid(column=7,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
#            withholdCheck = tki.Checkbutton(self.companyFrame,text='',variable=self.withholdVars[cName])
#            withholdCheck.grid(column=8,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            fiftyfiftyButton = tki.Button(self.companyFrame,text='50/50',command=fiftyfiftyButtonHandler, fg=fgcolor, bg=bgcolor)
            fiftyfiftyButton.grid(column=8,row=(i+1)*2,padx=PADVAL,pady=PADVAL)  
            withholdButton = tki.Button(self.companyFrame,text='Withhold',command=withholdButtonHandler, fg=fgcolor, bg=bgcolor)
            withholdButton.grid(column=9,row=(i+1)*2,padx=PADVAL,pady=PADVAL)            
            if SHOW_COMPSTOCK:
                for j,companyName in enumerate(self.accounter['companyNames']):
                    companyLabel = tki.Label(
                        self.companyFrame,
                        textvariable = self.companyHeldStockVarLists[cName][j],
                        font=self.bigFont)
                    companyLabel.grid(column=10+j,row=(i+1)*2,padx=PADVAL,pady=PADVAL,sticky='E')
            def stockBuyButtonHandler(entityName=cName,entityType='company',buysell='buy' ):
                return self.changeStockDialog(entityName,entityType,buysell)
            def stockSellButtonHandler(entityName=cName,entityType='company',buysell='sell' ):
                return self.changeStockDialog(entityName,entityType,buysell)
            stockBuyButton = tki.Button(self.companyFrame,text='Buy Stock',command = stockBuyButtonHandler, fg=fgcolor, bg=bgcolor)
            stockBuyButton.grid(column=companyCount+10,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            stockSellButton = tki.Button(self.companyFrame,text='Sell Stock',command = stockSellButtonHandler, fg=fgcolor, bg=bgcolor)
            stockSellButton.grid(column=companyCount+11,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            def moneyButtonHandler(entityName=cName,entityType='company'):
                return self.transferMoneyDialog(entityType,entityName)
            moneyButton = tki.Button(self.companyFrame,text='Transfer Money',command=moneyButtonHandler, fg=fgcolor, bg=bgcolor)
            moneyButton.grid(column=companyCount+12,row=(i+1)*2,padx=PADVAL,pady=PADVAL)
            if i < len(self.accounter['companyNames']):
                sep = ttk.Separator(self.companyFrame)
                sep.grid(column=0,columnspan=13+companyCount,row=i*2+1,padx=PADVAL,pady=PADVAL,sticky='EW')
#        print self.companyFrame.children
        self.updateLabels()

    def new(self):
        self.accounter = Accounter()
        self.accounter.setup()
        self.updateGUI()

    def setBank(self):
        sbdb = Mbox(['Set the amount of money in the bank:'],self.accounter.__dict__,['bankSize'], typeCast = [int])
        sbdb.b_submit.bind('<Destroy>',self.updateGUI)

    def openFile(self):
        if not os.path.exists('./saves/'):
            os.mkdir('./saves/')
        name = tkFileDialog.askopenfilename(initialdir='./saves',filetypes=[('sav','*.sav')])
        if name != '':
            nameParts = name.split('.')
            name = nameParts[0]
            fileType = nameParts[-1]
            if fileType == 'sav':
                print name
            self.accounter.load(name+'.sav')
            newAccounter = Accounter()
            newAccounter.setup()
            newAccounter.load(name+'.sav')
            self.gui.destroy()
            self.__init__(newAccounter)
#            self.updateGUI()

    def openScenario(self):
        self.openFile()
#        print 'call up a player dialog'
        lsd = LoadScenarioDialog(self.accounter)
        lsd.root = self

    def saveFile(self,autosave=False):
        if not os.path.exists('./saves/'):
            os.mkdir('./saves/')
        if not autosave:
            self.saveName = tkFileDialog.asksaveasfilename(initialdir='./saves',filetypes=[('sav','*.sav')])
        else:
            self.accounter.serialize('auto.sav')
#        print name
        if self.saveName.split('.')[-1] != 'sav':
            self.accounter.serialize(self.saveName+'.sav')
            print 'add'

        elif self.saveName != '':
            self.accounter.serialize(self.saveName)
        else:
            pass

    def addPlayerDialog(self):
        self.tempDict = {'pname':None,'pcash':0}
        apdb = Mbox(['Player name:','Player cash:'],self.tempDict,['pname','pcash'],self.addPlayer,[None,int])

    def addPlayer(self):
        pName = self.tempDict['pname']
        pCash = self.tempDict['pcash']
        doVarList = [[pName,pCash],[]]
        undoVarList = [[pName],[]]
#        self.accounter.addPlayer(pName,pCash)
        Memento(
                "Added player '{0}' with {2}{1}.".format(pName,pCash,self.accounter['currencySign']),
                [self.accounter.addPlayer,self.updateGUI],
                doVarList,
                [self.accounter.removePlayer,self.updateGUI],
                undoVarList
        )

    def addCompany(self,cName,stockNum,ownIPO,mainColor,bgColor):
#        self.accounter.addCompany(cName,stockNum)
#        self.accounter.setCompanyColors(cName,mainColor,bgColor)
        try:
            doVarList = [[cName,stockNum,ownIPO],[cName,mainColor,bgColor],[]]
            undoVarList = [[cName],[]]
    #        self.accounter.addPlayer(pName,pCash)
            Memento(
                    "Added company '{0}' with {1} shares.".format(cName,stockNum),
                    [self.accounter.addCompany,self.accounter.setCompanyColors,self.updateGUI],
                    doVarList,
                    [self.accounter.removeCompany,self.updateGUI],
                    undoVarList
            )
        except ValueError:
            showinfo(message = 'That company name is already taken. Please choose another.')

    def editCompany(self,cName,stockNum,ownIPO,mainColor,bgColor):
        previousStockNum = self.accounter.companyTotalStock[cName]
        previousMainColour,previousBgColour = self.accounter.companyColors[self.accounter.companyNames.index(cName)]
        doVarList = [[cName,stockNum,ownIPO],[cName,mainColor,bgColor],[]]
        undoVarList = [[cName,previousStockNum,ownIPO],[cName,previousMainColour,previousBgColour],[]]
#        self.accounter.addPlayer(pName,pCash)
        Memento(
                "Edited company '{0}' to {1} shares.".format(cName,stockNum),
                [self.accounter.editCompany,self.accounter.setCompanyColors,self.updateGUI],
                doVarList,
                [self.accounter.editCompany,self.accounter.setCompanyColors,self.updateGUI],
                undoVarList
        )

    def payDividend(self,companyName,kind='100%'):
        text = {'100%':'','50/50':'Paying 50/50.','withhold':'Withholding.'}
        try:
            revenue = int(eval(self.revenueVars[companyName].get()))
#            withhold = bool(self.withholdVars[companyName].get())
            doVarList = [[companyName,revenue,kind]]
            undoVarList = [[companyName,-revenue,kind]]
#            self.accounter.companyDividend(*doVars)
            Memento(
                    '{0} operated for {3}{1}. {2}'.format(companyName,revenue,text[kind],self.accounter['currencySign']),
                    [self.accounter.companyDividend],
                    doVarList,
                    [self.accounter.companyDividend],
                    undoVarList
            )
#            UNDOSTACK[-1].do()


        except ValueError:
            showinfo(message = 'Must enter an integer in revenue box.')
        self.updateLabels()
#        self.updateGUI()

    def removeCompany(self,cName):
        doVarList = [[cName],[cName],[]]
        cIndex = self.accounter.companyNames.index(cName)
        stockNum = self.accounter.companyTotalStock[cName]
        mainColor,bgColor = self.accounter.companyColors[cIndex]
        print "WARNING! Need to implement undoing companies' holdings and cash on undo."
        undoVarList = [[cName,stockNum],[cName,mainColor,bgColor],[]]
        Memento(
            'Removing company {0}.'.format(cName),
            [self.removeCompanyHelper,self.accounter.removeCompany,self.updateGUI],
            doVarList,
            [self.accounter.addCompany,self.accounter.setCompanyColors,self.updateGUI],
            undoVarList
        )

    def removePlayer(self,pName):
#        self.removePlayerHelper(pName)
#        self.accounter.removePlayer(pName)
#        self.updateGUI()
        pCash = self.accounter.players[pName][0]
        doVarList = [[pName],[pName],[]]
        undoVarList = [[pName,pCash],[]]
        print "WARNING! Need to implement undoing players' holdings and cash on undo."
        Memento(
            'Removing player {0}.'.format(pName),
            [self.removePlayerHelper,self.accounter.removePlayer,self.updateGUI],
            doVarList,
            [self.accounter.addPlayer,self.updateGUI],
            undoVarList
        )

    def removeCompanyHelper(self,deadCompanyName):
        ## Remove deleted company vars.
        deadCompanyIndex = self.accounter.companyNames.index(deadCompanyName)
        for cName in self.revenueVars.keys():
            del(self.companyHeldStockVarLists[cName][deadCompanyIndex])
        for pName in self.playerCashVars.keys():
            del(self.playerHeldStockVarLists[pName][deadCompanyIndex])
        del(self.revenueVars[deadCompanyName])
        del(self.withholdVars[deadCompanyName])
        del(self.IPOvars[deadCompanyName])
        del(self.poolVars[deadCompanyName])
        del(self.companyHeldStockVarLists[deadCompanyName])
        del(self.companyCashVars[deadCompanyName])

    def removePlayerHelper(self,pName):
        ## Remove deleted player vars.
        del(self.playerCashVars[pName])
        del(self.playerHeldStockVarLists[pName])

    def removePlayerDialog(self):
#        self.tempDict = {'pname':None,'pcash':0}
        dd = DeleteDialog('player',self.accounter,self.removePlayer)
#        dd.frm.bind('<Destroy>',self.updateGUI)

    def removeCompanyDialog(self):
#        self.tempDict = {'pname':None,'pcash':0}
        dd = DeleteDialog('company',self.accounter,self.removeCompany)
#        dd.frm.bind('<Destroy>',self.updateGUI)

    def addCompanyDialog(self):
        ccd = AddCompanyDialog(self.accounter,self.addCompany)
#        ccd.frm.bind('<Destroy>',self.updateGUI)
#        self.tempDict = {'cname':None}
#        acdb = Mbox(['Company name:','Total company stocks available:'],self.tempDict,['cname','stockNum'],self.addCompany,[None,int])

#    def onAddCompany(self,event=None):
#
#        self.

    def editCompanyDialog(self,cName):
        ecd = EditCompanyDialog(self.accounter,self.editCompany,cName)

    def changeStockDialog(self,entityName,entityType,buysell):
        pcsd = StockChangeDialog(entityName,self.accounter,entityType,buysell)
        pcsd.root = self
#        pcsd.frm.bind('<Destroy>',self.updateLabels)

    def transferMoneyDialog(self,entityType,entityName):
        tmd = TransferMoneyDialog(entityType,entityName,self.accounter)
        tmd.root = self
        
    def changeMoneyDialog(self,entityName,entityType):
        self.tempDict = {'name':entityName,'cash':0}
        if entityType == 'player':
            relevantFnc = self.changePlayerMoney
        elif entityType == 'company':
            relevantFnc = self.changeCompanyMoney
        cpmdb = Mbox(
            [u'Change cash of {0} by:\n(use negative number to subtract)'.format(entityName)],\
            self.tempDict,
            ['cash'],
            relevantFnc,[int]
        )
#        cpmdb.frm.bind('<Destroy>',self.updateLabels())

    def setStockValueDialog(self,event=None):
        ssvd = SetStockValueDialog(self.accounter)
        ssvd.root = self

    def changePlayerMoney(self,event=None):
        doVarList = [[self.tempDict['name'],'player',self.tempDict['cash']]]
        undoVarList = [[self.tempDict['name'],'player',-self.tempDict['cash']]]
        Memento(
                'Changed money of player {0} by {2}{1}.'.format(doVarList[0][0],doVarList[0][2],self.accounter['currencySign']),
                [self.accounter.changeMoney],
                doVarList,
                [self.accounter.changeMoney],
                undoVarList
        )
        self.updateLabels()

    def changeCompanyMoney(self,event=None):
        doVarList = [[self.tempDict['name'],'company',self.tempDict['cash']]]
        undoVarList = [[self.tempDict['name'],'company',-self.tempDict['cash']]]
        Memento(
                'Changed money of company {0} by {2}{1}.'.format(doVarList[0][0],doVarList[0][2],self.accounter['currencySign']),
                [self.accounter.changeMoney],
                doVarList,
                [self.accounter.changeMoney],
                undoVarList
        )
        self.updateLabels()

    def updateEntry(self,event=None):
        ## Put whatever is in enterable widgets (i.e. revenues) into the
        ## accounter.
        ## Also check inputs.
        for i,cName in enumerate(self.accounter.companyNames):
            thisVar = self.revenueVars[cName]
            try:
                thisVar = int(thisVar.get())
                self.accounter.lastRecordedRevenues[cName] = thisVar
            except ValueError:
#                showinfo(message = 'Must enter an integer in revenue box.\n(You smegmerchant).')
                thisVar.set(str(self.accounter.lastRecordedRevenues[cName]))

if __name__ == '__main__':
    DEFAULT_PLAYERS = ['Boris','Ot','Xeryus']
    a = Accounter()
    a.setup()
    a.bankSize = 1
    a.currencySign = 'C$'

### PLAYERS
#    a.addPlayer('Boris',195)
#    a.addPlayer('Mischa',525)
#    a.addPlayer('Ot',195)
#    a.addPlayer('Jasper',400)
#    a.addPlayer('Mel',400)
#    a.addPlayer('Floor',400)
#    a.addPlayer('Tristan',0)
#    a.addPlayer('Eelco',0)
#    a.addPlayer('Rene',195)
#    a.addPlayer('Xeryus',195)
#    a.addPlayer('Wouter',400)
#    a.addPlayer('Gilles',480)
#    a.addPlayer('4. Red (aggro)',400)
#    a.addPlayer('1. White (opportuno)',400)
#    a.addPlayer('3. Blue (conservative)',400)
#    a.addPlayer('2. Green (longterm)',400)

    tk18xx = Tk18xx(a)
    tk18xx.updateLabels()
    tk18xx.autosave = True
    tk18xx.gui.mainloop()